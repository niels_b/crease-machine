import numpy as np
from numpy.linalg import det

pointA = np.array([[-1.0, -1.0]])
pointB = np.array([[+1.0, -1.0]])
pointC = np.array([[+2.0, +2.0]])
pointD = np.array([[-1.0, +1.0]])

X = np.concatenate((pointA, pointB, pointC, pointD), axis=0)

gauss_points = [[-0.5774, -0.5774],
                [-0.5774, +0.5774],
                [+0.5774, -0.5774],
                [+0.5774, +0.5774]]

for xi, eta in gauss_points:
    dN = 0.25*np.array([
        [+(1 + eta), +(1 + xi)],
        [-(1 + eta), +(1 - xi)],
        [-(1 - eta), -(1 - xi)],
        [+(1 - eta), -(1 + xi)]
                        ])
    J = X.T @ dN
    j = det(J)
    print(j)
