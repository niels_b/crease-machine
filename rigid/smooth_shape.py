import numpy as np
from core.directories import root_dir
from core.help_functions import make_vtk_file


def saddle(x, y):
    a, b = 0.8, 0.8
    z = x**2 / a**2 - y**2 / b**2
    return z


def dome(x, y):
    a, b = 0.9, 0.9
    z = -x**2 / a**2 - y**2 / b**2
    return z


def arch(x, y):
    b = 0.6
    z = -y**2 / b**2
    return z


def make_triangulated_shape():
    shape = 'arch'

    if shape == 'saddle':
        shape_fun = saddle
    elif shape == 'dome':
        shape_fun = dome
    elif shape == 'arch':
        shape_fun = arch
    else:
        raise Exception('shape not valid')

    length_square = 0.6

    nodes = []
    triangles = []

    small_number = 1e-6
    step = 0.01
    x = np.arange(-length_square/2, length_square/2 + small_number, step)
    y = np.arange(-length_square/2, length_square/2 + small_number, step)
    x, y = np.meshgrid(x, y)

    num_y, num_x = x.shape
    z = np.zeros((num_y, num_x))

    for j in range(num_y):
        for i in range(num_x):
            z[j][i] = shape_fun(x[j][i], y[j][i])
            nodes.append((x[j][i], y[j][i], z[j][i]))

    for i in range(num_x-1):
        for j in range(num_y-1):
            triangles.append((i*num_y+j, i*num_y+j+1, (i+1)*num_y+j+1))
            triangles.append((i*num_y+j, (i+1)*num_y+j, (i+1)*num_y+j+1))

    make_vtk_file(nodes, triangles, shape, 0,
                  filepath=f'{root_dir}/outputs/rigid/{shape}/{shape}-smooth.vtk')

    return nodes, triangles


if __name__ == '__main__':
    make_triangulated_shape()
