import time
import numpy as np
import json
import matplotlib.pyplot as plt
from math import sin, cos, acos, pi
from core.directories import root_dir
from rigid.make_developable import total_optimization
from core.help_functions import create_output_folder, set_background


vec_len = np.linalg.norm


def make_unit_length(vec):
    return vec / vec_len(vec)


def rotate_to_flat(my_panel, my_nodes):
    p0, p1, p2, p3 = my_panel[0], my_panel[1], my_panel[2], my_panel[3]

    p0_vec, p1_vec, p2_vec, p3_vec = (np.array(my_nodes[p0]), np.array(my_nodes[p1]),
                                      np.array(my_nodes[p2]), np.array(my_nodes[p3]))

    p_0_to_1 = p1_vec - p0_vec
    p_0_to_1 = make_unit_length(p_0_to_1)   # verified

    p_0_to_3 = p3_vec - p0_vec
    p_0_to_3 = make_unit_length(p_0_to_3)   # verified

    normal_vec = np.cross(p_0_to_1, p_0_to_3)
    normal_vec = make_unit_length(normal_vec)   # verified

    z_axis = np.array([0, 0, 1])

    # axis_of_rotation = np.cross(z_axis, normal_vec)
    axis_of_rotation = np.cross(normal_vec, z_axis)
    axis_of_rotation = make_unit_length(axis_of_rotation)

    th = acos(np.dot(normal_vec, z_axis))   # angle of rotation
    # print(th*180/pi)

    # th = pi - th    # added because it worked

    ax, ay, az = axis_of_rotation[0], axis_of_rotation[1], axis_of_rotation[2]

    # print(ax, ay, az, th*180/pi)

    R = np.array([[cos(th) + ax ** 2 * (1 - cos(th)), ax * ay * (1 - cos(th)) - az * sin(th),
                   ax * az * (1 - cos(th)) + ay * sin(th)],
                  [ay * ax * (1 - cos(th)) + az * sin(th), cos(th) + ay ** 2 * (1 - cos(th)),
                   ay * az * (1 - cos(th)) - ax * sin(th)],
                  [az * ax * (1 - cos(th)) - ay * sin(th), az * ay * (1 - cos(th)) + ax * sin(th),
                   cos(th) + az ** 2 * (1 - cos(th))]])

    p0_flat_vec = R @ p0_vec
    p1_flat_vec = R @ p1_vec
    p2_flat_vec = R @ p2_vec
    p3_flat_vec = R @ p3_vec

    # print([p0_flat_vec[2], p1_flat_vec[2], p2_flat_vec[2], p3_flat_vec[2]])

    p0_flat_vec = np.array([p0_flat_vec[0], p0_flat_vec[1], 0])
    p1_flat_vec = np.array([p1_flat_vec[0], p1_flat_vec[1], 0])
    p2_flat_vec = np.array([p2_flat_vec[0], p2_flat_vec[1], 0])
    p3_flat_vec = np.array([p3_flat_vec[0], p3_flat_vec[1], 0])

    return [p0_flat_vec, p1_flat_vec, p2_flat_vec, p3_flat_vec]


def rotate_to_align(root_panel, leaf_panel, root_vec, leaf_vec, th=None):
    if th is None:
        my_attachment = root_panel[root_vec[1]] - root_panel[root_vec[0]]
        line_to_align = leaf_panel[leaf_vec[1]] - leaf_panel[leaf_vec[0]]

        my_attachment = make_unit_length(my_attachment)
        line_to_align = make_unit_length(line_to_align)

        th = acos(np.dot(line_to_align, my_attachment))     # angle of rotation
        if np.cross(my_attachment, line_to_align)[2] > 0:
            th = -th    # accounts for the direction

    R = np.array([[cos(th), -sin(th), 0],
                  [sin(th), cos(th),  0],
                  [0,       0,        1]])

    p0_aligned_vec = R @ leaf_panel[0]
    p1_aligned_vec = R @ leaf_panel[1]
    p2_aligned_vec = R @ leaf_panel[2]
    p3_aligned_vec = R @ leaf_panel[3]

    # p0_aligned_vec = np.array([p0_aligned_vec[0], p0_aligned_vec[1], 0])
    # p1_aligned_vec = np.array([p1_aligned_vec[0], p1_aligned_vec[1], 0])
    # p2_aligned_vec = np.array([p2_aligned_vec[0], p2_aligned_vec[1], 0])
    # p3_aligned_vec = np.array([p3_aligned_vec[0], p3_aligned_vec[1], 0])

    return [p0_aligned_vec, p1_aligned_vec, p2_aligned_vec, p3_aligned_vec]


def translate_to_attach(root_panel, leaf_panel, root_node, leaf_node):

    translation_vector = root_panel[root_node] - leaf_panel[leaf_node]

    p0_connected_vec = leaf_panel[0] + translation_vector
    p1_connected_vec = leaf_panel[1] + translation_vector
    p2_connected_vec = leaf_panel[2] + translation_vector
    p3_connected_vec = leaf_panel[3] + translation_vector

    return [p0_connected_vec, p1_connected_vec, p2_connected_vec, p3_connected_vec]


def align_all_rows(very_first_flat_panel, rows, optimal_nodes):
    nucleations = [very_first_flat_panel]  # list of panels (panel is a list of arrays representing nodes)
    connected_panel = very_first_flat_panel

    first_panels = [row[0] for row in rows]     # first panel of every row
    for first_panel in first_panels[1:]:
        flat_panel = rotate_to_flat(first_panel, optimal_nodes)
        aligned_panel = rotate_to_align(connected_panel, flat_panel, (3, 2), (0, 1))
        connected_panel = translate_to_attach(connected_panel, aligned_panel, 3, 0)
        nucleations.append(connected_panel)

    return nucleations


def advance_row(first_flat_panel, nonflat_row_remainder, optimal_nodes):
    flat_row = []
    connected_panel = first_flat_panel
    flat_row.append([(flat_nodes[0], flat_nodes[1]) for flat_nodes in first_flat_panel])

    for panel in nonflat_row_remainder:
        flat_panel = rotate_to_flat(panel, optimal_nodes)
        aligned_panel = rotate_to_align(connected_panel, flat_panel, (1, 2), (0, 3))
        connected_panel = translate_to_attach(connected_panel, aligned_panel, 1, 0)
        flat_row.append([(flat_nodes[0], flat_nodes[1]) for flat_nodes in connected_panel])

    return flat_row


def flattening_process():
    optimal_nodes, panels, miura_units_x, miura_units_y = total_optimization(shape)

    start_time = time.perf_counter()
    # split the pattern up in rows
    rows = []
    for i in range(2 * miura_units_y):
        row = []
        for j in range(2 * miura_units_x):
            row.append(panels[i * 2 * miura_units_x + j])
        rows.append(row)

    # print(f'rows: {rows}')

    very_first_flat_panel = rotate_to_flat(rows[0][0], optimal_nodes)    # make very first panel lie in the xy-plane

    if shape == 'saddle':
        extra_rotation = -15
    elif shape == 'dome':
        extra_rotation = -55
    elif shape == 'arch':
        extra_rotation = -39
    else:
        raise Exception('shape invalid')
    very_first_flat_panel = rotate_to_align(None, very_first_flat_panel,
                                            None, (0, 3), extra_rotation/180*pi)

    very_first_flat_panel = translate_to_attach([np.array([-0.25, -0.25, 0])],
                                                very_first_flat_panel, 0, 0)

    # print(f'very first flat panel: {very_first_flat_panel}')

    flat_nucleations = align_all_rows(very_first_flat_panel, rows, optimal_nodes)
    # make first panels of every row attach to the first

    flat_rows = []

    for i, row in enumerate(rows):
        flat_row = advance_row(flat_nucleations[i], row[1:], optimal_nodes)
        flat_rows.append(flat_row)

    end_time = time.perf_counter()
    print(f'flattening time: {round(end_time - start_time, 0)} seconds')

    return flat_rows, panels


def make_pdf_from_flats(flat_rows):
    set_background(32)

    fig = plt.figure()
    ax = fig.add_subplot(111)

    # flat_rows = [flat_rows[0]]

    for flat_row in flat_rows:
        # flat_row = [flat_row[0]]
        for my_flat_panel in flat_row:
            my_flat_panel_xs = [my_flat_node[0] for my_flat_node in my_flat_panel]
            my_flat_panel_ys = [my_flat_node[1] for my_flat_node in my_flat_panel]

            plt.scatter(my_flat_panel_xs, my_flat_panel_ys, s=4, color='white')

            my_flat_panel_xs.append(my_flat_panel_xs[0])
            my_flat_panel_ys.append(my_flat_panel_ys[0])

            plt.plot(my_flat_panel_xs, my_flat_panel_ys, color='white')

    xticks, yticks = [], []
    plt.xticks(xticks, xticks)
    plt.yticks(yticks, yticks)
    # plt.xlabel('')
    # plt.ylabel('')
    # plt.xlim(-0.2, 0.8)
    # plt.ylim(-0.4, 0.6)
    # plt.axis('equal')
    ax.set_aspect('equal')
    ax.axis('off')
    plt.tight_layout()
    plt.savefig(f'{root_dir}/outputs/{config}/rigid-flat-pattern.pdf')

    plt.close()


def make_input_from_flat_rigid(flat_rows, panels):
    nodes = []

    n_cols = len(flat_rows[0])
    for i in range(n_cols):
        for flat_row in flat_rows:
            nodes.append(list(flat_row[i][0]) + [0.0])
        nodes.append(list(flat_rows[-1][i][3]) + [0.0])
    for flat_row in flat_rows:
        nodes.append(list(flat_row[-1][1]) + [0.0])
    nodes.append(list(flat_rows[-1][-1][2]) + [0.0])

    data = {'config': f'{shape}-flattening',
            'nodes': nodes,
            'panels': panels,
            'supports': [],
            'loads': [],
            'relevant_dof': 0,
            'ModElastic': 4e9,
            'Poisson': 0.3,
            'Thickness': 0.08e-3,
            'LScaleFactor': 2,
            'maxIcr': 100,
            'maxLmd': 1400,
            'tol': 1e-5,
            'initialLoadFactor': 15,
            'multiply_load': 1e-3,
            'verbose': 1
            }

    with open(f'{root_dir}/inputs/flattening-{shape}.json', 'w') as f:
        json.dump(data, f)


if __name__ == '__main__':
    shape = 'saddle'
    config = f'rigid/{shape}/flattening'
    create_output_folder(config)
    my_flat_rows, my_panels = flattening_process()
    make_pdf_from_flats(my_flat_rows)
    make_input_from_flat_rigid(my_flat_rows, my_panels)
