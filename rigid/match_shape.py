import time
import matplotlib.pyplot as plt
import numpy as np
from line_profiler import profile
from math import pi, cos, sin

from core.directories import root_dir
from core.construct_pattern import miura_pattern
from core.part1_prepare_data import prepareData
from core.help_functions import create_output_folder, make_vtk_file

from matplotlib import cm

np.set_printoptions(edgeitems=30, linewidth=100000)
# plt.style.use('dark_background')
# grey = 32
# plt.rcParams['axes.facecolor'] = (grey / 255, grey / 255, grey / 255)
# plt.rcParams['savefig.facecolor'] = (grey / 255, grey / 255, grey / 255)

# length_square = 0.5     # length of side of the square that makes the saddle domain
# miura_units_x, miura_units_y = 4, 4     # number of miura units in x- and y-directions - normal is 8, 8

# determines the curvature of the saddle - no problems with 0.8, 0.8 - normal is 0.6, 0.6
# a, b = 0.9, 0.9

# fraction of unit size that the miura units are stretched 'north' and 'up' - normal is 0.8, 0.6
# shift_factor_north, shift_factor_perp = 0.8, 0.6


class Node:
    def __init__(self, x, y, z=None):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return f'node with coordinates {self.x, self.y}'


def add_vector_to_node(node, vector, length_vector):
    return Node(node.x + length_vector * vector[0], node.y + length_vector * vector[1], node.z + length_vector * vector[2])


def average_of_nodes(node1, node2):
    x = (node1.x + node2.x) / 2
    y = (node1.y + node2.y) / 2
    z = (node1.z + node2.z) / 2
    average_node = Node(x, y, z)
    return average_node


class Miura_unit:
    def __init__(self, node_sw, node_se, node_nw, node_ne):
        self.node_sw = node_sw
        self.node_se = node_se
        self.node_nw = node_nw
        self.node_ne = node_ne
        self.node_w = Node(0, 0, 0)
        self.node_e = Node(0, 0, 0)
        self.node_s = Node(0, 0, 0)
        self.node_n = Node(0, 0, 0)
        self.node_m = Node(0, 0, 0)

        self.xs = []
        self.ys = []
        self.zs = []

        self.lines = []

    def add_necessary_nodes(self):
        self.node_w = Node((self.node_nw.x + self.node_sw.x) / 2,
                           (self.node_nw.y + self.node_sw.y) / 2,
                           (self.node_nw.z + self.node_sw.z) / 2)
        self.node_e = Node((self.node_ne.x + self.node_se.x) / 2,
                           (self.node_ne.y + self.node_se.y) / 2,
                           (self.node_ne.z + self.node_se.z) / 2)
        self.node_s = Node((self.node_sw.x + self.node_se.x) / 2,
                           (self.node_sw.y + self.node_se.y) / 2,
                           (self.node_sw.z + self.node_se.z) / 2)
        self.node_n = Node((self.node_nw.x + self.node_ne.x) / 2,
                           (self.node_nw.y + self.node_ne.y) / 2,
                           (self.node_nw.z + self.node_ne.z) / 2)
        self.node_m = Node((self.node_sw.x + self.node_se.x + self.node_nw.x + self.node_ne.x) / 4,
                           (self.node_sw.y + self.node_se.y + self.node_nw.y + self.node_ne.y) / 4,
                           (self.node_sw.z + self.node_se.z + self.node_nw.z + self.node_ne.z) / 4)

    # def move_to_miura_like(self):
    #     vector_s_to_n = [self.node_n.x - self.node_s.x, self.node_n.y - self.node_s.y, self.node_n.z - self.node_s.z]
    #     vector_s_to_n = np.array(vector_s_to_n)
    #     vector_s_to_n = np.array(vector_s_to_n) / np.linalg.norm(vector_s_to_n)
    #     vector_s_to_n = vector_s_to_n.tolist()
    #
    #     self.node_s = add_vector_to_node(self.node_s, vector_s_to_n, shift_factor_north * length_square / miura_units_y)
    #     self.node_n = add_vector_to_node(self.node_n, vector_s_to_n, shift_factor_north * length_square / miura_units_y)
    #     self.node_w = add_vector_to_node(self.node_w,
    #                                      saddle_perpend(self.node_w.x, self.node_w.y),
    #                                      shift_factor_perp * length_square / miura_units_y)
    #     self.node_e = add_vector_to_node(self.node_e,
    #                                      saddle_perpend(self.node_e.x, self.node_e.y),
    #                                      shift_factor_perp * length_square / miura_units_y)
    #     self.node_m = add_vector_to_node(self.node_m, vector_s_to_n, shift_factor_north * length_square / miura_units_y)
    #     self.node_m = add_vector_to_node(self.node_m,
    #                                      saddle_perpend(self.node_m.x, self.node_m.y),
    #                                      shift_factor_perp * length_square / miura_units_y)

    def move_to_miura_like_2(self, shape_perp_fun, length_square, miura_units_y,
                             shift_factor_north, shift_factor_perp, a, b):
        # this version calculates the up-direction in the middle of the unit cell
        vector_s_to_n = [self.node_n.x - self.node_s.x, self.node_n.y - self.node_s.y,
                         self.node_n.z - self.node_s.z]
        vector_s_to_n = np.array(vector_s_to_n)
        vector_s_to_n = np.array(vector_s_to_n) / np.linalg.norm(vector_s_to_n)
        vector_s_to_n = vector_s_to_n.tolist()

        perp = shape_perp_fun(self.node_m.x, self.node_m.y, a, b)

        self.node_s = add_vector_to_node(self.node_s, vector_s_to_n, shift_factor_north * length_square / miura_units_y)
        self.node_n = add_vector_to_node(self.node_n, vector_s_to_n, shift_factor_north * length_square / miura_units_y)
        self.node_w = add_vector_to_node(self.node_w, perp, shift_factor_perp * length_square / miura_units_y)
        self.node_e = add_vector_to_node(self.node_e, perp, shift_factor_perp * length_square / miura_units_y)
        self.node_m = add_vector_to_node(self.node_m, vector_s_to_n, shift_factor_north * length_square / miura_units_y)
        self.node_m = add_vector_to_node(self.node_m, perp, shift_factor_perp * length_square / miura_units_y)

    # def make_plot_points(self):
    #     self.xs, self.ys, self.zs = [], [], []
    #     for node in [self.node_sw, self.node_se, self.node_nw, self.node_ne, self.node_w, self.node_e, self.node_s,
    #                  self.node_n, self.node_m]:
    #         self.xs.append(node.x)
    #         self.ys.append(node.y)
    #         self.zs.append(node.z)

    # def make_plot_lines(self):
    #     self.lines = []
    #     self.lines.append(((self.node_sw.x, self.node_s.x), (self.node_sw.y, self.node_s.y), (self.node_sw.z, self.node_s.z)))
    #     self.lines.append(((self.node_s.x, self.node_se.x), (self.node_s.y, self.node_se.y), (self.node_s.z, self.node_se.z)))
    #     self.lines.append(((self.node_w.x, self.node_m.x), (self.node_w.y, self.node_m.y), (self.node_w.z, self.node_m.z)))
    #     self.lines.append(((self.node_m.x, self.node_e.x), (self.node_m.y, self.node_e.y), (self.node_m.z, self.node_e.z)))
    #     self.lines.append(((self.node_nw.x, self.node_n.x), (self.node_nw.y, self.node_n.y), (self.node_nw.z, self.node_n.z)))
    #     self.lines.append(((self.node_n.x, self.node_ne.x), (self.node_n.y, self.node_ne.y), (self.node_n.z, self.node_ne.z)))
    #     self.lines.append(((self.node_sw.x, self.node_w.x), (self.node_sw.y, self.node_w.y), (self.node_sw.z, self.node_w.z)))
    #     self.lines.append(((self.node_w.x, self.node_nw.x), (self.node_w.y, self.node_nw.y), (self.node_w.z, self.node_nw.z)))
    #     self.lines.append(((self.node_s.x, self.node_m.x), (self.node_s.y, self.node_m.y), (self.node_s.z, self.node_m.z)))
    #     self.lines.append(((self.node_m.x, self.node_n.x), (self.node_m.y, self.node_n.y), (self.node_m.z, self.node_n.z)))
    #     self.lines.append(((self.node_se.x, self.node_e.x), (self.node_se.y, self.node_e.y), (self.node_se.z, self.node_e.z)))
    #     self.lines.append(((self.node_e.x, self.node_ne.x), (self.node_e.y, self.node_ne.y), (self.node_e.z, self.node_ne.z)))


def make_vtk_4_nodes(my_miuras, filepath, config):
    with open(filepath, 'w') as f:
        f.write('# vtk DataFile Version 2.0\n')
        f.write(f'{config}_results\n')
        f.write('ASCII\n')
        f.write('DATASET UNSTRUCTURED_GRID\n\n')

        f.write(f'POINTS {len(my_miuras)*4} float\n')
        for my_miura in my_miuras:
            for node in [my_miura.node_sw, my_miura.node_se, my_miura.node_ne, my_miura.node_nw]:
                f.write(f'{node.x} {node.y} {node.z}\n')
        f.write('\n')

        f.write(f'CELLS {len(my_miuras)*4} {3 * len(my_miuras)*4}\n')
        for i in range(len(my_miuras)):
            f.write(f'2 {0 + 4 * i} {1 + 4 * i}\n')
            f.write(f'2 {1 + 4 * i} {2 + 4 * i}\n')
            f.write(f'2 {2 + 4 * i} {3 + 4 * i}\n')
            f.write(f'2 {3 + 4 * i} {0 + 4 * i}\n')

        f.write(f'CELL_TYPES {4*len(my_miuras)}\n')
        for i in range(len(my_miuras)*4):
            f.write(f'3\n')
        f.write('\n')


def make_vtk_9_nodes(my_miuras, filepath, config):
    with open(filepath, 'w') as f:
        f.write('# vtk DataFile Version 2.0\n')
        f.write(f'{config}_results\n')
        f.write('ASCII\n')
        f.write('DATASET UNSTRUCTURED_GRID\n\n')

        f.write(f'POINTS {len(my_miuras)*9} float\n')
        for my_miura in my_miuras:
            for node in [my_miura.node_sw, my_miura.node_se, my_miura.node_ne, my_miura.node_nw,
                         my_miura.node_s, my_miura.node_e, my_miura.node_n, my_miura.node_w, my_miura.node_m]:
                f.write(f'{node.x} {node.y} {node.z}\n')
        f.write('\n')

        f.write(f'CELLS {len(my_miuras)*12} {3 * len(my_miuras)*12}\n')
        for i in range(len(my_miuras)):
            f.write(f'2 {0 + 9 * i} {4 + 9 * i}\n')
            f.write(f'2 {4 + 9 * i} {1 + 9 * i}\n')
            f.write(f'2 {1 + 9 * i} {5 + 9 * i}\n')
            f.write(f'2 {5 + 9 * i} {2 + 9 * i}\n')
            f.write(f'2 {2 + 9 * i} {6 + 9 * i}\n')
            f.write(f'2 {6 + 9 * i} {3 + 9 * i}\n')
            f.write(f'2 {3 + 9 * i} {7 + 9 * i}\n')
            f.write(f'2 {7 + 9 * i} {0 + 9 * i}\n')
            f.write(f'2 {4 + 9 * i} {8 + 9 * i}\n')
            f.write(f'2 {5 + 9 * i} {8 + 9 * i}\n')
            f.write(f'2 {6 + 9 * i} {8 + 9 * i}\n')
            f.write(f'2 {7 + 9 * i} {8 + 9 * i}\n')

        f.write(f'CELL_TYPES {12*len(my_miuras)}\n')
        for i in range(len(my_miuras)*12):
            f.write(f'3\n')
        f.write('\n')


def get_truss(nodes, panels):
    supports = [[0, 1, 1, 1]]  # not relevant
    loads = [[1, 0.1, 0, 0]]  # not relevant
    analyInputOpt = {'ZeroBend': 'AsIs',
                     'ModElastic': 4e9,  # pascal
                     'Poisson': 0.3,  # no unit
                     'Thickness': 0.08e-3,  # meters
                     'LScaleFactor': 2,  # no units
                     'maxIcr': 0,  # steps during arc-length
                     'MaterCalib': 'auto',
                     'verbose': True  # print icrm, error, lambda
                     }
    truss, _, _ = prepareData(nodes, panels, supports, loads, analyInputOpt)

    return truss


def saddle(x, y, a, b):
    # r = np.sqrt(x**2 + y**2)
    # theta = np.arctan2(x, y)
    # theta += 45/180*pi
    # x = r * cos(theta)
    # y = r * sin(theta)
    z = x**2 / a**2 - y**2 / b**2
    return z


def saddle_perpend(x, y, a, b):
    vector = np.array([-2*x / a**2, 2*y / b**2, 1])
    vector = vector / np.linalg.norm(vector)
    vector = vector.tolist()
    return vector


def dome(x, y, a, b):
    z = - x**2 / a**2 - y**2 / b**2
    return z


def dome_perpend(x, y, a, b):
    vector = np.array([2*x / a**2, 2*y / b**2, 1])
    vector = vector / np.linalg.norm(vector)
    vector = vector.tolist()
    return vector


def arch(x, y, a, b):
    z = -y**2 / b**2
    return z


def arch_perpend(x, y, a, b):
    vector = np.array([0, 2*y / b**2, 1])
    vector = vector / np.linalg.norm(vector)
    vector = vector.tolist()
    return vector


def meshgrid_data(shape_fun, length_square, a, b):
    # Make data
    small_number = 1e-6
    x = np.arange(-length_square/2, length_square/2 + small_number, 0.01)
    y = np.arange(-length_square/2, length_square/2 + small_number, 0.01)
    x, y = np.meshgrid(x, y)

    num_y, num_x = x.shape
    z = np.zeros((num_y, num_x))

    for j in range(num_y):
        for i in range(num_x):
            z[j][i] = shape_fun(x[j][i], y[j][i], a, b)
    return x, y, z


def plot_saddle(x, y, z, panels):
    # Plot the surface
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    ax.plot_surface(x, y, z, vmin=z.min() * 2, cmap=cm.Blues, alpha=0.8)

    colors = ['red'] * 4 + ['black'] * 5
    for panel in panels:
        ax.scatter(panel.xs, panel.ys, panel.zs, s=2, alpha=1, c=colors)
        for line in panel.lines:
            ax.plot(line[0], line[1], line[2], 'black')

    ax.set_xlim([-0.35, 0.35])
    ax.set_ylim([-0.35, 0.35])
    ax.set_zlim([-0.25, 0.25])

    ax.axis('equal')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    # plt.tight_layout()
    fig.savefig(root_dir + '/outputs/saddle1.pdf')


def make_miuras(squares_x, squares_y, shape_fun, length_square, a, b):
    miura_units = []
    for i in range(squares_x):
        for j in range(squares_y):
            x = -length_square / 2 + i / squares_x * length_square
            y = -length_square / 2 + j / squares_y * length_square
            z = shape_fun(x, y, a, b)
            node_se = Node(x, y, z)

            x = -length_square / 2 + (i+1) / squares_x * length_square
            y = -length_square / 2 + j / squares_y * length_square
            z = shape_fun(x, y, a, b)
            node_sw = Node(x, y, z)

            x = -length_square / 2 + i / squares_x * length_square
            y = -length_square / 2 + (j+1) / squares_y * length_square
            z = shape_fun(x, y, a, b)
            node_ne = Node(x, y, z)

            x = -length_square / 2 + (i+1) / squares_x * length_square
            y = -length_square / 2 + (j+1) / squares_y * length_square
            z = shape_fun(x, y, a, b)
            node_nw = Node(x, y, z)

            miura_units.append(Miura_unit(node_se, node_sw, node_ne, node_nw))

    return miura_units


def assemble_miura_units(miura_units, miura_units_x, miura_units_y):
    nnodes = (2 * miura_units_x + 1) * (2 * miura_units_y + 1)
    my_nodes = [None]*nnodes

    for node_nr in range(nnodes):
        # print(f'\n{node_nr}')
        column_nr = node_nr // (2 * miura_units_y + 1)
        if column_nr % 2 == 0:               # the column is even
            if node_nr // (2 * miura_units_y + 1) == 0:                     # the leftmost column
                leftmost_strip = list(range(miura_units_y))

                if node_nr % 2 == 0:                                            # the row is even
                    if node_nr == 0:                                                # the bottom row
                        # print('Bottom node in the leftmost column.')
                        my_nodes[node_nr] = miura_units[0].node_sw
                    elif node_nr == miura_units_y * 2:                              # the top row
                        # print('Top node in the leftmost column.')
                        my_nodes[node_nr] = miura_units[leftmost_strip[-1]].node_nw
                    else:                                                           # an interior row
                        # print('Node in an even interior row in the leftmost column.')
                        unit_index = (node_nr // 2) - 1
                        my_nodes[node_nr] = miura_units[leftmost_strip[unit_index]].node_nw
                else:                                                           # the row is odd
                    # print('Node in odd row in leftmost column.')
                    unit_index = node_nr // 2
                    my_nodes[node_nr] = miura_units[leftmost_strip[unit_index]].node_w

            elif column_nr == 2 * miura_units_x:   # the rightmost column
                rightmost_strip = list(range(miura_units_y*(miura_units_x-1), miura_units_y*miura_units_x))

                if node_nr % 2 == 0:                                            # the row is even
                    if node_nr == nnodes - 1 - (2 * miura_units_y):                 # the bottom row
                        # print('Bottom node in the rightmost column.')
                        my_nodes[node_nr] = miura_units[rightmost_strip[0]].node_se
                    elif node_nr == nnodes - 1:                                     # the top row
                        # print('Top node in the rightmost column.')
                        my_nodes[node_nr] = miura_units[rightmost_strip[-1]].node_ne
                    else:                                                           # an interior row
                        # print('Node in an even interior row in the rightmost column.')
                        unit_index = ((node_nr % (miura_units_y * 2 + 1)) // 2) - 1
                        my_nodes[node_nr] = miura_units[rightmost_strip[unit_index]].node_ne
                else:                                                           # the row is odd
                    # print('Node in odd row in rightmost column.')
                    unit_index = ((node_nr % (miura_units_y * 2 + 1)) // 2)
                    my_nodes[node_nr] = miura_units[rightmost_strip[unit_index]].node_e
            else:                                                           # an interior even column
                strip_to_left_nr = column_nr // 2 - 1
                strip_to_right_nr = column_nr // 2
                strip_to_left = list(range(strip_to_left_nr*miura_units_y, strip_to_right_nr*miura_units_y))
                strip_to_right = list(range(strip_to_right_nr*miura_units_y, (strip_to_right_nr+1)*miura_units_y))

                if node_nr % 2 == 0:                                            # the row is even
                    if node_nr % (2 * (2 * miura_units_y + 1)) == 0:                # the bottom row
                        # print('Bottom node in an even interior column.')
                        my_nodes[node_nr] = miura_units[strip_to_left[0]].node_se
                    elif node_nr % (2 * (2 * miura_units_y + 1)) == miura_units_y * 2:  # the top row
                        # print('Top node in an even interior column.')
                        my_nodes[node_nr] = miura_units[strip_to_left[-1]].node_ne
                    else:
                        # print('Interior node in an even interior row in an even interior column.')
                        unit_index = ((node_nr % (miura_units_y * 2 + 1)) // 2 - 1)
                        my_nodes[node_nr] = miura_units[strip_to_left[unit_index]].node_ne
                else:                                                           # the row is odd
                    # print('Node in an odd row in an even interior column.')
                    unit_index = ((node_nr % (miura_units_y * 2 + 1)) // 2)
                    average = average_of_nodes(miura_units[strip_to_left[unit_index]].node_e,
                                               miura_units[strip_to_right[unit_index]].node_w)
                    my_nodes[node_nr] = average
                    miura_units[strip_to_left[unit_index]].node_e = average
                    miura_units[strip_to_right[unit_index]].node_w = average

        else:                                                           # the column is odd
            strip_nr = column_nr // 2
            strip = list(range(strip_nr*miura_units_y, (strip_nr+1)*miura_units_y))
            if node_nr % 2 != 0:                                            # the row is even
                if node_nr % (2 * miura_units_y + 1) == 0:                      # the bottom row
                    # print('Bottom node in an odd column.')
                    my_nodes[node_nr] = miura_units[strip[0]].node_s
                elif node_nr % (2 * miura_units_y + 1) == miura_units_y * 2:    # the top row
                    # print('Top node in an odd column.')
                    my_nodes[node_nr] = miura_units[strip[-1]].node_n
                else:
                    # print('Interior node in an even row in an odd column.')
                    unit_index_1 = (node_nr % (miura_units_y * 2 + 1)) // 2 - 1
                    unit_index_2 = (node_nr % (miura_units_y * 2 + 1)) // 2
                    average = average_of_nodes(miura_units[strip[unit_index_1]].node_n,
                                               miura_units[strip[unit_index_2]].node_s)
                    my_nodes[node_nr] = average
                    miura_units[strip[unit_index_1]].node_n = average
                    miura_units[strip[unit_index_2]].node_s = average
            else:                                                           # the row is odd
                # print('Interior node in an odd row in an odd column.')
                unit_index = (node_nr % (miura_units_y * 2 + 1)) // 2
                my_nodes[node_nr] = miura_units[strip[unit_index]].node_m

    my_new_nodes = []
    for node in my_nodes:
        my_new_nodes.append((node.x, node.y, node.z))

    # panel construction is same as always, nodes already made, so parameters except size unnecessary -> filler values
    _, my_panels = miura_pattern((2*miura_units_x, 2*miura_units_y), 20, 1, 1, 20)
    return my_new_nodes, my_panels, miura_units


def make_triangulated_shape(shape_fun, length_square, a, b):
    nodes = []
    triangles = []

    small_number = 1e-6
    step = 0.05
    x = np.arange(-length_square/2, length_square/2 + small_number, step)
    y = np.arange(-length_square/2, length_square/2 + small_number, step)
    x, y = np.meshgrid(x, y)

    num_y, num_x = x.shape
    z = np.zeros((num_y, num_x))

    for j in range(num_y):
        for i in range(num_x):
            z[j][i] = shape_fun(x[j][i], y[j][i], a, b)
            nodes.append((x[j][i], y[j][i], z[j][i]))

    for i in range(num_x-1):
        for j in range(num_y-1):
            triangles.append((i*num_y+j, i*num_y+j+1, (i+1)*num_y+j+1))
            triangles.append((i*num_y+j, (i+1)*num_y+j, (i+1)*num_y+j+1))
    return nodes, triangles


@profile
def shape_creator(shape='saddle'):
    start_time = time.perf_counter()
    config = f'rigid/{shape}/shaping'
    create_output_folder(config)

    length_square = 0.5
    miura_units_x, miura_units_y = 4, 4  # number of miura units in x- and y-directions - normal is 4, 4
    # fraction of unit size that the miura units are stretched 'north' and 'up' - normal is 0.8, 0.6
    shift_factor_north, shift_factor_perp = 0.8, 0.6

    if shape == 'saddle':
        shape_fun, shape_perp_fun = saddle, saddle_perpend
        # determines the curvature of the saddle - no problems with 0.8, 0.8 - normal is 0.6, 0.6
        a, b = 0.8, 0.8
    elif shape == 'dome':
        shape_fun, shape_perp_fun = dome, dome_perpend
        # determines the curvature of the dome - no problems with 0.9, 0.9
        a, b = 0.9, 0.9
    elif shape == 'arch':
        shape_fun, shape_perp_fun = arch, arch_perpend
        # b determines the curvature of the valley - no problems with
        a, b = 1, 0.6
    else:
        raise Exception('invalid shape')

    # smooth saddle (fig 0)
    nodes_smooth, triangles_smooth = make_triangulated_shape(shape_fun, length_square, a, b)
    make_vtk_file(nodes_smooth, triangles_smooth, config, 0,
                  filepath=f'{root_dir}/outputs/{config}/fig0.vtk')

    my_miuras = make_miuras(miura_units_x, miura_units_y, shape_fun, length_square, a, b)

    # quads placed on top (fig 1)
    make_vtk_4_nodes(my_miuras, f'{root_dir}/outputs/{config}/fig1.vtk', config)

    for my_miura in my_miuras:
        my_miura.add_necessary_nodes()

    # quads with all necessary nodes (fig 2)
    make_vtk_9_nodes(my_miuras, f'{root_dir}/outputs/{config}/fig2.vtk', config)

    for my_miura in my_miuras:
        # my_miura.move_to_miura_like()
        my_miura.move_to_miura_like_2(shape_perp_fun, length_square, miura_units_y,
                                      shift_factor_north, shift_factor_perp, a, b)
        # my_miura.make_plot_points()
        # my_miura.make_plot_lines()
    # plot_saddle(*meshgrid_data(saddle), my_miuras)

    # units moved to Miura-like (fig 3)
    make_vtk_9_nodes(my_miuras, f'{root_dir}/outputs/{config}/fig3.vtk', config)

    nodes, panels, my_miuras = assemble_miura_units(my_miuras, miura_units_x, miura_units_y)

    # everything connected (fig 5)
    make_vtk_9_nodes(my_miuras, f'{root_dir}/outputs/{config}/fig5.vtk', config)

    truss = get_truss(nodes, panels)
    make_vtk_file(truss['nodes'], truss['trigl'], config, 0,
                  filepath=f'{root_dir}/outputs/{config}/fig6.vtk')

    end_time = time.perf_counter()
    print(f'matching time: {round(end_time - start_time, 0)} seconds')

    return nodes, panels, miura_units_x, miura_units_y


if __name__ == '__main__':
    shape_creator('arch')
