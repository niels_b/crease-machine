import csv
import time
from math import pi
import numpy as np
from scipy.optimize import minimize, NonlinearConstraint
from rigid.match_shape import shape_creator, get_truss
from core.directories import root_dir
from core.help_functions import make_vtk_file, create_output_folder
from core.part1_prepare_data import prepareData

vec_len = np.linalg.norm


def list_of_pattern_and_cross_edges(miura_units_x, miura_units_y):
    vertical_edges = []
    for i in range(miura_units_x * 2 + 1):
        for j in range(miura_units_y * 2):
            vertical_edges.append([i * (miura_units_y * 2 + 1) + j,
                                   i * (miura_units_y * 2 + 1) + j + 1])

    horizontal_edges = []
    for i in range(miura_units_x * 2):
        for j in range(miura_units_y * 2 + 1):
            horizontal_edges.append([i * (miura_units_y * 2 + 1) + j,
                                     (i + 1) * (miura_units_y * 2 + 1) + j])

    southwest_to_northeast_edges = []
    for i in range(miura_units_x * 2):
        for j in range(miura_units_y * 2):
            southwest_to_northeast_edges.append([i * (miura_units_y * 2 + 1) + j,
                                                 (i + 1) * (miura_units_y * 2 + 1) + j + 1])

    northwest_to_southeast_edges = []
    for i in range(miura_units_x * 2):
        for j in range(1, miura_units_y * 2 + 1):
            northwest_to_southeast_edges.append([i * (miura_units_y * 2 + 1) + j,
                                                 (i + 1) * (miura_units_y * 2 + 1) + j - 1])

    all_edges = vertical_edges + horizontal_edges + southwest_to_northeast_edges + northwest_to_southeast_edges
    return all_edges


def list_of_interior_vertices(nodes, miura_units_x, miura_units_y):
    nnodes = len(nodes)
    all_nodes = list(range(nnodes))
    nodes_to_remove = []
    for i in range(miura_units_y * 2 + 1):
        nodes_to_remove.append(i)
    for i in range(nnodes - (miura_units_y * 2 + 1), nnodes):
        nodes_to_remove.append(i)
    for i in range(miura_units_x * 2 + 1):
        nodes_to_remove.append(i * (miura_units_y * 2 + 1))
        nodes_to_remove.append(i * (miura_units_y * 2 + 1) + miura_units_y * 2)

    for node_to_remove in nodes_to_remove:
        if node_to_remove in all_nodes:
            all_nodes.remove(node_to_remove)

    interior_nodes = all_nodes

    neighbours_clockwise = []
    for interior_node in interior_nodes:
        neighbours_clockwise.append((interior_node - 1,
                                     interior_node - (miura_units_y * 2 + 1),
                                     interior_node + 1,
                                     interior_node + (miura_units_y * 2 + 1)))

    return interior_nodes, neighbours_clockwise


def list_of_clockwise_panels(panels):
    clockwise_panels = []
    for panel in panels:
        clockwise_panels.append((panel[3], panel[2], panel[1], panel[0]))
    return clockwise_panels


def list_of_fixed_and_free_nodes(nodes, miura_units_x, miura_units_y):
    fixed_nodes = []
    for i in range(miura_units_x * 2 + 1):
        if i % 2 == 0:  # even column
            for j in range(miura_units_y * 2 + 1):
                if j % 2 == 0:  # even row
                    fixed_nodes.append(i * (miura_units_y * 2 + 1) + j)

    free_nodes = list(set(range(len(nodes))) - set(fixed_nodes))

    return fixed_nodes, free_nodes


def create_initial_design(nodes, free_nodes):
    x0 = []
    for free_node in free_nodes:
        x0.append(nodes[free_node][0])
        x0.append(nodes[free_node][1])
        x0.append(nodes[free_node][2])
    x0 = np.array(x0)
    return x0


def initial_edge_lengths(nodes, edges):
    initial_lengths = []
    for edge in edges:
        node0, node1 = nodes[edge[0]], nodes[edge[1]]
        length = np.sqrt((node0[0] - node1[0])**2 + (node0[1] - node1[1])**2 + (node0[2] - node1[2])**2)
        initial_lengths.append(length)
    return initial_lengths


def get_node_coord(node_nr, free_nodes, initial_node_coords, free_node_coords):
    if node_nr in free_nodes:
        node_index = free_nodes.index(node_nr)
        node = free_node_coords[node_index * 3: node_index * 3 + 3]
    else:
        node = initial_node_coords[node_nr]
    return node


def objective(initial_node_coords, free_nodes, edges, initial_lengths, free_node_coords):
    total_change_in_length = 0
    for i, edge in enumerate(edges):

        # initial length
        L0 = initial_lengths[i]

        # get coords of new nodes
        node0 = get_node_coord(edge[0], free_nodes, initial_node_coords, free_node_coords)
        node1 = get_node_coord(edge[1], free_nodes, initial_node_coords, free_node_coords)

        # current length
        L = np.sqrt((node0[0] - node1[0])**2 + (node0[1] - node1[1])**2 + (node0[2] - node1[2])**2)

        change_in_length = 1 / (2 * L0) * (L - L0)**2
        total_change_in_length += change_in_length

    return total_change_in_length


def constraints(initial_node_coords, free_nodes, interior_nodes, neighbours_clockwise, clockwise_panels,
                free_node_coords):
    developability_constraints = []

    def angle(vec0, vec1):
        return 2 * np.arctan2(vec_len(np.cross(vec0, vec1)),
                              vec_len(vec0) * vec_len(vec1) + np.dot(vec0, vec1)
                              )

    for i, interior_node in enumerate(interior_nodes):
        p_n0, p_n1, p_n2, p_n3 = neighbours_clockwise[i]

        p_i = np.array(get_node_coord(interior_node, free_nodes, initial_node_coords, free_node_coords))
        p_n0 = np.array(get_node_coord(p_n0, free_nodes, initial_node_coords, free_node_coords))
        p_n1 = np.array(get_node_coord(p_n1, free_nodes, initial_node_coords, free_node_coords))
        p_n2 = np.array(get_node_coord(p_n2, free_nodes, initial_node_coords, free_node_coords))
        p_n3 = np.array(get_node_coord(p_n3, free_nodes, initial_node_coords, free_node_coords))

        constr = (2 * pi
                  - angle(p_n0 - p_i, p_n1 - p_i)
                  - angle(p_n1 - p_i, p_n2 - p_i)
                  - angle(p_n2 - p_i, p_n3 - p_i)
                  - angle(p_n3 - p_i, p_n0 - p_i))

        developability_constraints.append(constr)

    planarity_constraints = []
    for panel in clockwise_panels:
        p_a = np.array(get_node_coord(panel[0], free_nodes, initial_node_coords, free_node_coords))
        p_b = np.array(get_node_coord(panel[1], free_nodes, initial_node_coords, free_node_coords))
        p_c = np.array(get_node_coord(panel[2], free_nodes, initial_node_coords, free_node_coords))
        p_d = np.array(get_node_coord(panel[3], free_nodes, initial_node_coords, free_node_coords))

        tetrahedal_volume = np.dot(np.cross(p_b - p_a, p_c - p_a), (p_d - p_a))

        planarity_constraints.append(tetrahedal_volume)

    all_constraints = developability_constraints + planarity_constraints
    return all_constraints


def objective_sens(initial_node_coords, free_nodes, edges, initial_lengths, free_node_coords):
    objec_sens = np.zeros(len(free_nodes)*3)
    for p in range(len(free_nodes)):
        node_nr = free_nodes[p]

        connected_edges = []    # make list of the edges that are connected to the free node
        for i, edge in enumerate(edges):
            if node_nr in edge:
                if node_nr == edge[0]:
                    connected_edges.append((i, edge, 'left'))
                elif node_nr == edge[1]:
                    connected_edges.append((i, edge, 'right'))
                else:
                    raise Exception('Node is not in the edge but we thought it was.')

        for xyz in range(3):  # go over the x, y, and z coordinates of the free node
            df_da = 0
            for i, edge, side in connected_edges:
                L0 = initial_lengths[i]

                # get coords of new nodes
                node0 = get_node_coord(edge[0], free_nodes, initial_node_coords, free_node_coords)
                node1 = get_node_coord(edge[1], free_nodes, initial_node_coords, free_node_coords)

                # current length
                L = np.sqrt((node0[0] - node1[0])**2 + (node0[1] - node1[1])**2 + (node0[2] - node1[2])**2)

                if side == 'left':
                    dL_da = 1 / L * (node0[xyz] - node1[xyz])
                elif side == 'right':
                    dL_da = -1 / L * (node0[xyz] - node1[xyz])  # notice the minus sign
                else:
                    raise Exception('something went wrong')

                df_da += (L / L0 - 1) * dL_da   # add the contribution of this edge to the sensitivity

            objec_sens[3 * p + xyz] = df_da

    return objec_sens


def constraints_sens(initial_node_coords, free_nodes, interior_nodes, neighbours_clockwise, clockwise_panels,
                     free_node_coords):
    m = len(interior_nodes) + len(clockwise_panels)     # developability and planarity constraints, respectively
    n = 3 * len(free_nodes)
    jac = np.zeros((m, n))

    # developability constraints
    def get_dvcrossw_da(my_node_nr, my_interior_node, my_neighbour_index0, my_neighbour_index1,
                        my_pi, my_pn0, my_pn1, my_xyz, my_v, my_w):
        my_xyzs = [0, 1, 2, 0, 1]
        a, b, c = my_xyzs[my_xyz], my_xyzs[my_xyz + 1], my_xyzs[my_xyz + 2]

        if my_node_nr == my_interior_node:
            return ((((my_pn0[c] - my_pi[c]) * (my_pn1[a] - my_pi[a]) -
                      (my_pn1[c] - my_pi[c]) * (my_pn0[a] - my_pi[a])) *
                     (-(my_pn0[c] - my_pi[c]) + (my_pn1[c] - my_pi[c]))) +
                    (((my_pn0[a] - my_pi[a]) * (my_pn1[b] - my_pi[b]) -
                      (my_pn1[a] - my_pi[a]) * (my_pn0[b] - my_pi[b])) *
                     ((my_pn0[b] - my_pi[b]) - (my_pn1[b] - my_pi[b])))) / vec_len(np.cross(my_v, my_w))

        elif my_node_nr == my_neighbour_index0:
            return ((((my_pn0[c] - my_pi[c]) * (my_pn1[a] - my_pi[a]) -
                      (my_pn1[c] - my_pi[c]) * (my_pn0[a] - my_pi[a])) *
                     (-(my_pn1[c] - my_pi[c]))) +
                    (((my_pn0[a] - my_pi[a]) * (my_pn1[b] - my_pi[b]) -
                      (my_pn1[a] - my_pi[a]) * (my_pn0[b] - my_pi[b])) *
                     (my_pn1[b] - my_pi[b]))) / vec_len(np.cross(my_v, my_w))

        elif my_node_nr == my_neighbour_index1:
            return ((((my_pn0[c] - my_pi[c]) * (my_pn1[a] - my_pi[a]) -
                      (my_pn1[c] - my_pi[c]) * (my_pn0[a] - my_pi[a])) *
                     (my_pn0[c] - my_pi[c])) +
                    (((my_pn0[a] - my_pi[a]) * (my_pn1[b] - my_pi[b]) -
                      (my_pn1[a] - my_pi[a]) * (my_pn0[b] - my_pi[b])) *
                     (-(my_pn0[b] - my_pi[b])))) / vec_len(np.cross(my_v, my_w))

        else:
            return 0

    def get_dvwvdotw_da(my_node_nr, my_interior_node, my_neighbour_index0, my_neighbour_index1,
                        my_pi, my_pn0, my_pn1, my_xyz, my_v, my_w):
        my_xyzs = [0, 1, 2, 0, 1]
        a, b, c = my_xyzs[my_xyz], my_xyzs[my_xyz + 1], my_xyzs[my_xyz + 2]

        if my_node_nr == my_interior_node:
            return (-(my_pn0[a] - p_i[a]) * (vec_len(my_w) / vec_len(my_v) + 1) -
                    (my_pn1[a] - my_pi[a]) * (vec_len(my_v) / vec_len(my_w) + 1))

        elif my_node_nr == my_neighbour_index0:
            return (my_pn0[a] - p_i[a]) * (vec_len(my_w) / vec_len(my_v)) + (my_pn1[a] - my_pi[a])

        elif my_node_nr == my_neighbour_index1:
            return (my_pn1[a] - p_i[a]) * (vec_len(my_v) / vec_len(my_w)) + (my_pn0[a] - my_pi[a])

        else:
            return 0

    for i, interior_node in enumerate(interior_nodes):
        p_n0_ind, p_n1_ind, p_n2_ind, p_n3_ind = neighbours_clockwise[i]
        neighbours_indices = [p_n0_ind, p_n1_ind, p_n2_ind, p_n3_ind, p_n0_ind]

        p_i = np.array(get_node_coord(interior_node, free_nodes, initial_node_coords, free_node_coords))
        p_n0 = np.array(get_node_coord(p_n0_ind, free_nodes, initial_node_coords, free_node_coords))
        p_n1 = np.array(get_node_coord(p_n1_ind, free_nodes, initial_node_coords, free_node_coords))
        p_n2 = np.array(get_node_coord(p_n2_ind, free_nodes, initial_node_coords, free_node_coords))
        p_n3 = np.array(get_node_coord(p_n3_ind, free_nodes, initial_node_coords, free_node_coords))
        p_n = [p_n0, p_n1, p_n2, p_n3, p_n0]

        for j, free_node in enumerate(free_nodes):
            if free_node in [interior_node] or free_node in neighbours_indices:    # node is in the constraint
                for xyz in range(3):
                    dg_da = 0
                    for u in range(4):
                        xi, yi, zi = p_i[0], p_i[1], p_i[2]    # x, y, z coordinates of p_i
                        x_nj0, y_nj0, z_nj0 = p_n[u][0], p_n[u][1], p_n[u][2]   # x, y, z coordinates of p_nj
                        x_nj1, y_nj1, z_nj1 = p_n[u+1][0], p_n[u+1][1], p_n[u+1][2]     # x, y, z coordinates of p_nj+1
                        v = np.array([x_nj0 - xi, y_nj0 - yi, z_nj0 - zi])
                        w = np.array([x_nj1 - xi, y_nj1 - yi, z_nj1 - zi])

                        neighbour_index0, neighbour_index1 = neighbours_indices[u], neighbours_indices[u+1]
                        dvcrossw_da = get_dvcrossw_da(free_node, interior_node, neighbour_index0, neighbour_index1,
                                                      p_i, p_n[u], p_n[u+1], xyz, v, w)     # derivative of ||v x w||
                        dvwvdotw_da = get_dvwvdotw_da(free_node, interior_node, neighbour_index0, neighbour_index1,
                                                      p_i, p_n[u], p_n[u+1], xyz, v, w)
                        # derivative of (||v|| ||w|| + v • w

                        datan2_da = (1 / (1 + (vec_len(np.cross(v, w)) / (vec_len(v) * vec_len(w) + np.dot(v, w)))**2) *
                                     ((vec_len(v) * vec_len(w) + np.dot(v, w)) * dvcrossw_da - vec_len(np.cross(v, w)) *
                                     dvwvdotw_da) / (vec_len(v) * vec_len(w) + np.dot(v, w))**2)

                        dg_da += -2 * datan2_da  # add the contributions from the four triplets
                    jac[i, 3 * j + xyz] = dg_da     # add the sensitivity to the jacobian

            else:   # node is not in the constraint
                for xyz in range(3):
                    jac[i, 3 * j + xyz] += 0

    # planarity constraints
    for i, panel in enumerate(clockwise_panels):
        p_a = np.array(get_node_coord(panel[0], free_nodes, initial_node_coords, free_node_coords))
        p_b = np.array(get_node_coord(panel[1], free_nodes, initial_node_coords, free_node_coords))
        p_c = np.array(get_node_coord(panel[2], free_nodes, initial_node_coords, free_node_coords))
        p_d = np.array(get_node_coord(panel[3], free_nodes, initial_node_coords, free_node_coords))

        xyzs = [0, 1, 2, 0, 1]
        for j, free_node in enumerate(free_nodes):
            if free_node in panel:  # node is in the constraint

                if free_node == panel[0]:       # we're looking at p_a
                    for xyz in range(3):
                        x, y, z = xyzs[xyz], xyzs[xyz+1], xyzs[xyz+2]
                        dg_da = (- p_b[y] * p_c[z] + p_b[y] * p_d[z]
                                 + p_c[y] * p_b[z] - p_c[y] * p_d[z]
                                 - p_d[y] * p_b[z] + p_d[y] * p_c[z])
                        jac[len(interior_nodes)+i, 3 * j + xyz] = dg_da

                elif free_node == panel[1]:     # we're looking at p_b
                    for xyz in range(3):
                        x, y, z = xyzs[xyz], xyzs[xyz+1], xyzs[xyz+2]

                        dg_da = (+ p_a[y] * p_c[z] - p_a[y] * p_d[z]
                                 - p_c[y] * p_a[z] + p_c[y] * p_d[z]
                                 + p_d[y] * p_a[z] - p_d[y] * p_c[z])
                        jac[len(interior_nodes) + i, 3 * j + xyz] = dg_da

                elif free_node == panel[2]:     # we're looking at p_c
                    for xyz in range(3):
                        x, y, z = xyzs[xyz], xyzs[xyz + 1], xyzs[xyz + 2]

                        dg_da = (- p_a[y] * p_b[z] + p_a[y] * p_d[z]
                                 + p_b[y] * p_a[z] - p_b[y] * p_d[z]
                                 - p_d[y] * p_a[z] + p_d[y] * p_b[z])
                        jac[len(interior_nodes) + i, 3 * j + xyz] = dg_da

                elif free_node == panel[3]:     # we're looking at p_d
                    for xyz in range(3):
                        x, y, z = xyzs[xyz], xyzs[xyz + 1], xyzs[xyz + 2]

                        dg_da = (+ p_a[y] * p_b[z] - p_a[y] * p_c[z]
                                 - p_b[y] * p_a[z] + p_b[y] * p_c[z]
                                 + p_c[y] * p_a[z] - p_c[y] * p_b[z])
                        jac[len(interior_nodes) + i, 3 * j + xyz] = dg_da
                else:
                    raise Exception('we thought the node was in the panel but it turns out it_s not')

    # print(jac)
    return jac


def concave_quadrilateral_constraint_initial_signs(initial_node_coords, panels):
    initial_signs = [None]*(len(panels)*4)

    for i, panel in enumerate(panels):
        p = [np.array(initial_node_coords[panel[w]]) for w in [0, 1, 2, 3]]

        for a in [0, 1, 2, 3]:
            my_new_list = [3, 0, 1, 2, 3, 0]
            b, c = my_new_list[a+2], my_new_list[a]

        # we take the z-component of the cross product; if its sign has flipped, a triangle in the quad has prob flipped
            new_entry = (p[a][0]*p[b][1] - p[a][0]*p[c][1] - p[b][0]*p[a][1] +
                         p[b][0]*p[c][1] + p[c][0]*p[a][1] - p[c][0]*p[b][1])

            if new_entry < 0:
                initial_signs[i*4 + a] = 'minus'
            else:
                initial_signs[i*4 + a] = 'plus'

    return initial_signs


def concave_quadrilateral_constraint(initial_node_coords, free_nodes, panels, initial_signs, free_node_coords):
    cons = np.zeros(len(panels) * 4)

    for i, panel in enumerate(panels):
        p = [np.array(get_node_coord(panel[w], free_nodes, initial_node_coords, free_node_coords))
             for w in [0, 1, 2, 3]]

        for a in [0, 1, 2, 3]:
            my_new_list = [3, 0, 1, 2, 3, 0]
            b, c = my_new_list[a+2], my_new_list[a]

        # we take the z-component of the cross product; if its sign has flipped, a triangle in the quad has prob flipped
            new_entry = (p[a][0]*p[b][1] - p[a][0]*p[c][1] - p[b][0]*p[a][1] +
                         p[b][0]*p[c][1] + p[c][0]*p[a][1] - p[c][0]*p[b][1])

            if initial_signs[i*4 + a] == 'minus':
                new_entry = -1*new_entry

            cons[i*4 + a] = new_entry

    return cons


def concave_quadrilateral_constraint_sens(initial_node_coords, free_nodes, panels, initial_signs, free_node_coords):
    m = len(panels) * 4
    n = len(free_nodes) * 3
    jac = np.zeros((m, n))

    for i, panel in enumerate(panels):
        p = [np.array(get_node_coord(panel[w], free_nodes, initial_node_coords, free_node_coords))
             for w in [0, 1, 2, 3]]

        for a in [0, 1, 2, 3]:
            my_new_list = [3, 0, 1, 2, 3, 0]
            b, c = my_new_list[a+2], my_new_list[a]

            if initial_signs[i*4 + a] == 'minus':
                my_sign = -1
            else:
                my_sign = 1

            for j, free_node in enumerate(free_nodes):  # loop over the design variables
                if free_node in [panel[a], panel[b], panel[c]]:     # the free node is in the constraint
                    if free_node == panel[a]:
                        dg_dax = p[b][1] - p[c][1]
                        dg_day = -p[b][0] + p[c][0]
                        dg_daz = 0
                        dg_da = my_sign * np.array([dg_dax, dg_day, dg_daz])
                        jac[i * 4 + a, j * 3:j * 3 + 3] = dg_da
                    elif free_node == panel[b]:
                        dg_dbx = -p[a][1] + p[c][1]
                        dg_dby = p[a][0] - p[c][0]
                        dg_dbz = 0
                        dg_db = my_sign * np.array([dg_dbx, dg_dby, dg_dbz])
                        jac[i * 4 + a, j * 3:j * 3 + 3] = dg_db
                    elif free_node == panel[c]:
                        dg_dcx = p[a][1] - p[b][1]
                        dg_dcy = -p[a][0] + p[b][0]
                        dg_dcz = 0
                        dg_dc = my_sign * np.array([dg_dcx, dg_dcy, dg_dcz])
                        jac[i * 4 + a, j * 3:j * 3 + 3] = dg_dc
                    else:
                        raise Exception('is it in the panel or not?')

    return jac


def concave_quadrilateral_constraint_2(initial_node_coords, free_nodes, panels, free_node_coords):
    cons = np.zeros(len(panels) * 4)

    for i, panel in enumerate(panels):
        p = [np.array(get_node_coord(panel[w], free_nodes, initial_node_coords, free_node_coords))
             for w in [0, 1, 2, 3]]

        for a in [0, 1, 2, 3]:
            my_new_list = [0, 1, 2, 3, 0, 1, 2]
            b, c, d = my_new_list[a + 1], my_new_list[a + 2], my_new_list[a + 3]

            AB = vec_len(p[b] - p[a])
            AD = vec_len(p[d] - p[a])
            AC = vec_len(p[c] - p[a])
            if AB < AD:
                cons[i * 4 + a] = AB**2 - AC**2
            else:
                cons[i * 4 + a] = AD**2 - AC**2

    return cons


def concave_quadrilateral_constraint_sens_2(initial_node_coords, free_nodes, panels, free_node_coords):
    m = len(panels) * 4
    n = len(free_nodes) * 3
    jac = np.zeros((m, n))

    for i, panel in enumerate(panels):
        p = [np.array(get_node_coord(panel[w], free_nodes, initial_node_coords, free_node_coords))
             for w in [0, 1, 2, 3]]

        for a in [0, 1, 2, 3]:
            my_new_list = [0, 1, 2, 3, 0, 1, 2]
            b, c, d = my_new_list[a + 1], my_new_list[a + 2], my_new_list[a + 3]

            AB = vec_len(p[b] - p[a])
            AD = vec_len(p[d] - p[a])

            if AB < AD:
                b = b
            else:
                b = d   # so we use the correct value in the following calculations

            for j, free_node in enumerate(free_nodes):  # loop over the design variables
                if free_node in [panel[a], panel[b], panel[c]]:  # the free node is in the constraint
                    if free_node == panel[a]:
                        dg_dax = -2*p[b][0] + 2*p[c][0]
                        dg_day = -2*p[b][1] + 2*p[c][1]
                        dg_daz = -2*p[b][2] + 2*p[c][2]
                        dg_da = np.array([dg_dax, dg_day, dg_daz])
                        jac[i * 4 + a, j * 3:j * 3 + 3] = dg_da
                    elif free_node == panel[b]:
                        dg_dbx = 2*p[b][0] - 2*p[a][0]
                        dg_dby = 2*p[b][1] - 2*p[a][1]
                        dg_dbz = 2*p[b][2] - 2*p[a][2]
                        dg_db = np.array([dg_dbx, dg_dby, dg_dbz])
                        jac[i * 4 + a, j * 3:j * 3 + 3] = dg_db
                    elif free_node == panel[c]:
                        dg_dcx = -2*p[c][0] + 2*p[a][0]
                        dg_dcy = -2*p[c][1] + 2*p[a][1]
                        dg_dcz = -2*p[c][2] + 2*p[a][2]
                        dg_dc = np.array([dg_dcx, dg_dcy, dg_dcz])
                        jac[i * 4 + a, j * 3:j * 3 + 3] = dg_dc
                    else:
                        raise Exception('is it in the panel or not?')

    return jac


def create_results_csv(x0, f0, config):
    filename = f'{root_dir}/outputs/{config}/minimize_results.csv'
    with open(filename, 'w') as file:
        my_writer = csv.DictWriter(file, fieldnames=['x', 'f'], delimiter=',')
        my_writer.writeheader()
        # my_writer.writerow({'x': list(x0), 'f': f0})


def add_intermediate_optimization_results(nodes, panels, free_nodes, config, _, intermediate_result):
    filename = f'{root_dir}/outputs/{config}/minimize_results.csv'

    with open(filename, 'r') as file:
        temp_reader = csv.reader(file, delimiter=',')
        iteration = sum(1 for _ in temp_reader) - 1

    with open(filename, 'a') as file:
        my_writer = csv.DictWriter(file, fieldnames=['x', 'f'], delimiter=',')
        my_writer.writerow({'x': list(intermediate_result['x']), 'f': intermediate_result['fun']})

    optimal_design = list(intermediate_result['x'])

    optimal_nodes = [tuple(get_node_coord(node_nr, free_nodes, nodes, optimal_design)) for node_nr in range(len(nodes))]

    truss = get_truss(optimal_nodes, panels)

    # relevant_trigls = [truss['trigl'][x] for x in [19, 20, 23, 42]]
    #
    # make_vtk_file(truss['nodes'], relevant_trigls, config, 0,
    #               filepath=f'{root_dir}/outputs/{config}/{config}-optimal-selection-step-{iteration}.vtk')
    #
    make_vtk_file(truss['nodes'], truss['trigl'], config, 0,
                  filepath=f'{root_dir}/outputs/{config}/optimal{iteration}.vtk')


def total_optimization(shape='saddle'):
    config = f'rigid/{shape}/developing'
    create_output_folder(config)
    nodes, panels, miura_units_x, miura_units_y = shape_creator(shape)
    start_time = time.perf_counter()
    edges = list_of_pattern_and_cross_edges(miura_units_x, miura_units_y)
    interior_nodes, neighbours_clockwise = list_of_interior_vertices(nodes, miura_units_x, miura_units_y)
    clockwise_panels = list_of_clockwise_panels(panels)
    fixed_nodes, free_nodes = list_of_fixed_and_free_nodes(nodes, miura_units_x, miura_units_y)
    initial_lengths = initial_edge_lengths(nodes, edges)
    x0 = create_initial_design(nodes, free_nodes)
    initial_concave_signs = concave_quadrilateral_constraint_initial_signs(nodes, panels)

    def f(*args):
        return objective(nodes, free_nodes, edges, initial_lengths, *args)
        # return 0

    def g(*args):
        return constraints(nodes, free_nodes, interior_nodes, neighbours_clockwise, clockwise_panels, *args)

    def df_dx(*args):
        return objective_sens(nodes, free_nodes, edges, initial_lengths, *args)
        # return 0

    def dg_dx(*args):
        return constraints_sens(nodes, free_nodes, interior_nodes, neighbours_clockwise, clockwise_panels, *args)

    def concave(*args):
        # return concave_quadrilateral_constraint(nodes, free_nodes, panels, initial_concave_signs, *args)
        return concave_quadrilateral_constraint_2(nodes, free_nodes, panels, *args)

    def concave_dx(*args):
        # return concave_quadrilateral_constraint_sens(nodes, free_nodes, panels, initial_concave_signs, *args)
        return concave_quadrilateral_constraint_sens_2(nodes, free_nodes, panels, *args)

    def add_intermediate_results(*args):
        return add_intermediate_optimization_results(nodes, panels, free_nodes, config, *args)

    f0 = f(x0)
    g0 = g(x0)

    print(f'constraints total at start: {sum([abs(item) for item in g0])}')

    create_results_csv(x0, f0, config)

    final_design = minimize(f, x0, method='trust-constr',
                            constraints=
                            [
                             NonlinearConstraint(g, 0, 0, jac=dg_dx),
                             # NonlinearConstraint(concave, 0, np.inf, jac=concave_dx)
                            ],
                            jac=df_dx,
                            callback=add_intermediate_results,
                            options={'verbose': 3, 'maxiter': 50, 'initial_tr_radius': 0.7})

    optimal_objective = final_design['fun']
    optimal_design = final_design['x']
    optimal_constraints = final_design['constr'][0]

    print(f'constraints total at end: {sum([abs(item) for item in optimal_constraints])}')

    optimal_nodes = [tuple(get_node_coord(node_nr, free_nodes, nodes, optimal_design)) for node_nr in range(len(nodes))]

    # print(final_design)
    # print(f'optimal objective: {optimal_objective}')
    # print(f'optimal design: {optimal_design}')
    # print(f'optimal constraints: {optimal_constraints}')
    # print(f'optimal nodes: {optimal_nodes}')

    truss = get_truss(optimal_nodes, panels)
    make_vtk_file(truss['nodes'], truss['trigl'], config, 0,
                  filepath=f'{root_dir}/outputs/{config}/foldable.vtk')

    end_time = time.perf_counter()
    print(f'developing time: {round(end_time - start_time, 0)} seconds')

    return optimal_nodes, panels, miura_units_x, miura_units_y


if __name__ == '__main__':
    total_optimization('arch')
