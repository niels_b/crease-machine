import csv
import ast
from PIL import Image
import subprocess
import matplotlib.pyplot as plt
from core.surface_matching import make_nodes_from_coords
from core.directories import root_dir
from core.help_functions import read_from_input, set_background

shape = 'saddle'
input_file = f'{shape}-opt'
output_path = f'{root_dir}/outputs/{shape}-opt'

_, panels, _, _, _, _ = read_from_input(input_file)

designs = []
with open(f'{output_path}/minimize-results.csv') as f:
    my_reader = csv.DictReader(f, delimiter=',')
    for row in my_reader:
        my_design = ast.literal_eval(row['x'])
        designs.append(my_design)

design = designs[-1]
half_nodes = list(design)[:-3]
all_nodes = make_nodes_from_coords(half_nodes, (8, 8), True, True)
flat_nodes = [(node[0], node[1]) for node in all_nodes]

background_colour = 32   # dark mode is 32
background_colour_vid = 34   # dark mode is 34

set_background(background_colour)
px = 1 / plt.rcParams['figure.dpi']
fig = plt.figure(figsize=(1380 * px, 1080 * px))
ax = fig.add_subplot(111)

for panel in panels:
    panel_xs = []
    panel_ys = []
    for indx in panel:
        panel_xs.append(flat_nodes[indx][0])
        panel_ys.append(flat_nodes[indx][1])

    plt.scatter(panel_xs, panel_ys, s=4, color='white')
    panel_xs.append(panel_xs[0])
    panel_ys.append(panel_ys[0])
    plt.plot(panel_xs, panel_ys, color='white')

xticks, yticks = [], []
plt.xticks(xticks, xticks)
plt.yticks(yticks, yticks)
xlim = plt.xlim()
ylim = plt.ylim()
ax.set_aspect('equal')
ax.axis('off')
plt.tight_layout()
plt.savefig(f'{output_path}/itr-flats/final-flat.pdf')
plt.close()

set_background(background_colour_vid)
# set_background(background_colour)
px = 1 / plt.rcParams['figure.dpi']


for i, design in enumerate(designs):
    first_design = designs[0]
    half_nodes = list(first_design)[:-3]
    all_nodes = make_nodes_from_coords(half_nodes, (8, 8), True, True)
    flat_nodes = [(node[0], node[1]) for node in all_nodes]

    fig = plt.figure(figsize=(1380 * px, 1080 * px))
    ax = fig.add_subplot(111)

    for panel in panels:
        panel_xs = []
        panel_ys = []
        for indx in panel:
            panel_xs.append(flat_nodes[indx][0])
            panel_ys.append(flat_nodes[indx][1])

        plt.scatter(panel_xs, panel_ys, s=4, color='#85E691')
        panel_xs.append(panel_xs[0])
        panel_ys.append(panel_ys[0])
        plt.plot(panel_xs, panel_ys, color='#85E691')

    half_nodes = list(design)[:-3]
    all_nodes = make_nodes_from_coords(half_nodes, (8, 8), True, True)
    flat_nodes = [(node[0], node[1]) for node in all_nodes]

    for panel in panels:
        panel_xs = []
        panel_ys = []
        for indx in panel:
            panel_xs.append(flat_nodes[indx][0])
            panel_ys.append(flat_nodes[indx][1])

        plt.scatter(panel_xs, panel_ys, s=4, color='white')
        panel_xs.append(panel_xs[0])
        panel_ys.append(panel_ys[0])
        plt.plot(panel_xs, panel_ys, color='white')

    xticks, yticks = [], []
    plt.xticks(xticks, xticks)
    plt.yticks(yticks, yticks)
    plt.xlim(xlim)
    plt.ylim(ylim)
    ax.set_aspect('equal')
    ax.axis('off')
    plt.tight_layout()
    plt.savefig(f'{output_path}/itr-flats/frame{i}.png')
    plt.close()

video_length = 6    # number of seconds
fps = str(len(designs)//video_length)     # frames per second, match this with paraview export
if fps == '0':
    fps = '1'
width, height = Image.open(f'{output_path}/itr-flats/frame0.png').size
width_height = f'{width}x{height}'
command = (f'cd; ffmpeg -hide_banner -loglevel error -r {fps} -f image2 -s {width_height} -i '
           f'{output_path}/itr-flats/frame%d.png '
           f'-vcodec libx264 -crf 15 -pix_fmt yuv420p '
           f'{output_path}/itr-flats/flats-video.mp4')
subprocess.run(command, shell=True)
