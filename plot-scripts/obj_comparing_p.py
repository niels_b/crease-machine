import matplotlib.pyplot as plt
import csv
from core.directories import root_dir
from core.help_functions import set_background

set_background(32)
my_fontsize = 14
output_filepath = f'{root_dir}/outputs'

with open(f'{output_filepath}/saddle-opt-smooth-no/minimize-results.csv', 'r') as f:
    my_reader = csv.DictReader(f, delimiter=',')
    non_smooth_f = [float(row['f']) for row in my_reader]

smooth_p1_f = []
smooth_p1_fplus = []
with open(f'{output_filepath}/saddle-opt-smooth-p-1/minimize-results-plus.csv', 'r') as f:
    my_reader = csv.DictReader(f, delimiter=',')
    for row in my_reader:
        smooth_p1_f.append(float(row['f']))
        smooth_p1_fplus.append(float(row['fplus']))

smooth_p2_f = []
smooth_p2_fplus = []
with open(f'{output_filepath}/saddle-opt-smooth-p-2/minimize-results-plus.csv', 'r') as f:
    my_reader = csv.DictReader(f, delimiter=',')
    for row in my_reader:
        smooth_p2_f.append(float(row['f']))
        smooth_p2_fplus.append(float(row['fplus']))

smooth_p3_f = []
smooth_p3_fplus = []
with open(f'{output_filepath}/saddle-opt-smooth-p-3/minimize-results-plus.csv', 'r') as f:
    my_reader = csv.DictReader(f, delimiter=',')
    for row in my_reader:
        smooth_p3_f.append(float(row['f']))
        smooth_p3_fplus.append(float(row['fplus']))

smooth_p4_f = []
smooth_p4_fplus = []
with open(f'{output_filepath}/saddle-opt-smooth-p-4/minimize-results-plus.csv', 'r') as f:
    my_reader = csv.DictReader(f, delimiter=',')
    for row in my_reader:
        smooth_p4_f.append(float(row['f']))
        smooth_p4_fplus.append(float(row['fplus']))

smooth_p5_f = []
smooth_p5_fplus = []
with open(f'{output_filepath}/saddle-opt-smooth-p-5/minimize-results-plus.csv', 'r') as f:
    my_reader = csv.DictReader(f, delimiter=',')
    for row in my_reader:
        smooth_p5_f.append(float(row['f']))
        smooth_p5_fplus.append(float(row['fplus']))

set_background(32)
plt.figure()
# plt.scatter(iterations, objective_function_values, s=4)
plt.plot(range(len(non_smooth_f)), non_smooth_f, color='C0', linestyle='solid', label='no smoothing')
plt.plot(range(len(smooth_p1_fplus)), smooth_p1_fplus, color='C1', linestyle='solid', label='p = 1')
plt.plot(range(len(smooth_p2_fplus)), smooth_p2_fplus, color='C2', linestyle='solid', label='p = 2')
plt.plot(range(len(smooth_p3_fplus)), smooth_p3_fplus, color='C3', linestyle='solid', label='p = 3')
plt.plot(range(len(smooth_p4_fplus)), smooth_p4_fplus, color='C4', linestyle='solid', label='p = 4')
plt.plot(range(len(smooth_p5_fplus)), smooth_p5_fplus, color='C5', linestyle='solid', label='p = 5')

plt.xlabel('Nelder-Mead iteration', fontsize=my_fontsize)
plt.ylabel(f'maximum distance (m)', fontsize=my_fontsize)
plt.ylim((0, 0.052))
plt.xticks([0, 7000], ['0', '7000'], fontsize=my_fontsize)
plt.yticks([0, 0.05], ['0', '0.05'], fontsize=my_fontsize)
plt.legend(fontsize=my_fontsize, loc='lower right')
plt.tight_layout()
plt.savefig(f'{output_filepath}/comparing-p-nonsmooth.pdf')
plt.close()


plt.figure()
# plt.scatter(iterations, objective_function_values, s=4)
plt.plot(range(len(smooth_p1_f)), smooth_p1_f, color='C1', linestyle='solid', label='p = 1')
plt.plot(range(len(smooth_p2_f)), smooth_p2_f, color='C2', linestyle='solid', label='p = 2')
plt.plot(range(len(smooth_p3_f)), smooth_p3_f, color='C3', linestyle='solid', label='p = 3')
plt.plot(range(len(smooth_p4_f)), smooth_p4_f, color='C4', linestyle='solid', label='p = 4')
plt.plot(range(len(smooth_p5_f)), smooth_p5_f, color='C5', linestyle='solid', label='p = 5')

plt.xlabel('Nelder-Mead iteration')
plt.ylabel(f'smooth maximum distance (m)')
plt.ylim((0, 0.86))
plt.legend()
plt.tight_layout()
plt.savefig(f'{output_filepath}/comparing-p-smooth.pdf')
plt.close()


plt.figure()
# plt.scatter(iterations, objective_function_values, s=4)
plt.plot(range(len(non_smooth_f)), non_smooth_f, color='C0', linestyle='solid', label='non-smooth')
plt.plot(range(len(smooth_p1_f)), smooth_p1_f, color='C1', linestyle='dashed', label='smooth, p=1')
plt.plot(range(len(smooth_p1_fplus)), smooth_p1_fplus, color='C1', linestyle='solid')
plt.plot(range(len(smooth_p2_f)), smooth_p2_f, color='C2', linestyle='dashed', label='smooth, p=2')
plt.plot(range(len(smooth_p2_fplus)), smooth_p2_fplus, color='C2', linestyle='solid')
plt.plot(range(len(smooth_p3_f)), smooth_p3_f, color='C3', linestyle='dashed', label='smooth, p=3')
plt.plot(range(len(smooth_p3_fplus)), smooth_p3_fplus, color='C3', linestyle='solid')
plt.plot(range(len(smooth_p4_f)), smooth_p4_f, color='C4', linestyle='dashed', label='smooth, p=4')
plt.plot(range(len(smooth_p4_fplus)), smooth_p4_fplus, color='C4', linestyle='solid')
plt.plot(range(len(smooth_p5_f)), smooth_p5_f, color='C5', linestyle='dashed', label='smooth, p=5')
plt.plot(range(len(smooth_p5_fplus)), smooth_p5_fplus, color='C5', linestyle='solid')

plt.xlabel('Nelder-Mead iteration')
plt.ylabel(f'objective function value (m)')
plt.ylim((0, 0.86))
# plt.yscale('log')
plt.legend()
plt.tight_layout()
plt.savefig(f'{output_filepath}/comparing-all.pdf')
plt.close()
