import numpy as np
import ast
import csv
import matplotlib.pyplot as plt
from core.directories import root_dir
from rigid.make_developable import (objective, constraints, list_of_fixed_and_free_nodes, list_of_interior_vertices,
                                    list_of_clockwise_panels, list_of_pattern_and_cross_edges, initial_edge_lengths)
from rigid.match_shape import shape_creator

shape = 'saddle'
itr = 50
iterations = list(np.arange(0, (itr-1)+0.1, 1))
objective_function_values = []
developability_constraints_total = []
planarity_constraints_total = []

designs = []
with open(f'{root_dir}/outputs/rigid/{shape}/developing/minimize_results.csv') as f:
    my_reader = csv.DictReader(f, delimiter=',')
    for row in my_reader:
        my_design = ast.literal_eval(row['x'])
        designs.append(my_design)
        # objective_function_values.append(float(row['f']))

# gathering required params
nodes, panels, miura_units_x, miura_units_y = shape_creator(shape)
edges = list_of_pattern_and_cross_edges(miura_units_x, miura_units_y)
initial_lengths = initial_edge_lengths(nodes, edges)
_, free_nodes = list_of_fixed_and_free_nodes(nodes, miura_units_x, miura_units_y)
interior_nodes, neighbours_clockwise = list_of_interior_vertices(nodes, miura_units_x, miura_units_y)
clockwise_panels = list_of_clockwise_panels(panels)

for design in designs:
    design_objective = objective(nodes, free_nodes, edges, initial_lengths, design)
    design_constraints = constraints(nodes, free_nodes, interior_nodes, neighbours_clockwise, clockwise_panels, design)

    objective_function_values.append(design_objective)

    developability_constraints = design_constraints[:len(interior_nodes)]
    planarity_constraints = design_constraints[len(interior_nodes):]
    developability_constraints_total.append(sum([abs(item) for item in developability_constraints]))
    planarity_constraints_total.append(sum([abs(item) for item in planarity_constraints]))

print(objective_function_values)

plt.figure()
plt.scatter(iterations, objective_function_values, s=4)
plt.plot(iterations, objective_function_values, color='C0')
plt.xlabel('iteration')
plt.ylabel(f'objective function value')
# plt.yscale('log')
plt.tight_layout()
plt.savefig(f'{root_dir}/outputs/rigid/{shape}/developing/{shape}-rigid-objective-function.pdf')
plt.close()

plt.figure()
plt.scatter(iterations, developability_constraints_total, s=4, color='C1')
plt.plot(iterations, developability_constraints_total, color='C1', label='foldability')
plt.scatter(iterations, planarity_constraints_total, s=4, color='C2')
plt.plot(iterations, planarity_constraints_total, color='C2', label='planarity')
plt.xlabel('iteration')
plt.ylabel(f'constraint total')
plt.yscale('log')
# plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig(f'{root_dir}/outputs/rigid/{shape}/developing/{shape}-rigid-constraints.pdf')
plt.close()
