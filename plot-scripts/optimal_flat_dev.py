import csv
import ast
import matplotlib.pyplot as plt
from core.surface_matching_dev import make_nodes_from_coords, initial_rigid_motion
from core.directories import root_dir
from core.help_functions import read_from_input

shape = 'saddle'
input_file = f'{shape}-opt'
output_path = f'{root_dir}/outputs/{shape}-opt2-bfgs-0.001'

nodes, panels, _, _, _, _ = read_from_input(input_file)
nodes = initial_rigid_motion(nodes)

designs = []
with open(f'{output_path}/minimize-results.csv') as f:
    my_reader = csv.DictReader(f, delimiter=',')
    for row in my_reader:
        my_design = ast.literal_eval(row['x'])
        designs.append(my_design)

# first and final
for i, design in enumerate(designs):
    if i in [0, len(designs)-1]:
        half_nodes = list(design)[:-3]
        all_nodes = make_nodes_from_coords(half_nodes, (8, 8), True, True, initial_nodes=nodes)
        flat_nodes = [(node[0], node[1]) for node in all_nodes]

        fig = plt.figure()
        ax = fig.add_subplot(111)

        for panel in panels:
            panel_xs = []
            panel_ys = []
            for indx in panel:
                panel_xs.append(flat_nodes[indx][0])
                panel_ys.append(flat_nodes[indx][1])

            plt.scatter(panel_xs, panel_ys, s=4, color='black')
            panel_xs.append(panel_xs[0])
            panel_ys.append(panel_ys[0])
            plt.plot(panel_xs, panel_ys, color='black')

        xticks, yticks = [], []
        plt.xticks(xticks, xticks)
        plt.yticks(yticks, yticks)
        # plt.xlabel('')
        # plt.ylabel('')
        # plt.xlim(-0.2, 0.8)
        # plt.ylim(-0.4, 0.6)
        # plt.axis('equal')
        ax.set_aspect('equal')
        ax.axis('off')
        plt.tight_layout()
        plt.savefig(f'{output_path}/{shape}-flat-pattern-{i}.pdf')

        plt.close()

# combined
fig = plt.figure(figsize=(8, 8))
ax = fig.add_subplot(111)

for i, design in enumerate(designs):
    if i in [0, len(designs)-1]:
        half_nodes = list(design)[:-3]
        all_nodes = make_nodes_from_coords(half_nodes, (8, 8), True, True, initial_nodes=nodes)
        flat_nodes = [(node[0], node[1]) for node in all_nodes]

        for panel in panels:
            panel_xs = []
            panel_ys = []
            for indx in panel:
                panel_xs.append(flat_nodes[indx][0])
                panel_ys.append(flat_nodes[indx][1])

            if i == 0:
                my_colour = '#167E4A'
                my_alpha = 0.5
            elif i == len(designs)-1:
                my_colour = 'black'
                my_alpha = 1
            else:
                raise Exception('wrong design nr')

            plt.scatter(panel_xs, panel_ys, s=4, color=my_colour, alpha=my_alpha)
            panel_xs.append(panel_xs[0])
            panel_ys.append(panel_ys[0])
            plt.plot(panel_xs, panel_ys, color=my_colour, alpha=my_alpha)

xticks, yticks = [], []
plt.xticks(xticks, xticks)
plt.yticks(yticks, yticks)
# plt.xlabel('')
# plt.ylabel('')
# plt.xlim()
# plt.ylim()
# plt.axis('equal')
ax.set_aspect('equal')
ax.axis('off')
plt.tight_layout()
plt.savefig(f'{output_path}/{shape}-opt-flat-pattern-start-and-end.pdf')

plt.close()
