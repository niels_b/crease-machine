from math import log, e
import numpy as np
import matplotlib.pyplot as plt
from core.directories import root_dir
from core.help_functions import create_output_folder

config = 'analysis-type'
create_output_folder(config)


def my_fun(x):
    return log(x+1)


def my_fun_der(x):
    return 1 / (x + 1)


true_displacements = list(np.arange(0, 4.5, 0.01))
true_load_factors = [my_fun(a) for a in true_displacements]

hor_lines = [0.4, 0.8, 1.2, 1.6]
x_hor_lines = [e**a - 1 for a in hor_lines]

num_load_factors = [0] + hor_lines
num_disps = [0]
for fac in num_load_factors[:-1]:
    num_disps.append(num_disps[-1] + e**fac * 0.4)

plt.figure()
plt.hlines(hor_lines, [0]*len(hor_lines), x_hor_lines, colors='grey', linestyles='dashed', zorder=2)

plt.plot(true_displacements, true_load_factors, color='black', zorder=3, label='true solution')

plt.scatter(num_disps, num_load_factors, color='blue', s=10, zorder=3)
plt.plot(num_disps, num_load_factors, color='blue', zorder=3, label='numerical solution')

plt.xlabel('displacement of a relevant DOF')
plt.ylabel('load factor')
plt.xticks([])
plt.yticks([])
plt.xlim((0, 4.2))
plt.ylim((0, 1.8))
plt.legend(loc='center right')
plt.tight_layout()
plt.savefig(f'{root_dir}/outputs/{config}/purely-incremental.pdf')
plt.close()

hor_lines = [0.8, 1.6]
x_hor_lines = [e**a - 1 for a in hor_lines]

num_load_factors = [0]
num_disps = [0]
target = 0
for j in range(2):
    target += 0.8
    for i in range(3):
        num_disps.append(num_disps[-1] + e**num_load_factors[-1] * (target - num_load_factors[-1]))
        num_load_factors.append(num_load_factors[-1] + (target - num_load_factors[-1]))

        num_disps.append(num_disps[-1])
        num_load_factors.append(my_fun(num_disps[-1]))

plt.figure()
plt.hlines(hor_lines, [0]*len(hor_lines), x_hor_lines, colors='grey', linestyles='dashed', zorder=2)
plt.plot(true_displacements, true_load_factors, color='black', zorder=3, label='true solution')
plt.scatter(num_disps, num_load_factors, color='blue', s=10, zorder=3)
plt.plot(num_disps, num_load_factors, color='blue', zorder=3, label='numerical solution')
plt.xlabel('displacement of a relevant DOF')
plt.ylabel('load factor')
plt.xticks([])
plt.yticks([])
plt.xlim((0, 4.2))
plt.ylim((0, 1.8))
plt.legend(loc='center right')
plt.tight_layout()
plt.savefig(f'{root_dir}/outputs/{config}/incremental-iterative.pdf')
plt.close()


true_load_factors = [1.5*my_fun(a) - 0.1*a**2 for a in true_displacements]
plt.figure()
plt.hlines(1.5, 0, 4.2, colors='grey', linestyles='dashed', zorder=2)
plt.plot(true_displacements, true_load_factors, color='black', zorder=3, label='true solution')
plt.text(2.1, 1.52, 'target load factor')
plt.text(3.3, 0.8, 'negative\nstiffness')
plt.xlabel('displacement of a relevant DOF')
plt.ylabel('load factor')
plt.xticks([])
plt.yticks([])
plt.xlim((0, 4.2))
plt.ylim((0, 1.8))
plt.tight_layout()
plt.savefig(f'{root_dir}/outputs/{config}/negative-stiffness.pdf')
plt.close()


plt.figure()
plt.xlabel('displacement of a relevant DOF')
plt.ylabel('load factor')
plt.xticks([])
plt.yticks([])
plt.xlim((0, 4.2))
plt.ylim((0, 1.8))
plt.tight_layout()
plt.savefig(f'{root_dir}/outputs/{config}/snapback.pdf')
plt.close()
