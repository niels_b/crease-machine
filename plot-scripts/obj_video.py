import csv
import ast
import numpy as np
from PIL import Image
import subprocess
import matplotlib.pyplot as plt
from core.surface_matching import make_nodes_from_coords
from core.directories import root_dir
from core.help_functions import read_from_input, set_background

shape = 'arch'
input_file = f'{shape}-opt'
output_path = f'{root_dir}/outputs/{shape}-opt'

_, panels, _, _, _, _ = read_from_input(input_file)

itr = 0
max_distances = []
smooth_max_distances = []

with open(f'{output_path}/minimize-results-plus.csv') as f:
    my_reader = csv.DictReader(f, delimiter=',')
    for row in my_reader:
        smooth_max_distances.append(float(row['f']))
        max_distances.append(float(row['fplus']))
        itr += 1

iterations = np.arange(itr)

background_colour = 32   # dark mode is 32
background_colour_vid = 34   # dark mode is 34

set_background(background_colour)

plt.figure()
plt.plot(iterations, max_distances, color='C3', label='maximum distance')
plt.plot(iterations, smooth_max_distances, color='C3', linestyle='dashed', label='smooth maximum distance')
plt.xlabel('Nelder-Mead iteration')
plt.ylabel(f'(smooth) maximum distance (m)')
plt.ylim((0, 1.05*max(smooth_max_distances)))
plt.legend()
xticks = plt.xticks()
yticks = plt.yticks()
xlim = plt.xlim()
ylim = plt.ylim()
plt.tight_layout()
plt.savefig(f'{output_path}/itr-obj/final-obj.pdf')
plt.close()

my_fontsize = 14
set_background(background_colour_vid)
px = 1 / plt.rcParams['figure.dpi']
for i in range(len(max_distances)):
    print(i)
    plt.figure(figsize=(540 * px, 540 * px))
    # plt.xticks(xticks[0], xticks[1])
    # plt.yticks(yticks[0], yticks[1])

    if shape == 'saddle':
        plt.xticks([0, 2030], ['0', '2030'], fontsize=my_fontsize)
        plt.yticks([0.000, 0.0243, 0.0495, 0.100], ['0.000', '0.024', '0.049', '0.100'], fontsize=my_fontsize)
        plt.ylim((0, 0.100))

    elif shape == 'dome':
        plt.xticks([0, 13276], ['0', '13276'], fontsize=my_fontsize)
        plt.yticks([0.000, 0.02711, 0.07186, 0.140], ['0.000', '0.027', '0.072', '0.140'], fontsize=my_fontsize)
        plt.ylim((0, 0.140))

    elif shape == 'arch':
        plt.xticks([0, 2225], ['0', '2225'], fontsize=my_fontsize)
        plt.yticks([0.000, 0.036, 0.0936, 0.150], ['0.000', '0.036', '0.094', '0.150'], fontsize=my_fontsize)
        plt.ylim((0, 0.150))

    else:
        raise Exception('invalid shape')

    plt.xlim(xlim)
    # plt.ylim(ylim)

    plt.plot(iterations[:i+1], max_distances[:i+1], color='C3', label='maximum distance')
    plt.plot(iterations[:i+1], smooth_max_distances[:i+1], color='C3', linestyle='dashed',
             label='smooth maximum distance')
    plt.xlabel('Nelder-Mead iteration', fontsize=my_fontsize)
    plt.ylabel(f'(smooth) maximum distance (m)', fontsize=my_fontsize)

    plt.legend(fontsize=my_fontsize)
    plt.tight_layout()
    plt.savefig(f'{output_path}/itr-obj/frame{i}.png')
    plt.close()


video_length = 6    # number of seconds
fps = str(len(max_distances)//video_length)     # frames per second, match this with paraview export
if fps == '0':
    fps = '1'
width, height = Image.open(f'{output_path}/itr-obj/frame0.png').size
width_height = f'{width}x{height}'
command = (f'cd; ffmpeg -hide_banner -loglevel error -r {fps} -f image2 -s {width_height} -i '
           f'{output_path}/itr-obj/frame%d.png '
           f'-vcodec libx264 -crf 15 -pix_fmt yuv420p '
           f'{output_path}/itr-obj/obj-video.mp4')
subprocess.run(command, shell=True)
