import numpy as np
import csv
import matplotlib.pyplot as plt
from core.directories import root_dir

shape = 'saddle'
filepath = f'{root_dir}/outputs/{shape}-opt2-bfgs-0.01'

itr = 0
max_distances = []
smooth_max_distances = []

with open(f'{filepath}/minimize-results-plus.csv', 'r') as file:
    my_reader = csv.DictReader(file, delimiter=',')
    for row in my_reader:
        smooth_max_distances.append(float(row['f']))
        max_distances.append(float(row['fplus']))
        itr += 1

print(len(max_distances))
iterations = np.arange(itr)
my_fontsize = 14

plt.figure()
plt.plot(iterations, max_distances, color='C3', label='maximum distance')
plt.plot(iterations, smooth_max_distances, color='C3', linestyle='dashed', label='smooth maximum distance')
plt.xlabel('BFGS iteration', fontsize=my_fontsize)
plt.ylabel(f'(smooth) maximum distance (m)', fontsize=my_fontsize)
plt.ylim((0, 1.05*max(smooth_max_distances)))

# if shape == 'saddle':
#     plt.xticks([0, 2030], ['0', '2030'], fontsize=my_fontsize)
#     plt.yticks([0.000, 0.0243, 0.0495, 0.100], ['0.000', '0.024', '0.049', '0.100'], fontsize=my_fontsize)
#     plt.ylim((0, 0.100))
#
# else:
#     raise Exception('invalid shape')

plt.legend(fontsize=my_fontsize)
plt.tight_layout()
plt.savefig(f'{filepath}/{shape}-opt-objective-function.pdf')
plt.close()
