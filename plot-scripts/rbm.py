import numpy as np
from core.directories import root_dir
from core.help_functions import create_output_folder, read_from_input, make_vtk_data, make_vtk_file
from core.surface_matching_clean import (initial_rigid_motion, create_initial_design, make_nodes_from_coords,
                                         initial_compression, rigid_motion, rotate_with_matrix)
from rigid.match_shape import get_truss
from core.part1_prepare_data import prepareData
from core.part2_path_analysis_sparse import arc_length_improved_with_target_load_factor

config = 'rbm'


create_output_folder(config)

size = (8, 8)  # number of panels in each direction, not number of miura units
symmetric = True
middle_zero = True
coords_and_rbm = True
nodes, panels, supports, loads, relevant_dof, analyInputOpt = read_from_input(config)

nodes = initial_rigid_motion(nodes)

x0 = list(create_initial_design(nodes, size, symmetric, middle_zero, coords_and_rbm))

n_coords = 2 * (size[0] // 2) * (size[1] + 1) + size[1] + 1
flat_coords = x0[:n_coords]
flat_nodes = make_nodes_from_coords(flat_coords, size, symmetric, middle_zero)

# initial compression to make the origami 3D
nodes2, _, _, _, _ = initial_compression(flat_nodes, panels, size, analyInputOpt, shape='saddle')

pitch, ytrans, ztrans = x0[-3:]

# coords = []
# n_unique_nodes = ((size[0] + 1) // 2) * (size[1] + 1)  # does not include middle column
#
# for node in nodes[:n_unique_nodes]:
#     coords.append(node[0])
#     coords.append(node[1])
#
# for node in nodes[n_unique_nodes:n_unique_nodes + size[1] + 1]:  # middle column
#     coords.append(node[1])
#
# nodes = make_nodes_from_coords(coords, size, symmetric, middle_zero)

# # save result
# truss = get_truss(nodes, panels)
# Uhis = np.zeros((435, 2))
# my_nodes, triangles = make_vtk_data(truss, Uhis)
# make_vtk_file(my_nodes[-1], triangles, config,
#               filepath=f'{root_dir}/outputs/{config}/{config}-uncompressed.vtk')

# nodes = initial_compression(nodes, panels, size, analyInputOpt)

# truss = get_truss(nodes, panels)
# my_nodes, triangles = make_vtk_data(truss, Uhis)
# make_vtk_file(my_nodes[-1], triangles, config,
#               filepath=f'{root_dir}/outputs/{config}/{config}-compressed.vtk')
#
# nodes2 = nodes  # initial compression is already done

truss, angles, analyInputOpt = prepareData(nodes2, panels, supports, loads, analyInputOpt)

Uhis, Fhis = arc_length_improved_with_target_load_factor(truss, angles, analyInputOpt)
truss, Uhis = rigid_motion(truss, Uhis, size, pitch, ytrans, ztrans)

# save result
nodes, triangles = make_vtk_data(truss, Uhis)
make_vtk_file(nodes[-1], triangles, config,
              filepath=f'{root_dir}/outputs/{config}/{config}-aligned.vtk')

# misaligning
truss['nodes'] = [tuple(np.array(node) + np.array([0.4, 0.2, 0.1])) for node in truss['nodes']]
inertia_tensor = np.array([[0.1, 0.3, 0.4],
                           [0.3, -0.5, 0.2],
                           [0.4, 0.2, -0.6]])
truss['nodes'], Uhis = rotate_with_matrix(truss, Uhis, inertia_tensor, checking=False, pitch=0)

# save result
nodes, triangles = make_vtk_data(truss, Uhis)
make_vtk_file(nodes[-1], triangles, config,
              filepath=f'{root_dir}/outputs/{config}/{config}-misaligned.vtk')
