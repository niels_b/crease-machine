import csv
import ast
from core.directories import root_dir
from core.surface_matching import objective
from core.help_functions import read_from_input

shape = 'saddle'
config = f'{shape}-opt'
output_filepath = f'{root_dir}/outputs/{config}'

nodes, panels, supports, loads, relevant_dof, analyInputOpt = read_from_input(config)

itr = 0
designs = []
objective_function_values = []

with open(f'{output_filepath}/minimize-results.csv', 'r') as f:
    my_reader = csv.DictReader(f, delimiter=',')
    for row in my_reader:
        designs.append(ast.literal_eval(row['x']))
        objective_function_values.append(float(row['f']))


obj_1 = objective(nodes, panels, supports, loads, analyInputOpt, shape, config,
                  (8, 8), False, True, True, False, False, False, False, False, False,
                  True, designs[-1])
print(obj_1)
