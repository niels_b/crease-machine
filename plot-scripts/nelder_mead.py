import csv
import numpy as np
from scipy.optimize import minimize, NonlinearConstraint
from core.directories import root_dir
from core.help_functions import create_output_folder, set_background
import matplotlib
import matplotlib.pyplot as plt

config = 'nelder-mead'


def f(x):
    # print(f'calculating objective with {x}')
    x1, x2 = x
    obj = (x1 + 2*x2 - 7)**2 + (2*x1 + x2 - 5)**2  # booth
    return obj


def create_plot(my_designs):
    grey = 32
    plt.style.use('dark_background')
    plt.rcParams['axes.facecolor'] = (grey/255, grey/255, grey/255)
    plt.rcParams['savefig.facecolor'] = (grey/255, grey/255, grey/255)

    designs_x1 = [el[0] for el in my_designs]
    designs_x2 = [el[1] for el in my_designs]

    # generate 2 2d grids for the x1 & x2 bounds
    color_x2, color_x1 = np.meshgrid(np.linspace(-10, 10, 1000), np.linspace(-10, 10, 1000))

    color_f = (color_x1 + 2*color_x2 - 7)**2 + (2*color_x1 + color_x2 - 5)**2
    # x1 and x2 are bounds, so z should be the value *inside* those bounds.
    # Therefore, remove the last value from the z array.
    color_f = color_f[:-1, :-1]

    plt.figure()
    plt.title('Booth function')

    c = plt.pcolormesh(color_x1, color_x2, color_f, cmap='RdBu', norm=matplotlib.colors.LogNorm(), rasterized=True)

    # set the limits of the plot to the limits of the data
    plt.xlim([color_x1.min(), color_x1.max()])
    plt.ylim([color_x2.min(), color_x2.max()])
    plt.colorbar(c)

    plt.plot(designs_x1, designs_x2, color='black', marker='o', linestyle='dashed', linewidth=0.2, markersize=1)

    # loop through each x,y pair
    itr = 0
    for i, j in zip(designs_x1, designs_x2):
        plt.annotate(str(itr), xy=(i+0.1, j), color='black', fontsize=8)
        itr += 1

    plt.tight_layout()
    plt.savefig(f'{root_dir}/outputs/{config}/heatmap.pdf')
    plt.close()


def nelder_mead_optimization():

    create_output_folder(config)

    x0 = [np.array([-7.5, -7.5]), np.array([-9.5, -9.5]), np.array([-9.5, -7.5])]
    x = [x0]

    while f(x[-1][0]) > 1e-3:
        current_simplex = x[-1]
        obj_values = [f(current_simplex[0]), f(current_simplex[1]), f(current_simplex[2])]
        u_index, v_index, w_index = sorted(range(len(obj_values)), key=lambda k: obj_values[k])
        u, v, w = current_simplex[u_index], current_simplex[v_index], current_simplex[w_index]
        uv_middle = (u+v)/2
        w_uv_middle = uv_middle - w
        r = w + 2*w_uv_middle
        if f(u) < f(r) < f(v):
            new_simplex = [u, v, r]
        elif f(r) < f(u) < f(v):
            e = r + w_uv_middle
            if f(e) < f(r):
                new_simplex = [u, v, e]
            else:
                new_simplex = [u, v, r]
        elif f(u) < f(v) < f(r):
            ci = w + 0.5*w_uv_middle
            co = w + 1.5*w_uv_middle
            if f(ci) < f(v) and f(ci) < f(co):
                new_simplex = [u, v, ci]
            elif f(co) < f(v) and f(co) < f(ci):
                new_simplex = [u, v, co]
            else:
                v_prime = u + (v-u)/2
                w_prime = u + (w-u)/2
                new_simplex = [u, v_prime, w_prime]
        else:
            raise Exception('i dont know man')

        x.append(new_simplex)

    print(x)

    small = 1e-6
    X = np.arange(-10, 10+small, 0.1)
    Y = np.arange(-10, 10+small, 0.1)
    X, Y = np.meshgrid(X, Y)
    Z = (X + 2*Y - 7)**2 + (2*X + Y - 5)**2  # booth

    for i, simplex in enumerate(x):
        set_background(32)
        plt.figure()
        plt.contour(X, Y, Z, levels=[1e-2, 1e-1, 1e0, 1e1, 3e1, 1e2, 3e2, 1e3], colors='orange')

        plt.plot([simplex[0][0], simplex[1][0], simplex[2][0], simplex[0][0]],
                 [simplex[0][1], simplex[1][1], simplex[2][1], simplex[0][1]])

        plt.xticks([], [])
        plt.yticks([], [])

        plt.xlabel('x1')
        plt.ylabel('x2')

        plt.tight_layout()
        plt.savefig(f'{root_dir}/outputs/{config}/iteration-{i}.png')
        plt.close()


if __name__ == '__main__':
    nelder_mead_optimization()
