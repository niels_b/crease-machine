import csv
import ast
from core.directories import root_dir
from core.surface_matching import objective
from core.help_functions import read_from_input

shape = 'dome'
config = f'{shape}-opt'
output_filepath = f'{root_dir}/outputs/{config}'

nodes, panels, supports, loads, relevant_dof, analyInputOpt = read_from_input(config)

itr = 0
designs = []
objective_function_values = []

with open(f'{output_filepath}/minimize-results.csv', 'r') as f:
    my_reader = csv.DictReader(f, delimiter=',')
    for row in my_reader:
        designs.append(ast.literal_eval(row['x']))
        objective_function_values.append(float(row['f']))
        itr += 1

filename = f'{root_dir}/outputs/{config}/counter2.txt'
with open(filename, 'w') as file:
    file.write('0')

with open(f'{output_filepath}/minimize-results-plus.csv', 'w') as f:
    my_writer = csv.DictWriter(f, fieldnames=['x', 'f', 'fplus'], delimiter=',')
    my_writer.writeheader()
    for i in range(itr):
        my_writer.writerow({'x': list(designs[i]), 'f': objective_function_values[i],
                            'fplus': objective(nodes, panels, supports, loads, analyInputOpt, shape, config,
                                               (8, 8), False, True, True, False, False, False, False, False, False,
                                               True, designs[i])})
