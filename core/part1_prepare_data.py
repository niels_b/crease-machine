from math import pi, sqrt
import numpy as np

norm = np.linalg.norm
dot = np.dot


def prepareData(nodes, panels, supp, load, analyInputOpt):
    # analyInputOpt contains the following fields:
        # 'ModelType': 'N4B5' or 'N5B8'
        # 'MaterCalib': 'auto' or 'manual'
        # 'BarCM': Constitutive model of bar elements
        # 'Abar': Area of bars(manual mode)
        # 'RotSprBend': Constitutive model of bending elements(manual mode)
        # 'RotSprFold': Constitutive model of folding elements(manual mode)
        # 'Kb': Initial modulus of bending hinges
        # 'Kf': Initial modulus of folding hinges
        # 'ModElastic': Modulus of Elasticy of bar material
        # 'Poisson': Poisson's ratio
        # 'Thickness': Panel thickness
        # 'LScaleFactor': Ratio of length scale factor(Ls) over hinge length LF
        # 'ZeroBend': 'AsIs' - as the geometry is defined,
                    # 'Flat' - enforce neutral angle at pi(flat configuration)
                    # value - provide specific values, scalar for uniform
                    # assignment, vector for differential assignment
        # 'LoadType': 'Force' or 'Displacement'
        # 'Load': Loading condition
        # 'AdaptiveLoad': Function handle for adaptive loading
        # 'InitialLoadFactor': Initial load factor for MGDCM ('Force' mode)
        # 'MaxIcr': Maximum number of increments ('Force' mode)
        # 'DispStep': Number of displacement increments ('Displacement' mode)
        # 'StopCriterion': Function handle for specific stopping criterion.

    # analyInputOpt['stopCriterion'] = 1

    if 'LScaleFactor' not in analyInputOpt.keys():
        analyInputOpt['LScaleFactor'] = 2
    if 'maxLmd' not in analyInputOpt.keys():
        analyInputOpt['maxLmd'] = 1e5
    if 'tol' not in analyInputOpt.keys():
        analyInputOpt['tol'] = 1e-5
    if 'initialLoadFactor' not in analyInputOpt.keys():
        analyInputOpt['initialLoadFactor'] = 0.01
    if 'multiply_load' not in analyInputOpt.keys():
        analyInputOpt['multiply_load'] = 1

    bends, nodes, panelctr = findBend(panels, nodes)
    folds, bdrys, trigl = findfdbd(panels, bends)
    # print(f'nodes:\n{nodes}\n')
    # print(f'panels:\n{panels}\n')
    #
    # print(f'bends:\n{bends}\n')
    # print(f'panelctr:\n{panelctr}\n')
    # print(f'folds:\n{folds}\n')
    # print(f'bdrys:\n{bdrys}\n')
    # print(f'trigl:\n{trigl}\n')

    bars = []
    for bend in bends:
        bars.append(bend[0:2])
    for fold in folds:
        bars.append(fold[0:2])
    for bdry in bdrys:
        bars.append(bdry)
    # print(f'bars:\n{bars}\n')

    b, lengths = dirc3d(nodes, bars)
    # print(f'b:\n{b}\n')
    # print(f'lengths:\n{lengths}\n')

    if len(supp) == 0:
        rs = []
    else:
        rs = []     # transform node indices to DOF indices
        for sup in supp:
            if sup[1] == 1:
                rs.append(3*sup[0])
            if sup[2] == 1:
                rs.append(3*sup[0]+1)
            if sup[3] == 1:
                rs.append(3*sup[0]+2)

    pf0 = []    # is going to be a list with the length equal to the amount of folds
    for i in range(len(folds)):
        pf0.append(foldKe(nodes, folds[i]))

    zeroBend = analyInputOpt['ZeroBend']    # only really supports 'AsIs'
    pb0 = []    # is going to be a list with the length equal to the amount of bends
    if zeroBend == 'AsIs':
        for i in range(len(bends)):
            pb0.append(foldKe(nodes, bends[i]))
    else:
        raise Exception('ZeroBend must be AsIs')

    # print(pb0)

    if load:
        m = len(nodes)
        FD = np.zeros(3*m)
        for little_load in load:
            FD[little_load[0]*3] = little_load[1]
            FD[little_load[0]*3+1] = little_load[2]
            FD[little_load[0]*3+2] = little_load[3]
        analyInputOpt['load'] = FD
    else:
        raise Exception('there is no load')

    truss = {'nodes': nodes, 'bars': bars, 'trigl': trigl, 'B': b, 'lengths': lengths, 'fixedDOFs': list(set(rs))}
    # nodes: include the 5th node in each panel
    # bars: connectivity information of bar elements
    #       the two entries in a row refer to the indices of the two end nodes of a bar element
    # trigl: Triangulation information. The three entries in a row are vertex indices of each triangle.
    # b: compatibility matrix of the bar frame of size n_bars × n_dofs
    # lengths: initial lengths of bar elements
    # indices of fixed degrees of freedom

    angles = {'folds': folds, 'bends': bends, 'pf0': pf0, 'pb0': pb0, 'panels': panels}
    # folds: a list that contains indices of associated nodes of folding rotational springs. The first two entries
    #        define the rotation axis, and the later two refer to off-axis points of the two adjacent triangles in a
    #        rotational spring element
    # bends: a list that contains indices of associated nodes of bending rotational springs, in the same format as
    #        folds.
    # pf0: neutral angles of folding rotational springs
    # pb0: neutral angles of bending rotational springs
    # panels: the jth entry of the list contains the indices of the nodes of the jth panel in counter-clockwise order

    materCalib = analyInputOpt['MaterCalib']    # only supports 'auto'
    if materCalib == 'auto':
        EY = analyInputOpt['ModElastic']/1e6    # transform from N/m^2 to N/mm^2
        nv = analyInputOpt['Poisson']
        thck = analyInputOpt['Thickness']*1000  # transform from meters to millimeters
        truss['CM'] = ogden     # constitutional model for bar elements, EY should already be filled in
        truss['C0'] = EY
        Abar = np.zeros(len(bars))
        kpb = np.zeros(len(bends))
        kpf = np.zeros(len(folds))
        # print(f'kpf: {kpf}')

        G = EY*thck**3 / (12*(1-nv**2))
        Lf = np.array(lengths[len(kpb):(len(kpb)+len(kpf))])
        Ls = analyInputOpt['LScaleFactor']
        Kl = 1 / Ls * G
        # Km = 0.55 * G / thck * np.power(Lf, 1/3)
        Km = 0.55 * G * np.power(Lf / thck, 1/3)
        kpf = 1 / (1 / Kl + 1 / Km) / Lf
        kpf = 1 * kpf

        # print(f'Abar: {Abar}')
        # print(f'kpb: {kpb}')
        # print(f'kpf: {kpf}')
        # print(f'G: {G}')
        # print(f'Lf: {Lf}')
        # print(f'Ls: {Ls}')
        # print(f'Kl: {Kl}')
        # print(f'Km: {Km}')

        for i in range(len(panelctr)):
            Abarj, kpbj = getMaterial(nodes, panels[i], panelctr[i], EY, nv, thck, folds, bends, bdrys)
            for index, area in Abarj:
                Abar[index] = Abar[index] + area
            if len(kpbj) != 0:
                for index, k in kpbj:
                    kpb[index] = kpb[index] + k

        truss['A'] = Abar   # member areas of bar elements
        angles['CMbend'] = superLinearBend  # a function that defines the constitutive model for bending rotational
        #                                     spring elements
        angles['CMfold'] = enhancedLinear   # a function that defines the constitutive model for folding rotational
        #                                     spring elements
        angles['Kb'] = kpb  # stiffness parameters for bending rotational springs specified for each element
        angles['Kf'] = kpf  # stiffness parameters for folding rotational springs specified for each element

    else:
        raise Exception('the only implemented option for MaterCalib is auto')

    # print(f'truss: {truss}')
    # print(f'angles: {angles}')

    return truss, angles, analyInputOpt


def findBend(panels, nodes):
    """finds the bends"""
    nnodes = len(nodes)
    bends = np.empty((6*len(panels), 4), int)
    bends.fill(-99999)
    panelctr = []
    cntb, cntc = 0, 0
    complete_nodes = nodes[:]

    for i, panel in enumerate(panels):
        if len(panel) == 4:     # quadrilateral panel
            L1 = distance(nodes[panel[0]], nodes[panel[2]])     # length of first diagonal
            L2 = distance(nodes[panel[3]], nodes[panel[1]])     # length of second diagonal

            m = (np.array(nodes[panel[2]]) - np.array(nodes[panel[0]]))/L1   # unit vector of first diagonal
            n = (np.array(nodes[panel[3]]) - np.array(nodes[panel[1]]))/L2   # unit vector of second diagonal

            coeff = np.linalg.lstsq(np.column_stack((m, -n)), (np.array(nodes[panel[1]])-np.array(nodes[panel[0]])),
                                    rcond=None)[0]

            if L1 < L2:
                ctrnode = nodes[panel[0]] + coeff[0]*m  # location of the 5th node
            else:
                ctrnode = nodes[panel[1]] + coeff[1]*n  # location of the 5th node

            # If 0, 1, 2, 3 are co-planar, the two results are identical. If they are not, the central node is placed
            # such that the panel is bended along the shorter diagonal.

            complete_nodes.append(tuple(ctrnode))
            panelctr.append(nnodes + cntc)

            for k in range(len(panel)):
                ref1 = (k-1) % len(panel)
                ref2 = (k+1) % len(panel)
                # a list of the bends (4 per panel)
                bends[cntb, :] = [nnodes+cntc, panel[k], panel[ref1], panel[ref2]]  # bend is defined by 4 nodes

                cntb += 1

            cntc += 1

        else:
            raise Exception('havent implemented non-quadrilateral panels yet')

    bends = [list(bend) for bend in bends if all(bend >= 0)]

    return bends, complete_nodes, panelctr


def findfdbd(panels, bends):
    """finds the fonds and boundaries"""
    nnodes = max([max(bends) for bends in bends])+1    # number of nodes, excluding the 5th node in each panel

    # triangularization
    # panelsize = [len(panel) for panel in panels]    # should be a list of 4's for quadrilaterals

    ptri = []   # triangulation
    flg = []    # list of indices in panelsize where panels are already triangular
    for i in range(len([panel for panel in panels if len(panel) == 3])):
        ptri[i] = panels[flg[i]]
        raise Exception('this code is for triangular panels, and it is not complete / verified')

    triglraw = []
    for bend in bends:
        triglraw.append([bend[i] for i in [0, 1, 2]])
    for bend in bends:
        triglraw.append([bend[i] for i in [0, 1, 3]])

    triglraw2 = []
    for triglrawie in triglraw:
        triglraw2.append(np.sort(triglrawie))   # sort the nodes of every triangle

    triglraw2 = np.array(triglraw2)
    trigl_arrays = list(np.unique(triglraw2, axis=0))  # only have unique triangles
    trigl = []
    for trigl_array in trigl_arrays:
        trigl.append(list(trigl_array))

    # formulate connectivity matrix
    comm = np.zeros((nnodes, len(trigl)))
    for i in range(len(trigl)):
        comm[trigl[i], i] = True

    # search for fold lines
    ge = comm.T @ comm
    mf, me = np.where(np.triu(ge) == 2)     # triangular meshes that share two common nodes

    # some sorting
    mfme = list(zip(mf, me))
    mfme_sorted = sorted(mfme, key=lambda x: x[1])
    mf = []
    me = []
    for mf_and_me in mfme_sorted:
        mf.append(mf_and_me[0])
        me.append(mf_and_me[1])

    folds = np.zeros((len(mf), 4))
    for i in range(len(mf)):
        trigl_mf, trigl_me = trigl[mf[i]], trigl[me[i]]
        ia, ib = [], []
        link = [node for node in trigl_mf if node in trigl_me]
        for thing in link:
            ia.append(trigl_mf.index(thing))
            ib.append(trigl_me.index(thing))
        oftpa = list({0, 1, 2} - set(ia))
        oftpb = list({0, 1, 2} - set(ib))

        folds[i] = link + [trigl[mf[i]][oftpa_i] for oftpa_i in oftpa] + [trigl[me[i]][oftpb_i] for oftpb_i in oftpb]

    fdandbd = [sorted([int(fold[0]), int(fold[1])]) for fold in list(folds)]
    onlybd = [sorted([int(fold[0]), int(fold[1])]) for fold in list(bends)]

    ibd = []
    overlap = [thing for thing in fdandbd if thing in onlybd]
    for thing in overlap:
        ibd.append(fdandbd.index(thing))
    my_list = list(range(len(folds)))
    for i in ibd:
        my_list.remove(i)
    my_folds = list(folds[my_list])
    folds = []
    for fold in my_folds:
        folds.append(list(map(int, fold)))  # make sure everything is an integer

    # search for boundaries
    edges = []
    for tri in trigl:
        edges.append(sorted([tri[0], tri[1]]))
    for tri in trigl:
        edges.append(sorted([tri[1], tri[2]]))
    for tri in trigl:
        edges.append(sorted([tri[2], tri[0]]))

    u_arrays = list(np.unique(np.array(edges), axis=0))  # only have unique edges
    u = []
    for edge in u_arrays:
        u.append(list(edge))
    n = []
    for edge in edges:
        n.append(u.index(edge))

    counts = [n.count(x) for x in range(len(u))]
    bdrys = [thing for i, thing in enumerate(u) if counts[i] == 1]

    return folds, bdrys, trigl


def distance(node1, node2):
    dis = sqrt((node1[0]-node2[0])**2 + (node1[1]-node2[1])**2 + (node1[2]-node2[2])**2)
    return dis


def dirc3d(nodes, bars):
    n_elem = len(bars)   # number of elements
    n_nodes = len(nodes)    # number of nodes
    d_not_unit = []
    for bar in bars:
        d_not_unit.append([nodes[bar[1]][0]-nodes[bar[0]][0],
                           nodes[bar[1]][1]-nodes[bar[0]][1],
                           nodes[bar[1]][2]-nodes[bar[0]][2]])
    d = []
    length = []
    for row in d_not_unit:
        L = sqrt(row[0]**2 + row[1]**2 + row[2]**2)
        length.append(L)
        d.append([row[0]/L, row[1]/L, row[2]/L])
    thing1 = np.zeros((n_elem, 6), dtype=int)
    thing2 = np.zeros((n_elem, 6), dtype=int)
    thing3 = np.zeros((n_elem, 6), dtype=float)
    for i in range(6):
        thing1[:, i] = list(range(n_elem))
    for i in range(n_elem):
        bar = bars[i]
        thing2[i, :] = [2+3*bar[0]-2, 2+3*bar[0]-1, 2+3*bar[0],
                        2+3*bar[1]-2, 2+3*bar[1]-1, 2+3*bar[1]]     # 2+ because we start with 0 instead of one
        thing3[i, :] = d[i] + list(np.array(d[i])*-1)
    b = np.zeros((n_elem, 3*n_nodes))
    for i in range(len(thing1)):
        for j in range(6):
            b[thing1[i, j], thing2[i, j]] = thing3[i, j]
    b = -b

    return b, length


def foldKe(coods, my_list):
    # coods is a list of tuples with the nodes' coordinates (including the 5th node)
    # my_list is a panel (four indices of nodes)
    rkj = np.array([coods[my_list[1]][i] - coods[my_list[0]][i] for i in range(3)])
    rij = np.array([coods[my_list[2]][i] - coods[my_list[0]][i] for i in range(3)])
    rkl = np.array([coods[my_list[1]][i] - coods[my_list[3]][i] for i in range(3)])
    rmj = np.cross(rij, rkj)
    rnk = np.cross(rkj, rkl)

    sgn = (abs(np.dot(rnk.T, rij)) > 1e-8) * np.sign(np.dot(rnk.T, rij)) + (abs(np.dot(rnk.T, rij)) <= 1e-8) * 1

    intermediate_thing = np.dot(rmj.T, rnk) / (np.linalg.norm(rmj) * np.linalg.norm(rnk))

    if -1.0000000001 < intermediate_thing < -0.9999999999:
        he = pi
    elif 0.9999999999 < intermediate_thing < 1.0000000001:
        he = 0
    elif -0.9999999999 <= intermediate_thing <= 0.9999999999:
        he = np.arccos(intermediate_thing)
    else:
        raise Exception('he is not in a valid domain')

    he = sgn*he
    if he < 0:
        he = 2*pi + he
    return he


def ogden(Ex, C0):
    """
    inputs
    Ex: Green-Lagrange strain
    C0: the initial modulus

    outputs
    Sx: second Piola-Kirchhoff stress
    Ct: tangent modulus
    Wb: strain energy density
    """
    # function is verified with random inputs
    a1, a2 = 5, 1   # specify parameters
    # a1, a2 = 3, 1   # linear
    pstr = np.sqrt(2*Ex+1)
    C0 = np.less(pstr, 1) * 1*C0 + (np.logical_not(np.less(pstr, 1))) * C0
    Ct = C0 / (a1 - a2) * ((a1-2)*np.power(pstr, a1-4) - (a2-2)*np.power(pstr, a2-4))
    Sx = C0 / (a1 - a2) * (np.power(pstr, a1-2) - np.power(pstr, a2-2))
    Wb = C0 / (a1 - a2) * ((np.power(pstr, a1)-np.ones(pstr.shape))/a1 - (np.power(pstr, a2)-np.ones(pstr.shape))/a2)
    return Sx, Ct, Wb


def getMaterial(nodes, the_list, indexctr, E, nv, t, folds, bends, bdrys):
    the_list = list(the_list)

    sorted_folds = []
    for fold in folds:
        sorted_folds.append(sorted(fold[:2]))
    nf = len(sorted_folds)

    sorted_bends = []
    for bend in bends:
        sorted_bends.append(sorted(bend[:2]))
    nb = len(sorted_bends)

    sorted_bdrys = []
    for bdry in bdrys:
        sorted_bdrys.append(sorted(bdry))

    G = E*t**3 / (12*(1-nv**2))

    if len(the_list) == 4:
        list_1 = the_list
        list_2 = the_list[1:] + [the_list[0]]
        pairs = []
        for i in range(4):
            pairs.append(sorted([list_1[i], list_2[i]]))

        Lf = []     # lengths of the folds
        for pair in pairs:
            node_1, node_0 = nodes[pair[1]], nodes[pair[0]]
            Lf.append(sqrt(sum([(node_1[i] - node_0[i])**2 for i in range(3)])))

        # the matlab intersect function may put the results in a different order but I don't think it matters
        indfd, efd = [], []
        for i, fold in enumerate(sorted_folds):
            for j, pair in enumerate(pairs):
                if fold == pair:
                    indfd.append(i)
                    efd.append(j)

        spokes = []
        for node in the_list:
            spokes.append(sorted([node, indexctr]))

        Lb = []
        for node in the_list:
            Lb.append(sqrt(sum([(nodes[node][i] - nodes[indexctr][i])**2 for i in range(3)])))

        # the matlab intersect function may put the results in a different order but I don't think it matters
        # oh it matters (still not sure if it properly sorts it)
        faulty_indbd = []
        indbd = []
        same_things = []
        for i, bend in enumerate(sorted_bends):
            for j, spoke in enumerate(spokes):
                if bend == spoke:
                    same_things.append(bend)
                    faulty_indbd.append(i)
        correct_order = np.argsort(np.array(same_things), axis=0)[:, 0]
        for i in range(len(faulty_indbd)):
            indbd.append(faulty_indbd[correct_order[i]])

        Dsl = [Lb[0] + Lb[2], Lb[1] + Lb[3], Lb[2] + Lb[0], Lb[3] + Lb[1]]
        kb = []
        for little_dsl in Dsl:
            kb.append(G * (little_dsl/t)**(1/3) / little_dsl)
        kpbj = [[indbd[i], kb[i]] for i in range(4)]

        W = sum([Lf[0], Lf[2]]) / 2
        H = sum([Lf[1], Lf[3]]) / 2

        Af = np.zeros(4)
        Af[0] = t * abs(H**2 - nv*W**2) / (2*H*(1-nv**2))   # this is the same as the one below
        Af[2] = t * abs(H**2 - nv*W**2) / (2*H*(1-nv**2))   # this is the same as the one above
        Af[1] = t * abs(W**2 - nv*H**2) / (2*W*(1-nv**2))   # this is the same as the one below
        Af[3] = t * abs(W**2 - nv*H**2) / (2*W*(1-nv**2))   # this is the same as the one above
        Af = list(Af)

        Ab = np.ones(4)
        Ab = list(t * nv * (H**2 + W**2)**1.5 / (2 * H * W * (1 - nv**2)) * Ab)

        if len(indfd) == len(the_list):
            indices = indbd + [thing + nb for thing in indfd]
            areas = Ab + Af
            Abarj = []
            for i in range(len(indices)):
                Abarj.append([indices[i], areas[i]])
            # raise Exception('this part is not verified as it was not called in the simple folding example')
        elif len(indfd) < len(the_list):
            # the matlab intersect function may put the results in a different order but I don't think it matters
            inddd, edd = [], []
            for i, bdry in enumerate(sorted_bdrys):
                for j, pair in enumerate(pairs):
                    if bdry == pair:
                        inddd.append(i)
                        edd.append(j)

            indices = indbd + [thing + nb for thing in indfd] + [thing + nb + nf for thing in inddd]
            areas = Ab + [Af[i] for i in efd] + [Af[i] for i in edd]
            Abarj = []
            for i in range(len(indices)):
                Abarj.append([indices[i], areas[i]])
        else:
            raise Exception('something is wrong here')

    else:
        raise Exception('the getMaterial function only supports quadrilateral panels')
    return Abarj, kpbj


def superLinearBend(he, h0, Kp, L0):
    """
    inputs
    he: deformed bending angles
    h0: neutral angles
    Kp: stiffness parameter(s)
    L0: hinge lengths

    outputs
    Rspr: resistant moment
    Kspr: tangent rotational modulus
    Espr: stored energy
    """
    Rspr = np.sign(he-h0) * np.absolute(he-h0)**(4/3) * Kp * L0
    Kspr = np.maximum(4/3 * np.absolute(he-h0)**(1/3) * Kp, 0.001*Kp) * L0
    Espr = 3/7 * np.absolute(he-h0)**(7/3) * Kp * L0
    # print(h0)
    return Rspr, Kspr, Espr


def enhancedLinear(he, h0, kpi, L0, limlft, limrht):
    """
    inputs
    he: deformed folding angles
    h0: neutral angles
    kpi: stiffness parameter(s)
    L0: hinge lengths
    limlft: left limit
    limrht: right limit

    outputs
    Rspr: resistant moment
    Kspr: tangent rotational modulus
    Espr: stored energy
    """
    limlft = limlft / 180 * pi
    limrht = limrht / 180 * pi
    if isinstance(limlft, float):
        limlft = limlft * np.ones(len(he))
    if isinstance(limrht, float):
        limrht = limrht * np.ones(len(he))
    partl = pi / limlft
    partr = pi / (2*pi - limrht)
    if isinstance(kpi, float):
        kpi = kpi * np.ones(len(he))

    Rspr = np.zeros(len(he))
    Kspr = np.zeros(len(he))

    Lind = he < limlft
    Rind = he > limrht
    Mind = np.logical_not(np.logical_or(Lind, Rind))

    Rspr[Lind] = kpi[Lind] * np.real(limlft[Lind] - h0[Lind]) + \
        kpi[Lind] * np.tan(partl[Lind]/2 * (he[Lind] - limlft[Lind])) / (partl[Lind] / 2)
    Kspr[Lind] = kpi[Lind] * (1 / np.cos(partl[Lind] / 2 * (he[Lind] - limlft[Lind])))**2
    Rspr[Rind] = kpi[Rind] * np.real(limrht[Rind] - h0[Rind]) + \
        kpi[Rind] * np.tan(partr[Rind]/2 * (he[Rind] - limrht[Rind])) / (partr[Rind] / 2)
    Kspr[Rind] = kpi[Rind] * (1 / np.cos(partr[Rind] / 2 * (he[Rind] - limrht[Rind]))) ** 2
    Rspr[Mind] = kpi[Mind] * np.real(he[Mind] - h0[Mind])
    Kspr[Mind] = kpi[Mind]
    Rspr = L0 * Rspr
    Kspr = L0 * Kspr

    Espr = np.zeros(len(he))
    Espr[Lind] = 0.5 * kpi[Lind] * np.real(h0[Lind] - limlft[Lind])**2 + \
        kpi[Lind] * np.real(h0[Lind] - limlft[Lind]) * (limlft[Lind] - he[Lind]) - \
        4*kpi[Lind] / partl[Lind]**2 * np.log(np.absolute(np.cos(partl[Lind]/2 * (limlft[Lind] - he[Lind]))))
    Espr[Rind] = 0.5 * kpi[Rind] * np.real(limrht[Rind] - h0[Rind])**2 + \
        kpi[Rind] * np.real(limrht[Rind] - h0[Rind]) * (he[Rind] - limrht[Rind]) - \
        4*kpi[Rind] / partr[Rind]**2 * np.log(np.absolute(np.cos(partr[Rind]/2 * (he[Rind] - limrht[Rind]))))
    Espr[Mind] = 0.5 * kpi[Mind] * np.real(he[Mind] - h0[Mind])**2
    Espr = L0 * Espr

    return Rspr, Kspr, Espr
