import numpy as np
from math import cos, sin, acos
import csv
import time
from core.part1_prepare_data import prepareData
from core.part2_path_analysis_sparse import arc_length_improved_with_target_load_factor
from core.construct_pattern import (miura_supports_step1_v2, miura_loads_step1_v2)
from core.help_functions import (read_from_input, create_output_folder, make_vtk_data, make_vtk_file, bottom_nodes,
                                 create_new_nodes)
from scipy.optimize import minimize
from core.directories import root_dir

np.set_printoptions(edgeitems=30, linewidth=100000)


def quaternion_multiplication(q1, q2):
    w1, x1, y1, z1 = q1
    w2, x2, y2, z2 = q2
    w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2
    x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2
    y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2
    z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2
    q = (w, x, y, z)
    return q


def quat_vec_inv_quat(q, v):
    v = (0, v[0], v[1], v[2])
    q_inv = (q[0], -q[1], -q[2], -q[3])
    qv = quaternion_multiplication(q, v)
    qvq_inv = quaternion_multiplication(qv, q_inv)
    rotated_v = (qvq_inv[1], qvq_inv[2], qvq_inv[3])
    return rotated_v


def bottom_node_indices(size):
    cols, rows = size[0], size[1]

    indices = []
    for i in range(cols + 1):
        for j in range(rows // 2 + 1):
            indices.append(i * (rows + 1) + 2 * j)
    return indices


def make_nodes_from_coords(flat_coords, size, symmetric=True, middle_zero=True):

    flat_nodes = []

    pairs = (
        (45, 27), (46, 28), (47, 29), (48, 30), (49, 31), (50, 32), (51, 33), (52, 34), (53, 35),
        (54, 18), (55, 19), (56, 20), (57, 21), (58, 22), (59, 23), (60, 24), (61, 25), (62, 26),
        (63, 9), (64, 10), (65, 11), (66, 12), (67, 13), (68, 14), (69, 15), (70, 16), (71, 17),
        (72, 0), (73, 1), (74, 2), (75, 3), (76, 4), (77, 5), (78, 6), (79, 7), (80, 8)
    )

    if symmetric and not middle_zero:
        n_unique_nodes = len(flat_coords)//2

        for n in range(n_unique_nodes):
            flat_nodes.append((flat_coords[2*n], flat_coords[2*n + 1], 0.0))

        for pair in pairs:
            _, left = pair
            flat_nodes.append((-flat_coords[2*left], flat_coords[2*left + 1], 0.0))

        # for i, node in enumerate(flat_nodes):
        #     print(i, node)

    elif symmetric and middle_zero:
        n_unique_nodes = ((size[0]+1) // 2) * (size[1] + 1)     # does not include middle row

        for n in range(n_unique_nodes):
            flat_nodes.append((flat_coords[2*n], flat_coords[2*n + 1], 0.0))

        for n in range(size[1] + 1):
            indx = 2 * n_unique_nodes + n
            flat_nodes.append((0.0, flat_coords[indx], 0.0))

        for pair in pairs:
            _, left = pair
            flat_nodes.append((-flat_coords[2*left], flat_coords[2*left + 1], 0.0))

    else:
        nnodes = len(flat_coords) // 2

        for n in range(nnodes):
            flat_nodes.append((flat_coords[2 * n], flat_coords[2 * n + 1], 0.0))

    return flat_nodes


def get_final_deformed_nodes(truss, Uhis):
    """
    Returns numpy array in the shape nnodes x 3
    """
    n_dof = Uhis.shape[0]

    undeformed_nodes = np.array(truss['nodes'])
    final_deformed_nodes = np.zeros((int(n_dof / 3), 3))
    final_deformed_nodes += undeformed_nodes

    # add displacements
    for dof in range(n_dof):
        node_index = dof // 3
        x_y_z = dof % 3
        final_deformed_nodes[node_index][x_y_z] += Uhis[dof, -1]

    return final_deformed_nodes


def get_inertia_tensor(nodes):
    Ixx = sum(node[1] ** 2 + node[2] ** 2 for node in nodes)
    Iyy = sum(node[0] ** 2 + node[2] ** 2 for node in nodes)
    Izz = sum(node[0] ** 2 + node[1] ** 2 for node in nodes)
    Ixy = -sum(node[0] * node[1] for node in nodes)
    Ixz = -sum(node[0] * node[2] for node in nodes)
    Iyz = -sum(node[1] * node[2] for node in nodes)

    inertia_tensor = np.array([[Ixx, Ixy, Ixz],
                               [Ixy, Iyy, Iyz],
                               [Ixz, Iyz, Izz]])
    return inertia_tensor


def print_principal_inertia(nodes):
    inertia_tensor = get_inertia_tensor(nodes)
    principle_moments, principle_axes = np.linalg.eig(inertia_tensor)
    print(principle_moments)
    print(principle_axes)


def center_on_origin(truss, Uhis):
    final_deformed_nodes = get_final_deformed_nodes(truss, Uhis)

    center_of_mass = np.average(final_deformed_nodes, 0)
    centered_nodes = [tuple(np.array(node) - center_of_mass) for node in truss['nodes']]

    return centered_nodes


def rotate_with_matrix(truss, Uhis, inertia_tensor, checking=False, pitch=0):
    principle_moments, principle_axes = np.linalg.eig(inertia_tensor)

    reworked_principle_axes = np.zeros((3, 3))
    for i in range(3):
        ind = np.argmax(np.absolute(principle_axes[:, i]))
        reworked_principle_axes[:, ind] = principle_axes[:, i]
    for i in range(3):
        if reworked_principle_axes[i, i] < 0:
            reworked_principle_axes[:, i] = -1 * reworked_principle_axes[:, i]

    R = reworked_principle_axes.T  # actually np.eye(3) @ principle_axes.T

    extra_pitch = np.array([[1, 0,          0],
                            [0, cos(pitch), -sin(pitch)],
                            [0, sin(pitch), cos(pitch)]])
    R = extra_pitch @ R

    nodes = np.array([[node[0], node[1], node[2]] for node in truss['nodes']])

    rotated_nodes = (R @ nodes.T).T
    rotated_nodes = [(rotated_nodes[i, 0], rotated_nodes[i, 1], rotated_nodes[i, 2])
                     for i in range(rotated_nodes.shape[0])]

    rotated_Uhis = np.zeros(Uhis.shape)
    nnodes = Uhis.shape[0] // 3
    for i in range(Uhis.shape[1]):
        Uhis_i = Uhis[:, i]
        Uhis_i_reshaped = np.zeros((3, nnodes))
        for j in range(Uhis_i.shape[0]):
            Uhis_i_reshaped[j % 3, j // 3] = Uhis_i[j]

        Uhis_i_rotated = (R @ Uhis_i_reshaped).T        # nnodes x 3
        Uhis_i_rotated_reshaped = np.zeros((3*nnodes, 1))   # ndofs x 1
        for j in range(Uhis_i_rotated_reshaped.shape[0]):
            Uhis_i_rotated_reshaped[j] = Uhis_i_rotated[j // 3, j % 3]

        rotated_Uhis[:, i] = Uhis_i_rotated_reshaped[:, 0]

    if checking:
        truss['nodes'] = rotated_nodes
        final_deformed_nodes = get_final_deformed_nodes(truss, rotated_Uhis)
        print_principal_inertia(final_deformed_nodes)

    return rotated_nodes, rotated_Uhis


def rotate_with_quaternions(truss, Uhis, inertia_tensor, checking=False):
    principle_moments, principle_axes = np.linalg.eig(inertia_tensor)

    reworked_principle_axes = np.zeros((3, 3))
    for i in range(3):
        ind = np.argmax(np.absolute(principle_axes[:, i]))
        reworked_principle_axes[:, ind] = principle_axes[:, i]
    for i in range(3):
        if reworked_principle_axes[i, i] < 0:
            reworked_principle_axes[:, i] = -1 * reworked_principle_axes[:, i]
    p = reworked_principle_axes

    angle = acos((np.trace(p) - 1)/2)
    axis = np.array([p[1, 2] - p[2, 1],
                     p[2, 0] - p[0, 2],
                     p[0, 1] - p[1, 0]
                     ])
    axis = axis / np.linalg.norm(axis)

    q = (cos(angle/2), sin(angle/2)*axis[0], sin(angle/2)*axis[1], sin(angle/2)*axis[2])

    nodes = [(node[0], node[1], node[2]) for node in truss['nodes']]

    rotated_nodes = [quat_vec_inv_quat(q, node) for node in nodes]

    rotated_Uhis = np.zeros(Uhis.shape)
    nnodes = Uhis.shape[0] // 3

    for icrm in range(Uhis.shape[1]):
        Uhis_icrm = Uhis[:, icrm]
        for node_index in range(nnodes):
            Uhis_icrm_node = Uhis_icrm[3 * node_index: 3 * node_index + 3]

            rotated_Uhis_icrm_node = np.array(quat_vec_inv_quat(q, tuple(Uhis_icrm_node)))
            rotated_Uhis[3 * node_index: 3 * node_index + 3, icrm] = rotated_Uhis_icrm_node.copy()

    if checking:
        truss['nodes'] = rotated_nodes
        final_deformed_nodes = get_final_deformed_nodes(truss, rotated_Uhis)
        print_principal_inertia(final_deformed_nodes)

    return rotated_nodes, rotated_Uhis


def rotate_rigidly(truss, Uhis, checking=False, pitch=0):
    final_deformed_nodes = get_final_deformed_nodes(truss, Uhis)

    inertia_tensor = get_inertia_tensor(final_deformed_nodes)

    # rotated_nodes, rotated_Uhis = rotate_with_quaternions(truss, Uhis, inertia_tensor, checking)
    rotated_nodes, rotated_Uhis = rotate_with_matrix(truss, Uhis, inertia_tensor, checking, pitch)

    return rotated_nodes, rotated_Uhis


def translate_bottom_to_origin(truss, Uhis, size, ytrans, ztrans):
    final_deformed_nodes = get_final_deformed_nodes(truss, Uhis)

    # selected_nodes = [31, 40, 49]
    nr_x_nodes, nr_y_nodes = size[0] + 1, size[1] + 1
    selected_nodes = [nr_y_nodes // 2 + (nr_y_nodes * (nr_x_nodes // 2 + j)) for j in [-1, 0, 1]]
    selection_final_deformed_nodes = np.array([final_deformed_nodes[i] for i in selected_nodes])

    selection_center_of_mass = np.average(selection_final_deformed_nodes, 0)

    translated_nodes = [tuple(np.array(node) - selection_center_of_mass) for node in truss['nodes']]

    translated_nodes = [tuple(np.array(node) + np.array([0, ytrans, ztrans])) for node in translated_nodes]

    return translated_nodes


def rigid_motion(truss, Uhis, size, pitch, ytrans, ztrans):
    print(f'pitch, ytrans, ztrans: {pitch}, {ytrans}, {ztrans}')
    truss['nodes'] = center_on_origin(truss, Uhis)
    truss['nodes'], Uhis = rotate_rigidly(truss, Uhis, checking=False, pitch=pitch)
    truss['nodes'] = translate_bottom_to_origin(truss, Uhis, size, ytrans, ztrans)

    return truss, Uhis


def initial_compression(flat_nodes, panels, size, analyInputOpt, shape='saddle'):
    maxIcr, maxLmd, tol, initial_load_factor, multiply_load = (
        analyInputOpt['maxIcr'], analyInputOpt['maxLmd'], analyInputOpt['tol'],
        analyInputOpt['initialLoadFactor'], analyInputOpt['multiply_load'])

    analyInputOpt['tol'] = 1e-5
    analyInputOpt['multiply_load'] = 1

    nodes_step_1 = initial_rigid_motion(flat_nodes)

    if shape == 'saddle':
        analyInputOpt['maxIcr'] = 50
        analyInputOpt['initialLoadFactor'] = 15
        analyInputOpt['maxLmd'] = 43000

    elif shape == 'dome':
        analyInputOpt['maxIcr'] = 70
        analyInputOpt['initialLoadFactor'] = 50
        analyInputOpt['maxLmd'] = 43000

    elif shape == 'arch':
        analyInputOpt['maxIcr'] = 70
        analyInputOpt['initialLoadFactor'] = 50
        analyInputOpt['maxLmd'] = 50000

    else:
        raise Exception('invalid shape')

    supports_step_1 = miura_supports_step1_v2(size)
    loads_step_1 = miura_loads_step1_v2(size)
    truss, angles, AnalyInputOpt = prepareData(nodes_step_1, panels, supports_step_1, loads_step_1, analyInputOpt)
    Uhis, Fhis = arc_length_improved_with_target_load_factor(truss, angles, AnalyInputOpt)
    nodes2 = create_new_nodes(nodes_step_1, Uhis)

    analyInputOpt['maxIcr'] = maxIcr
    analyInputOpt['maxLmd'] = maxLmd
    analyInputOpt['tol'] = tol
    analyInputOpt['initialLoadFactor'] = initial_load_factor
    analyInputOpt['multiply_load'] = multiply_load

    return nodes2, truss, angles, Uhis, Fhis


def get_bounds(initial_design, size, symmetric, middle_zero,
               only_forces, only_coords, forces_and_pitch, coords_forces_and_pitch,
               forces_and_rbm, coords_forces_and_rbm, coords_and_rbm):

    if only_forces:
        bounds = []
        for force in initial_design:
            sign = np.sign(force)
            if sign < 0:
                bounds.append((5*force, 0.2*force))
            else:
                bounds.append((0.2*force, 5*force))

    elif forces_and_pitch:
        bounds = []
        forces = initial_design[:-1]
        pitch = initial_design[-1]

        for force in forces:
            sign = np.sign(force)
            if sign < 0:
                bounds.append((5 * force, 0.2 * force))
            else:
                bounds.append((0.2 * force, 5 * force))

        bounds.append((pitch-0.5, pitch+0.5))

    elif only_coords:
        if symmetric and not middle_zero:
            flat_coords = list(initial_design)
        elif symmetric and middle_zero:
            flat_coords = list(initial_design)
        else:
            raise NotImplementedError

        bounds = []
        coord_max_dev = 0.005

        for flat_coord in flat_coords:
            bounds.append((flat_coord - coord_max_dev, flat_coord + coord_max_dev))

    elif coords_forces_and_pitch:
        if symmetric and middle_zero:
            n_coords = 2 * (size[0] // 2) * (size[1] + 1) + size[1] + 1
            flat_coords_and_forces_and_pitch = list(initial_design)

            flat_coords = flat_coords_and_forces_and_pitch[:n_coords]
            forces = flat_coords_and_forces_and_pitch[n_coords:-1]
            pitch = flat_coords_and_forces_and_pitch[-1]

        else:
            raise NotImplementedError

        bounds = []
        coord_max_dev = 0.01
        force_max_factor = 5
        pitch_max_dev = 0.5

        for flat_coord in flat_coords:
            bounds.append((flat_coord - coord_max_dev, flat_coord + coord_max_dev))

        for force in forces:
            sign = np.sign(force)
            if sign < 0:
                bounds.append((force * force_max_factor, force / force_max_factor))
            else:
                bounds.append((force / force_max_factor, force * force_max_factor))

        bounds.append((pitch - pitch_max_dev, pitch + pitch_max_dev))

    elif forces_and_rbm:
        bounds = []
        forces = initial_design[:-3]
        pitch, ytrans, ztrans = initial_design[-3:]

        for force in forces:
            sign = np.sign(force)
            if sign < 0:
                bounds.append((5 * force, 0.2 * force))
            else:
                bounds.append((0.2 * force, 5 * force))

        bounds.append((pitch - 0.5, pitch + 0.5))
        bounds.append((ytrans - 0.1, ytrans + 0.1))
        bounds.append((ztrans - 0.1, ztrans + 0.1))

    elif coords_forces_and_rbm:
        if symmetric and middle_zero:
            n_coords = 2 * (size[0] // 2) * (size[1] + 1) + size[1] + 1
            flat_coords_and_forces_and_rbm = list(initial_design)

            flat_coords = flat_coords_and_forces_and_rbm[:n_coords]
            forces = flat_coords_and_forces_and_rbm[n_coords:-3]
            pitch, ytrans, ztrans = flat_coords_and_forces_and_rbm[-3:]

        else:
            raise NotImplementedError

        bounds = []
        coord_max_dev = 0.01
        force_max_factor = 5
        pitch_max_dev = 0.5
        ytrans_max_dev = 0.1
        ztrans_max_dev = 0.1

        for flat_coord in flat_coords:
            bounds.append((flat_coord - coord_max_dev, flat_coord + coord_max_dev))

        for force in forces:
            sign = np.sign(force)
            if sign < 0:
                bounds.append((force * force_max_factor, force / force_max_factor))
            else:
                bounds.append((force / force_max_factor, force * force_max_factor))

        bounds.append((pitch - pitch_max_dev, pitch + pitch_max_dev))
        bounds.append((ytrans - ytrans_max_dev, ytrans + ytrans_max_dev))
        bounds.append((ztrans - ztrans_max_dev, ztrans + ztrans_max_dev))

    elif coords_and_rbm:
        if symmetric and middle_zero:
            n_coords = 2 * (size[0] // 2) * (size[1] + 1) + size[1] + 1
            flat_coords_and_forces_and_rbm = list(initial_design)

            flat_coords = flat_coords_and_forces_and_rbm[:n_coords]
            pitch, ytrans, ztrans = flat_coords_and_forces_and_rbm[-3:]

        else:
            raise NotImplementedError

        bounds = []
        coord_max_dev = 0.03    # 0.01 is normal
        pitch_max_dev = 0.3
        ytrans_max_dev = 0.1
        ztrans_max_dev = 0.1

        for flat_coord in flat_coords:
            bounds.append((flat_coord - coord_max_dev, flat_coord + coord_max_dev))

        bounds.append((pitch - pitch_max_dev, pitch + pitch_max_dev))
        bounds.append((ytrans - ytrans_max_dev, ytrans + ytrans_max_dev))
        bounds.append((ztrans - ztrans_max_dev, ztrans + ztrans_max_dev))

    else:
        if symmetric and not middle_zero:
            n_unique_nodes = ((size[0] + 1) // 2 + 1) * (size[1] + 1)
            flat_coords_and_forces = list(initial_design)

            flat_coords = flat_coords_and_forces[:2 * n_unique_nodes]
            forces = flat_coords_and_forces[2 * n_unique_nodes:]

        elif symmetric and middle_zero:
            n_coords = 2 * (size[0] // 2) * (size[1] + 1) + size[1] + 1
            flat_coords_and_forces = list(initial_design)

            flat_coords = flat_coords_and_forces[:n_coords]
            forces = flat_coords_and_forces[n_coords:]

        else:
            # non-symmetric version not implemented
            raise NotImplementedError

        bounds = []
        coord_max_dev = 0.005
        force_max_factor = 1.1

        for flat_coord in flat_coords:
            bounds.append((flat_coord - coord_max_dev, flat_coord + coord_max_dev))
        for force in forces:
            my_sign = np.sign(force)
            if my_sign < 0:
                bounds.append((-abs(force)*force_max_factor, -abs(force)/force_max_factor))
            elif my_sign > 0:
                bounds.append((abs(force)/force_max_factor, abs(force)*force_max_factor))
            else:
                raise Exception('something is wrong with the bounds')

    return bounds


def max_pseudo_distance_from_shape(origami_bottom, shape, smoothing=False):

    def pseudo_distance_from_shape(my_node):
        if shape == 'saddle':
            a, b = 0.8, 0.8
            x, y, z = my_node[0], my_node[1], my_node[2]
            result = abs(x**2 / a**2 - y**2 / b**2 - z)
        elif shape == 'dome':
            a, b = 0.9, 0.9
            x, y, z = my_node[0], my_node[1], my_node[2]
            result = abs(-x**2 / a**2 - y**2 / b**2 - z)
        elif shape == 'arch':
            b = 0.6
            x, y, z = my_node[0], my_node[1], my_node[2]
            result = abs(- y**2 / b**2 - z)
        else:
            raise Exception('shape not allowed')
        return result

    if smoothing:
        p = 3
        max_pseudo_distance = sum([pseudo_distance_from_shape(node)**p for node in origami_bottom])**(1/p)

    else:
        max_pseudo_distance = max([pseudo_distance_from_shape(node) for node in origami_bottom])

    return max_pseudo_distance


def initial_rigid_motion(nodes):
    # translating center of mass to origin
    center_of_mass = np.average(np.array([list(node) for node in nodes]), 0)
    nodes = [tuple(np.array(node) - center_of_mass) for node in nodes]

    # rotating to align with coordinate system
    inertia_tensor = get_inertia_tensor(nodes)

    principle_moments, principle_axes = np.linalg.eig(inertia_tensor)

    reworked_principle_axes = np.zeros((3, 3))
    for i in range(3):
        ind = np.argmax(np.absolute(principle_axes[:, i]))
        reworked_principle_axes[:, ind] = principle_axes[:, i]
    for i in range(3):
        if reworked_principle_axes[i, i] < 0:
            reworked_principle_axes[:, i] = -1 * reworked_principle_axes[:, i]
    p = reworked_principle_axes.T

    nodes = [tuple(p@np.array(node)) for node in nodes]

    # translating bottom to shape
    selected_nodes = [31, 40, 49]
    selection_final_deformed_nodes = np.array([list(nodes[i]) for i in selected_nodes])
    selection_center_of_mass = np.average(selection_final_deformed_nodes, 0)
    nodes = [tuple(np.array(node) - selection_center_of_mass) for node in nodes]

    return nodes


def objective(nodes, panels, supports, loads, analyInputOpt, shape, config, size, files,
              symmetric, middle_zero, only_forces, only_coords, forces_and_pitch, coords_forces_and_pitch,
              forces_and_rbm, coords_forces_and_rbm, coords_and_rbm, design_variables):

    counter = 0
    if files:
        # count the number of objective function evaluations
        filename = f'{root_dir}/outputs/{config}/counter.txt'
        with open(filename, 'r') as file:
            for line in file:
                counter = int(line.strip())
        counter += 1
        with open(filename, 'w') as file:
            file.write(f'{counter}')
    else:
        # count the number of objective function evaluations
        filename = f'{root_dir}/outputs/{config}/counter2.txt'
        with open(filename, 'r') as file:
            for line in file:
                counter = int(line.strip())
        counter += 1
        with open(filename, 'w') as file:
            file.write(f'{counter}')

    pitch, ytrans, ztrans = 0, 0, 0
    if only_forces:
        forces = list(design_variables)

        nodes2 = nodes  # initial compression is already done

        print(forces)
        if len(forces) != 6:
            raise Exception('number of unique point forces should be six')
        for num, xyz, force_num, p_or_m in [(0, 2, 0, 1), (0, 3, 1, 1), (1, 2, 2, 1), (1, 3, 3, 1), (2, 1, 4, 1),
                                            (2, 3, 5, 1), (3, 1, 4, 1), (3, 3, 5, 1), (4, 1, 4, -1), (4, 3, 5, 1),
                                            (5, 1, 4, -1), (5, 3, 5, 1)]:
            loads[num][xyz] = p_or_m*forces[force_num]

    elif forces_and_pitch:
        forces = design_variables[:-1]
        pitch = design_variables[-1]

        nodes2 = nodes  # initial compression is already done

        print(forces)
        if len(forces) != 6:
            raise Exception('number of unique point forces should be six')
        for num, xyz, force_num, p_or_m in [(0, 2, 0, 1), (0, 3, 1, 1), (1, 2, 2, 1), (1, 3, 3, 1), (2, 1, 4, 1),
                                            (2, 3, 5, 1), (3, 1, 4, 1), (3, 3, 5, 1), (4, 1, 4, -1), (4, 3, 5, 1),
                                            (5, 1, 4, -1), (5, 3, 5, 1)]:
            loads[num][xyz] = p_or_m*forces[force_num]

    elif only_coords:
        flat_coords = list(design_variables)
        flat_nodes = make_nodes_from_coords(flat_coords, size, symmetric, middle_zero)

        # initial compression to make the origami 3D
        nodes2, _, _, _, _ = initial_compression(flat_nodes, panels, size, analyInputOpt)

    elif coords_forces_and_pitch:
        if symmetric and middle_zero:
            design_variables = list(design_variables)

            n_coords = 2 * (size[0] // 2) * (size[1] + 1) + size[1] + 1
            flat_coords = design_variables[:n_coords]
            flat_nodes = make_nodes_from_coords(flat_coords, size, symmetric, middle_zero)

            if files:
                # save uncompressed config
                truss, angles, analyInputOpt = prepareData(flat_nodes, panels, supports, loads, analyInputOpt)
                Uhis = np.zeros((435, 2))
                nodes, triangles = make_vtk_data(truss, Uhis)
                make_vtk_file(nodes[-1], triangles, config,
                              filepath=f'{root_dir}/outputs/{config}/{config}-before-compression-{counter}.vtk')

            # initial compression to make the origami 3D
            nodes2, _, _, _, _ = initial_compression(flat_nodes, panels, size, analyInputOpt)

            forces = design_variables[n_coords:-1]

            print(forces)
            if len(forces) != 6:
                raise Exception('number of unique point forces should be six')
            for num, xyz, force_num, p_or_m in [(0, 2, 0, 1), (0, 3, 1, 1), (1, 2, 2, 1), (1, 3, 3, 1), (2, 1, 4, 1),
                                                (2, 3, 5, 1), (3, 1, 4, 1), (3, 3, 5, 1), (4, 1, 4, -1), (4, 3, 5, 1),
                                                (5, 1, 4, -1), (5, 3, 5, 1)]:
                loads[num][xyz] = p_or_m * forces[force_num]

            pitch = design_variables[-1]

        else:
            raise NotImplementedError

    elif forces_and_rbm:
        forces = design_variables[:-3]
        pitch, ytrans, ztrans = design_variables[-3:]

        nodes2 = nodes  # initial compression is already done

        print(f'forces: {forces}')
        # if len(forces) != 8:
        #     raise Exception('number of unique point forces should be eight')
        # for num, xyz, force_num, p_or_m in [(0, 2, 0, 1), (0, 3, 1, 1), (1, 2, 2, 1), (1, 3, 3, 1), (2, 1, 4, 1),
        #                                     (2, 3, 5, 1), (3, 1, 6, 1), (3, 3, 7, 1), (4, 1, 4, -1), (4, 3, 5, 1),
        #                                     (5, 1, 6, -1), (5, 3, 7, 1)]:
        #     loads[num][xyz] = p_or_m*forces[force_num]

        if len(forces) != 3:
            raise Exception('number of unique point forces should be three')
        for num, xyz, force_num, factor in [(0, 2, 0, 0.2), (0, 3, 0, -1), (1, 2, 1, -0.2), (1, 3, 1, -1),
                                            (2, 1, 2, 0.5), (2, 3, 2, 1), (3, 1, 2, 0.5), (3, 3, 2, 1),
                                            (4, 1, 2, -0.5), (4, 3, 2, 1), (5, 1, 2, -0.5), (5, 3, 2, 1)]:
            loads[num][xyz] = factor*forces[force_num]
        #
        # if len(forces) != 1:
        #     raise Exception('number of unique point forces should be one')
        # for num, xyz, force_num, factor in [(0, 1, 0, 0.5), (0, 3, 0, 1), (1, 1, 0, 0.5), (1, 3, 0, 1),
        #                                     (2, 1, 0, -0.5), (2, 3, 0, 1), (3, 1, 0, -0.5), (3, 3, 0, 1)]:
        #     loads[num][xyz] = factor*forces[force_num]

    elif coords_forces_and_rbm:
        if symmetric and middle_zero:
            design_variables = list(design_variables)

            n_coords = 2 * (size[0] // 2) * (size[1] + 1) + size[1] + 1
            flat_coords = design_variables[:n_coords]
            flat_nodes = make_nodes_from_coords(flat_coords, size, symmetric, middle_zero)

            if files:
                # save uncompressed config
                truss, angles, analyInputOpt = prepareData(flat_nodes, panels, supports, loads, analyInputOpt)
                Uhis = np.zeros((435, 2))
                nodes, triangles = make_vtk_data(truss, Uhis)
                make_vtk_file(nodes[-1], triangles, config,
                              filepath=f'{root_dir}/outputs/{config}/{config}-before-compression-{counter}.vtk')

            # initial compression to make the origami 3D
            nodes2, _, _, _, _ = initial_compression(flat_nodes, panels, size, analyInputOpt)

            forces = design_variables[n_coords:-3]

            print(f'forces: {forces}')
            # if len(forces) != 8:
            #     raise Exception('number of unique point forces should be eight')
            # for num, xyz, force_num, p_or_m in [(0, 2, 0, 1), (0, 3, 1, 1), (1, 2, 2, 1), (1, 3, 3, 1), (2, 1, 4, 1),
            #                                     (2, 3, 5, 1), (3, 1, 6, 1), (3, 3, 7, 1), (4, 1, 4, -1), (4, 3, 5, 1),
            #                                     (5, 1, 6, -1), (5, 3, 7, 1)]:
            #     loads[num][xyz] = p_or_m * forces[force_num]

            if len(forces) != 3:
                raise Exception('number of unique point forces should be three')
            for num, xyz, force_num, factor in [(0, 2, 0, 0.2), (0, 3, 0, -1), (1, 2, 1, -0.2), (1, 3, 1, -1),
                                                (2, 1, 2, 0.5), (2, 3, 2, 1), (3, 1, 2, 0.5), (3, 3, 2, 1),
                                                (4, 1, 2, -0.5), (4, 3, 2, 1), (5, 1, 2, -0.5), (5, 3, 2, 1)]:
                loads[num][xyz] = factor * forces[force_num]

            pitch, ytrans, ztrans = design_variables[-3:]

        else:
            raise NotImplementedError

    elif coords_and_rbm:
        if symmetric and middle_zero:
            design_variables = list(design_variables)

            n_coords = 2 * (size[0] // 2) * (size[1] + 1) + size[1] + 1
            flat_coords = design_variables[:n_coords]
            flat_nodes = make_nodes_from_coords(flat_coords, size, symmetric, middle_zero)

            if files:
                # save uncompressed config
                truss, angles, analyInputOpt = prepareData(flat_nodes, panels, supports, loads, analyInputOpt)
                Uhis = np.zeros((435, 2))
                nodes, triangles = make_vtk_data(truss, Uhis)
                make_vtk_file(nodes[-1], triangles, config,
                              filepath=f'{root_dir}/outputs/{config}/{config}-before-compression-{counter}.vtk')
            else:
                # save uncompressed config
                truss, angles, analyInputOpt = prepareData(flat_nodes, panels, supports, loads, analyInputOpt)
                Uhis = np.zeros((435, 2))
                nodes, triangles = make_vtk_data(truss, Uhis)
                make_vtk_file(nodes[-1], triangles, config,
                              filepath=f'{root_dir}/outputs/{config}/itr-calcs/{config}-before-compression-{counter}.vtk')

            # initial compression to make the origami 3D
            nodes2, _, _, _, _ = initial_compression(flat_nodes, panels, size, analyInputOpt, shape=shape)

            pitch, ytrans, ztrans = design_variables[-3:]

        else:
            raise NotImplementedError

    else:
        if symmetric and not middle_zero:
            design_variables = list(design_variables)

            n_unique_nodes = ((size[0]+1) // 2 + 1) * (size[1] + 1)
            flat_coords = design_variables[:2 * n_unique_nodes]
            flat_nodes = make_nodes_from_coords(flat_coords, size, symmetric, middle_zero)

            # initial compression to make the origami 3D
            nodes2 = initial_compression(flat_nodes, panels, size, analyInputOpt)

            # not used anymore as we have the mechanical initial compression
            # nodes = adapt_nodes(flat_nodes, size, 0.03)

            forces = design_variables[2 * n_unique_nodes:]
            print(forces)
            if len(forces) != 6:
                raise Exception('number of unique point forces should be six')
            for num, xyz, force_num, p_or_m in [(0, 2, 0, 1), (0, 3, 1, 1), (1, 2, 2, 1), (1, 3, 3, 1), (2, 1, 4, 1),
                                                (2, 3, 5, 1), (3, 1, 4, 1), (3, 3, 5, 1), (4, 1, 4, -1), (4, 3, 5, 1),
                                                (5, 1, 4, -1), (5, 3, 5, 1)]:
                loads[num][xyz] = p_or_m * forces[force_num]

        elif symmetric and middle_zero:
            design_variables = list(design_variables)

            n_coords = 2 * (size[0] // 2) * (size[1] + 1) + size[1] + 1
            flat_coords = design_variables[:n_coords]
            flat_nodes = make_nodes_from_coords(flat_coords, size, symmetric, middle_zero)

            if files:
                # save uncompressed config
                truss, angles, analyInputOpt = prepareData(flat_nodes, panels, supports, loads, analyInputOpt)
                Uhis = np.zeros((435, 2))
                nodes, triangles = make_vtk_data(truss, Uhis)
                make_vtk_file(nodes[-1], triangles, config,
                              filepath=f'{root_dir}/outputs/{config}/{config}-before-compression-{counter}.vtk')

            # initial compression to make the origami 3D
            nodes2 = initial_compression(flat_nodes, panels, size, analyInputOpt)

            forces = design_variables[n_coords:]

            print(forces)
            if len(forces) != 6:
                raise Exception('number of unique point forces should be six')
            for num, xyz, force_num, p_or_m in [(0, 2, 0, 1), (0, 3, 1, 1), (1, 2, 2, 1), (1, 3, 3, 1), (2, 1, 4, 1),
                                                (2, 3, 5, 1), (3, 1, 4, 1), (3, 3, 5, 1), (4, 1, 4, -1), (4, 3, 5, 1),
                                                (5, 1, 4, -1), (5, 3, 5, 1)]:
                loads[num][xyz] = p_or_m * forces[force_num]

        else:
            nnodes = (size[0] + 1) * (size[1] + 1)
            design_variables = list(design_variables)

            flat_coords = design_variables[:2 * nnodes]
            flat_nodes = make_nodes_from_coords(flat_coords, size, symmetric, middle_zero)

            # initial compression to make the origami 3D
            nodes2 = initial_compression(flat_nodes, panels, size, analyInputOpt)

            forces = design_variables[2 * nnodes:]
            print(forces)
            if len(forces) != 12:
                raise Exception('number of point forces should be twelve')
            for num, xyz, force_num, p_or_m in [(0, 2, 0, 1), (0, 3, 1, 1), (1, 2, 2, 1), (1, 3, 3, 1), (2, 1, 4, 1),
                                                (2, 3, 5, 1), (3, 1, 6, 1), (3, 3, 7, 1), (4, 1, 8, 1), (4, 3, 9, 1),
                                                (5, 1, 10, 1), (5, 3, 11, 1)]:
                loads[num][xyz] = p_or_m * forces[force_num]

    truss, angles, analyInputOpt = prepareData(nodes2, panels, supports, loads, analyInputOpt)

    if files:
        # save result after compression but before final mechanical calculation
        Uhis = np.zeros((435, 2))
        nodes, triangles = make_vtk_data(truss, Uhis)
        make_vtk_file(nodes[-1], triangles, config,
                      filepath=f'{root_dir}/outputs/{config}/{config}-before-calc-{counter}.vtk')
    else:
        # save result after compression but before final mechanical calculation
        Uhis = np.zeros((435, 2))
        nodes, triangles = make_vtk_data(truss, Uhis)
        make_vtk_file(nodes[-1], triangles, config,
                      filepath=f'{root_dir}/outputs/{config}/itr-calcs/{config}-before-calc-{counter}.vtk')

    Uhis, Fhis = arc_length_improved_with_target_load_factor(truss, angles, analyInputOpt)

    # nodes, triangles = make_vtk_data(truss, Uhis)
    # make_vtk_file(nodes[-1], triangles, config, filepath=f'{root_dir}/outputs/{config}/{config}-not-centered.vtk')

    # nodes, triangles = make_vtk_data(truss, Uhis)
    # make_vtk_sequence(nodes, triangles, config, [], [])

    truss, Uhis = rigid_motion(truss, Uhis, size, pitch, ytrans, ztrans)
    final_deformed_nodes = get_final_deformed_nodes(truss, Uhis)
    bottom_final_deformed_nodes = [list(final_deformed_nodes[i, :]) for i in bottom_nodes(size)]
    max_pseudo_distance = max_pseudo_distance_from_shape(bottom_final_deformed_nodes, shape, smoothing=False)

    if files:
        # save results after full calculation
        nodes, triangles = make_vtk_data(truss, Uhis)
        make_vtk_file(nodes[-1], triangles, config,
                      filepath=f'{root_dir}/outputs/{config}/{config}-after-calc-centered-{counter}.vtk')
    else:
        # save results after full calculation
        nodes, triangles = make_vtk_data(truss, Uhis)
        make_vtk_file(nodes[-1], triangles, config,
                      filepath=f'{root_dir}/outputs/{config}/itr-calcs/{config}-after-calc-centered-{counter}.vtk')

    # nodes, triangles = make_vtk_data(truss, Uhis)
    # make_vtk_file(nodes[-1], triangles, config, filepath=f'{root_dir}/outputs/{config}/{config}-not-centered.vtk')

    # print(f'smooth maximum pseudo distance: {max_pseudo_distance}')
    # print(f'non-smooth maximum pseudo distance: {max_pseudo_distance_from_shape(bottom_final_deformed_nodes, shape,
    #                                                                             smoothing=False)}')

    return max_pseudo_distance


def create_initial_design(nodes, loads, size, symmetric, middle_zero,
                          only_forces, only_coords, forces_and_pitch, coords_forces_and_pitch,
                          forces_and_rbm, coords_forces_and_rbm, coords_and_rbm):

    if only_forces:
        if symmetric:
            forces = [1e-1, -10e-1, -1e-1, -10e-1, 3e-1, 3e-1]
        else:
            raise NotImplementedError

        initial_design = np.array(forces)

    elif forces_and_pitch:
        if symmetric:
            forces = [1e-1, -10e-1, -1e-1, -10e-1, 3e-1, 3e-1]
            pitch = [0]
        else:
            raise NotImplementedError

        initial_design = np.array(forces + pitch)

    elif only_coords:
        if symmetric and not middle_zero:
            coords = []
            n_unique_nodes = ((size[0] + 1) // 2 + 1) * (size[1] + 1)   # includes middle column

            for node in nodes[:n_unique_nodes]:
                coords.append(node[0])
                coords.append(node[1])

        elif symmetric and middle_zero:
            coords = []
            n_unique_nodes = ((size[0] + 1) // 2) * (size[1] + 1)  # does not include middle column

            for node in nodes[:n_unique_nodes]:
                coords.append(node[0])
                coords.append(node[1])

            for node in nodes[n_unique_nodes:n_unique_nodes + size[1] + 1]:  # middle column
                coords.append(node[1])

        else:
            coords = []
            for node in nodes:
                coords.append(node[0])
                coords.append(node[1])

        initial_design = np.array(coords)

    elif coords_forces_and_pitch:
        if symmetric and middle_zero:
            coords = []
            n_unique_nodes = ((size[0] + 1) // 2) * (size[1] + 1)  # does not include middle column

            for node in nodes[:n_unique_nodes]:
                coords.append(node[0])
                coords.append(node[1])

            for node in nodes[n_unique_nodes:n_unique_nodes + size[1] + 1]:  # middle column
                coords.append(node[1])

            # forces = [1e-1, -10e-1, -1e-1, -10e-1, 3e-1, 3e-1]

            # forces = [0.021319781137010517, -1.6242197373296543, -0.1801418887902767, -0.570503643138763,
            #           0.2728589097962424, 0.26489482924599017]

            forces = [0.1400949652459335, -1.6966679330604628, -0.03821284544500702, -0.6923862158737945,
                      0.29675383474481287, 0.27843721442976077]

            pitch = [-4.523993353737398e-5]

        else:
            raise NotImplementedError

        initial_design = np.array(coords + forces + pitch)

    elif forces_and_rbm:
        if symmetric:
            # forces = [1e-1, -10e-1, -1e-1, -10e-1, 3e-1, 3e-1, 3e-1, 3e-1]
            # forces = [0.06429356382914342, -1.857227131912221, -0.09716458208729097, -0.8258974730545344,
            #           0.19536514644586836, 0.1305530704942906, 0.4731862613366985, 0.47107476356304256]
            # forces = [1.0, 1.0, 0.3]
            forces = [1.0, 1.0, 0.5]
        else:
            raise NotImplementedError

        rbm = [0, 0, 0]
        # rbm = [0.0007634685669833394, -0.002275175875814604, 0.0014292064265886435]

        initial_design = np.array(forces + rbm)

    elif coords_forces_and_rbm:
        if symmetric and middle_zero:
            coords = []
            n_unique_nodes = ((size[0] + 1) // 2) * (size[1] + 1)  # does not include middle column

            for node in nodes[:n_unique_nodes]:
                coords.append(node[0])
                coords.append(node[1])

            for node in nodes[n_unique_nodes:n_unique_nodes + size[1] + 1]:  # middle column
                coords.append(node[1])

            # forces = [1e-1, -10e-1, -1e-1, -10e-1, 3e-1, 3e-1]

            # forces = [0.021319781137010517, -1.6242197373296543, -0.1801418887902767, -0.570503643138763,
            #           0.2728589097962424, 0.26489482924599017]

            # fill in result of previous opt round
            # forces = [0.06429356382914342, -1.857227131912221, -0.09716458208729097, -0.8258974730545344,
            #           0.19536514644586836, 0.1305530704942906, 0.4731862613366985, 0.47107476356304256]
            # forces = [0.04706271753390269, -1.8741848120799358, -0.08034305537346292, -0.836881035966959,
            #           0.18180770601117188, 0.12177556011054574, 0.500000705019664, 0.46729202041549733]
            forces = [1.2489154110616827, 0.6710315503950343, 0.3465002910983214]

            # fill in result of previous opt round
            # rbm = [0.0007634685669833394, -0.002275175875814604, 0.0014292064265886435]
            rbm = [0.001731656957191198, 0.0011807392407184537, -0.003186093569168532]

        else:
            raise NotImplementedError

        initial_design = np.array(coords + forces + rbm)

    elif coords_and_rbm:
        if symmetric and middle_zero:
            coords = []
            n_unique_nodes = ((size[0] + 1) // 2) * (size[1] + 1)  # does not include middle column

            for node in nodes[:n_unique_nodes]:
                coords.append(node[0])
                coords.append(node[1])

            for node in nodes[n_unique_nodes:n_unique_nodes + size[1] + 1]:  # middle column
                coords.append(node[1])

            rbm = [0.0, 0.0, 0.0]

        else:
            raise NotImplementedError

        initial_design = np.array(coords + rbm)

    else:
        if symmetric and not middle_zero:
            coords = []
            n_unique_nodes = ((size[0] + 1) // 2 + 1) * (size[1] + 1)   # includes middle column

            for node in nodes[:n_unique_nodes]:
                coords.append(node[0])
                coords.append(node[1])

            # forces = [1e-4, -10e-4, -1e-4, -10e-4, 3e-4, 6e-4]
            # forces = [1e-1, -10e-1, -1e-1, -10e-1, 3e-1, 6e-1]
            forces = [0.021319781137010517, -1.6242197373296543, -0.1801418887902767,
                      -0.570503643138763, 0.2728589097962424, 0.26489482924599017]
        elif symmetric and middle_zero:
            coords = []
            n_unique_nodes = ((size[0] + 1) // 2) * (size[1] + 1)  # does not include middle column

            for node in nodes[:n_unique_nodes]:
                coords.append(node[0])
                coords.append(node[1])

            for node in nodes[n_unique_nodes:n_unique_nodes + size[1] + 1]:  # middle column
                coords.append(node[1])

            # forces = [1e-1, -10e-1, -1e-1, -10e-1, 3e-1, 3e-1]
            forces = [0.021319781137010517, -1.6242197373296543, -0.1801418887902767,
                      -0.570503643138763, 0.2728589097962424, 0.26489482924599017]

        else:
            coords = []
            for node in nodes:
                coords.append(node[0])
                coords.append(node[1])

            forces = []
            for load in loads:
                for i in [1, 2, 3]:
                    if load[i] != 0:
                        forces.append(load[i])

        coords_and_forces = np.array(coords + forces)
        initial_design = coords_and_forces

    return initial_design


def create_results_csv(x0, f0, config):
    filename = f'{root_dir}/outputs/{config}/minimize-results.csv'
    with open(filename, 'w') as file:
        my_writer = csv.DictWriter(file, fieldnames=['x', 'f'], delimiter=',')
        my_writer.writeheader()
        my_writer.writerow({'x': list(x0), 'f': f0})


def add_intermediate_optimization_results(nodes, panels, supports, loads, analyInputOpt, shape, config, size, files,
                                          symmetric, middle_zero, only_forces, only_coords, forces_and_pitch,
                                          coords_forces_and_pitch, forces_and_rbm, coords_forces_and_rbm,
                                          coords_and_rbm, intermediate_result):
    filename = f'{root_dir}/outputs/{config}/minimize-results.csv'
    print(f'intermediate result: {intermediate_result}')
    x = intermediate_result
    f = objective(nodes, panels, supports, loads, analyInputOpt, shape, config, size, files,
                  symmetric, middle_zero, only_forces, only_coords, forces_and_pitch, coords_forces_and_pitch,
                  forces_and_rbm, coords_forces_and_rbm, coords_and_rbm, x)

    # with open(filename, 'r') as file:
    #     temp_reader = csv.reader(file, delimiter=',')
    #     iteration = sum(1 for _ in temp_reader) - 1

    with open(filename, 'a') as file:
        my_writer = csv.DictWriter(file, fieldnames=['x', 'f'], delimiter=',')
        my_writer.writerow({'x': list(x), 'f': f})


def total_optimization(shape):
    start_time = time.perf_counter()

    config = f'{shape}-opt'
    create_output_folder(config)

    size = (8, 8)  # number of panels in each direction, not number of miura units
    files = True
    symmetric = True
    middle_zero = True
    only_forces = False
    only_coords = False
    forces_and_pitch = False
    coords_forces_and_pitch = False
    forces_and_rbm = False
    coords_forces_and_rbm = False
    coords_and_rbm = True
    nodes, panels, supports, loads, relevant_dof, analyInputOpt = read_from_input(config)

    nodes = initial_rigid_motion(nodes)

    x0 = create_initial_design(nodes, loads, size, symmetric, middle_zero, only_forces, only_coords, forces_and_pitch,
                               coords_forces_and_pitch, forces_and_rbm, coords_forces_and_rbm, coords_and_rbm)
    my_bounds = get_bounds(x0, size, symmetric, middle_zero, only_forces, only_coords, forces_and_pitch,
                           coords_forces_and_pitch, forces_and_rbm, coords_forces_and_rbm, coords_and_rbm)

    # do initial compression only once if the forces are the only thing
    if symmetric and middle_zero and (only_forces or forces_and_pitch or forces_and_rbm):
        coords = []
        n_unique_nodes = ((size[0] + 1) // 2) * (size[1] + 1)  # does not include middle column

        for node in nodes[:n_unique_nodes]:
            coords.append(node[0])
            coords.append(node[1])

        for node in nodes[n_unique_nodes:n_unique_nodes + size[1] + 1]:  # middle column
            coords.append(node[1])

        nodes = make_nodes_from_coords(coords, size, symmetric, middle_zero)

        nodes, _, _, _, _ = initial_compression(nodes, panels, size, analyInputOpt, shape=shape)

    def f(*args):
        return objective(nodes, panels, supports, loads, analyInputOpt, shape, config, size, files,
                         symmetric, middle_zero, only_forces, only_coords, forces_and_pitch, coords_forces_and_pitch,
                         forces_and_rbm, coords_forces_and_rbm, coords_and_rbm, *args)

    def add_intermediate_results(*args):
        return add_intermediate_optimization_results(nodes, panels, supports, loads, analyInputOpt, shape, config, size,
                                                     files, symmetric, middle_zero, only_forces, only_coords,
                                                     forces_and_pitch, coords_forces_and_pitch, forces_and_rbm,
                                                     coords_forces_and_rbm, coords_and_rbm, *args)

    # count the number of objective function evaluations
    filename = f'{root_dir}/outputs/{config}/counter.txt'
    with open(filename, 'w') as file:
        file.write('0')

    f0 = f(x0)

    create_results_csv(x0, f0, config)

    # final_result = minimize(f, x0, method='BFGS',
    #                         callback=add_intermediate_results,
    #                         options={
    #                             'finite_diff_rel_step': 1e-2,
    #                             'disp': True, 'return_all': True, 'maxiter': 5})

    final_result = minimize(f, x0, method='Nelder-Mead',
                            callback=add_intermediate_results,
                            bounds=my_bounds,
                            options={'disp': True, 'maxiter': 100000, 'maxfev': 100000, 'return_all': True,
                                     'xatol': 1e-5, 'fatol': 1e-5})

    optimal_design = final_result['x']
    optimal_objective = final_result['fun']

    print(f'optimal objective: {optimal_objective}')
    print(f'optimal design: {optimal_design}')

    end_time = time.perf_counter()
    print(f'optimization time: {round((end_time - start_time)/60, 0)} minutes')

    # truss = get_truss(optimal_nodes, panels)
    # make_vtk_file(truss['nodes'], truss['trigl'], config, 0,
    #               filepath=f'{root_dir}/outputs/{config}/{config}-optimal-8-8.vtk')

    return optimal_design, panels


if __name__ == '__main__':
    total_optimization('dome')
