import numpy as np
from math import cos, sin, acos
import csv
import time
from scipy.optimize import minimize

from core.part1_prepare_data import prepareData
from core.part2_path_analysis_sparse import arc_length_improved_with_target_load_factor
from core.construct_pattern import miura_supports_step1_v2, miura_loads_step1_v2
from core.help_functions import (read_from_input, create_output_folder, make_vtk_data, make_vtk_file, bottom_nodes,
                                 create_new_nodes, create_empty_dir)
from core.directories import root_dir


def bottom_node_indices(size):
    cols, rows = size[0], size[1]

    indices = []
    for i in range(cols + 1):
        for j in range(rows // 2 + 1):
            indices.append(i * (rows + 1) + 2 * j)
    return indices


def make_nodes_from_coords(flat_coords, size, symmetric=True, middle_zero=True):

    flat_nodes = []

    pairs = (
        (45, 27), (46, 28), (47, 29), (48, 30), (49, 31), (50, 32), (51, 33), (52, 34), (53, 35),
        (54, 18), (55, 19), (56, 20), (57, 21), (58, 22), (59, 23), (60, 24), (61, 25), (62, 26),
        (63, 9), (64, 10), (65, 11), (66, 12), (67, 13), (68, 14), (69, 15), (70, 16), (71, 17),
        (72, 0), (73, 1), (74, 2), (75, 3), (76, 4), (77, 5), (78, 6), (79, 7), (80, 8)
    )

    if symmetric and not middle_zero:
        n_unique_nodes = len(flat_coords)//2

        for n in range(n_unique_nodes):
            flat_nodes.append((flat_coords[2*n], flat_coords[2*n + 1], 0.0))

        for pair in pairs:
            _, left = pair
            flat_nodes.append((-flat_coords[2*left], flat_coords[2*left + 1], 0.0))

        # for i, node in enumerate(flat_nodes):
        #     print(i, node)

    elif symmetric and middle_zero:
        n_unique_nodes = ((size[0]+1) // 2) * (size[1] + 1)     # does not include middle row

        for n in range(n_unique_nodes):
            flat_nodes.append((flat_coords[2*n], flat_coords[2*n + 1], 0.0))

        for n in range(size[1] + 1):
            indx = 2 * n_unique_nodes + n
            flat_nodes.append((0.0, flat_coords[indx], 0.0))

        for pair in pairs:
            _, left = pair
            flat_nodes.append((-flat_coords[2*left], flat_coords[2*left + 1], 0.0))

    else:
        nnodes = len(flat_coords) // 2

        for n in range(nnodes):
            flat_nodes.append((flat_coords[2 * n], flat_coords[2 * n + 1], 0.0))

    return flat_nodes


def get_final_deformed_nodes(truss, Uhis):
    """
    Returns numpy array in the shape nnodes x 3
    """
    n_dof = Uhis.shape[0]

    undeformed_nodes = np.array(truss['nodes'])
    final_deformed_nodes = np.zeros((int(n_dof / 3), 3))
    final_deformed_nodes += undeformed_nodes

    # add displacements
    for dof in range(n_dof):
        node_index = dof // 3
        x_y_z = dof % 3
        final_deformed_nodes[node_index][x_y_z] += Uhis[dof, -1]

    return final_deformed_nodes


def get_inertia_tensor(nodes):
    Ixx = sum(node[1] ** 2 + node[2] ** 2 for node in nodes)
    Iyy = sum(node[0] ** 2 + node[2] ** 2 for node in nodes)
    Izz = sum(node[0] ** 2 + node[1] ** 2 for node in nodes)
    Ixy = -sum(node[0] * node[1] for node in nodes)
    Ixz = -sum(node[0] * node[2] for node in nodes)
    Iyz = -sum(node[1] * node[2] for node in nodes)

    inertia_tensor = np.array([[Ixx, Ixy, Ixz],
                               [Ixy, Iyy, Iyz],
                               [Ixz, Iyz, Izz]])
    return inertia_tensor


def print_principal_inertia(nodes):
    inertia_tensor = get_inertia_tensor(nodes)
    principle_moments, principle_axes = np.linalg.eig(inertia_tensor)
    print(principle_moments)
    print(principle_axes)


def center_on_origin(truss, Uhis):
    final_deformed_nodes = get_final_deformed_nodes(truss, Uhis)

    center_of_mass = np.average(final_deformed_nodes, 0)
    centered_nodes = [tuple(np.array(node) - center_of_mass) for node in truss['nodes']]

    return centered_nodes


def rotate_with_matrix(truss, Uhis, inertia_tensor, checking=False, pitch=0):
    principle_moments, principle_axes = np.linalg.eig(inertia_tensor)

    reworked_principle_axes = np.zeros((3, 3))
    for i in range(3):
        ind = np.argmax(np.absolute(principle_axes[:, i]))
        reworked_principle_axes[:, ind] = principle_axes[:, i]
    for i in range(3):
        if reworked_principle_axes[i, i] < 0:
            reworked_principle_axes[:, i] = -1 * reworked_principle_axes[:, i]

    R = reworked_principle_axes.T  # actually np.eye(3) @ principle_axes.T

    extra_pitch = np.array([[1, 0,          0],
                            [0, cos(pitch), -sin(pitch)],
                            [0, sin(pitch), cos(pitch)]])
    R = extra_pitch @ R

    nodes = np.array([[node[0], node[1], node[2]] for node in truss['nodes']])

    rotated_nodes = (R @ nodes.T).T
    rotated_nodes = [(rotated_nodes[i, 0], rotated_nodes[i, 1], rotated_nodes[i, 2])
                     for i in range(rotated_nodes.shape[0])]

    rotated_Uhis = np.zeros(Uhis.shape)
    nnodes = Uhis.shape[0] // 3
    for i in range(Uhis.shape[1]):
        Uhis_i = Uhis[:, i]
        Uhis_i_reshaped = np.zeros((3, nnodes))
        for j in range(Uhis_i.shape[0]):
            Uhis_i_reshaped[j % 3, j // 3] = Uhis_i[j]

        Uhis_i_rotated = (R @ Uhis_i_reshaped).T        # nnodes x 3
        Uhis_i_rotated_reshaped = np.zeros((3*nnodes, 1))   # ndofs x 1
        for j in range(Uhis_i_rotated_reshaped.shape[0]):
            Uhis_i_rotated_reshaped[j] = Uhis_i_rotated[j // 3, j % 3]

        rotated_Uhis[:, i] = Uhis_i_rotated_reshaped[:, 0]

    if checking:
        truss['nodes'] = rotated_nodes
        final_deformed_nodes = get_final_deformed_nodes(truss, rotated_Uhis)
        print_principal_inertia(final_deformed_nodes)

    return rotated_nodes, rotated_Uhis


def rotate_rigidly(truss, Uhis, checking=False, pitch=0):
    final_deformed_nodes = get_final_deformed_nodes(truss, Uhis)

    inertia_tensor = get_inertia_tensor(final_deformed_nodes)

    rotated_nodes, rotated_Uhis = rotate_with_matrix(truss, Uhis, inertia_tensor, checking, pitch)

    return rotated_nodes, rotated_Uhis


def translate_bottom_to_origin(truss, Uhis, size, ytrans, ztrans):
    final_deformed_nodes = get_final_deformed_nodes(truss, Uhis)

    # selected_nodes = [31, 40, 49]
    nr_x_nodes, nr_y_nodes = size[0] + 1, size[1] + 1
    selected_nodes = [nr_y_nodes // 2 + (nr_y_nodes * (nr_x_nodes // 2 + j)) for j in [-1, 0, 1]]
    selection_final_deformed_nodes = np.array([final_deformed_nodes[i] for i in selected_nodes])

    selection_center_of_mass = np.average(selection_final_deformed_nodes, 0)

    translated_nodes = [tuple(np.array(node) - selection_center_of_mass) for node in truss['nodes']]

    translated_nodes = [tuple(np.array(node) + np.array([0, ytrans, ztrans])) for node in translated_nodes]

    return translated_nodes


def rigid_motion(truss, Uhis, size, pitch, ytrans, ztrans):
    # print(f'pitch, ytrans, ztrans: {pitch}, {ytrans}, {ztrans}')
    truss['nodes'] = center_on_origin(truss, Uhis)
    truss['nodes'], Uhis = rotate_rigidly(truss, Uhis, checking=False, pitch=pitch)
    truss['nodes'] = translate_bottom_to_origin(truss, Uhis, size, ytrans, ztrans)

    return truss, Uhis


def initial_compression(flat_nodes, panels, size, analyInputOpt, shape='saddle'):
    maxIcr, maxLmd, tol, initial_load_factor, multiply_load = (
        analyInputOpt['maxIcr'], analyInputOpt['maxLmd'], analyInputOpt['tol'],
        analyInputOpt['initialLoadFactor'], analyInputOpt['multiply_load'])

    analyInputOpt['tol'] = 1e-5
    analyInputOpt['multiply_load'] = 1

    nodes_step_1 = initial_rigid_motion(flat_nodes)

    if shape == 'saddle':
        analyInputOpt['maxIcr'] = 50
        analyInputOpt['initialLoadFactor'] = 15
        analyInputOpt['maxLmd'] = 43000

    elif shape == 'dome':
        analyInputOpt['maxIcr'] = 70
        analyInputOpt['initialLoadFactor'] = 50
        analyInputOpt['maxLmd'] = 43000

    elif shape == 'arch':
        analyInputOpt['maxIcr'] = 70
        analyInputOpt['initialLoadFactor'] = 50
        analyInputOpt['maxLmd'] = 50000

    else:
        raise Exception('invalid shape')

    supports_step_1 = miura_supports_step1_v2(size)
    loads_step_1 = miura_loads_step1_v2(size)
    truss, angles, AnalyInputOpt = prepareData(nodes_step_1, panels, supports_step_1, loads_step_1, analyInputOpt)
    Uhis, Fhis = arc_length_improved_with_target_load_factor(truss, angles, AnalyInputOpt)
    nodes2 = create_new_nodes(nodes_step_1, Uhis)

    analyInputOpt['maxIcr'] = maxIcr
    analyInputOpt['maxLmd'] = maxLmd
    analyInputOpt['tol'] = tol
    analyInputOpt['initialLoadFactor'] = initial_load_factor
    analyInputOpt['multiply_load'] = multiply_load

    return nodes2, truss, angles, Uhis, Fhis


def get_bounds(initial_design, size, symmetric, middle_zero, coords_and_rbm):

    if symmetric and middle_zero and coords_and_rbm:
        n_coords = 2 * (size[0] // 2) * (size[1] + 1) + size[1] + 1
        flat_coords_and_rbm = list(initial_design)

        flat_coords = flat_coords_and_rbm[:n_coords]
        pitch, ytrans, ztrans = flat_coords_and_rbm[-3:]

        bounds = []
        coord_max_dev = 0.03  # 0.01 is normal
        pitch_max_dev = 0.3
        ytrans_max_dev = 0.1
        ztrans_max_dev = 0.1

        for flat_coord in flat_coords:
            bounds.append((flat_coord - coord_max_dev, flat_coord + coord_max_dev))

        bounds.append((pitch - pitch_max_dev, pitch + pitch_max_dev))
        bounds.append((ytrans - ytrans_max_dev, ytrans + ytrans_max_dev))
        bounds.append((ztrans - ztrans_max_dev, ztrans + ztrans_max_dev))

    else:
        raise NotImplementedError

    return bounds


def max_pseudo_distance_from_shape(origami_bottom, shape, smoothing=False):

    def pseudo_distance_from_shape(my_node):
        if shape == 'saddle':
            a, b = 0.8, 0.8
            x, y, z = my_node[0], my_node[1], my_node[2]
            result = abs(x**2 / a**2 - y**2 / b**2 - z)
        elif shape == 'dome':
            a, b = 0.9, 0.9
            x, y, z = my_node[0], my_node[1], my_node[2]
            result = abs(-x**2 / a**2 - y**2 / b**2 - z)
        elif shape == 'arch':
            b = 0.6
            x, y, z = my_node[0], my_node[1], my_node[2]
            result = abs(- y**2 / b**2 - z)
        else:
            raise Exception('shape not allowed')
        return result

    if smoothing:
        p = 3
        max_pseudo_distance = sum([pseudo_distance_from_shape(node)**p for node in origami_bottom])**(1/p)

    else:
        max_pseudo_distance = max([pseudo_distance_from_shape(node) for node in origami_bottom])

    return max_pseudo_distance


def create_counter(filename):
    with open(filename, 'w') as file:
        file.write('-1')


def increase_counter(filename):
    with open(filename, 'r') as file:
        for line in file:
            counter = int(line.strip())
    counter += 1
    with open(filename, 'w') as file:
        file.write(f'{counter}')
    return counter


def initial_rigid_motion(nodes):
    # translating center of mass to origin
    center_of_mass = np.average(np.array([list(node) for node in nodes]), 0)
    nodes = [tuple(np.array(node) - center_of_mass) for node in nodes]

    # rotating to align with coordinate system
    inertia_tensor = get_inertia_tensor(nodes)

    principle_moments, principle_axes = np.linalg.eig(inertia_tensor)

    reworked_principle_axes = np.zeros((3, 3))
    for i in range(3):
        ind = np.argmax(np.absolute(principle_axes[:, i]))
        reworked_principle_axes[:, ind] = principle_axes[:, i]
    for i in range(3):
        if reworked_principle_axes[i, i] < 0:
            reworked_principle_axes[:, i] = -1 * reworked_principle_axes[:, i]
    p = reworked_principle_axes.T

    nodes = [tuple(p@np.array(node)) for node in nodes]

    # translating bottom to shape
    selected_nodes = [31, 40, 49]
    selection_final_deformed_nodes = np.array([list(nodes[i]) for i in selected_nodes])
    selection_center_of_mass = np.average(selection_final_deformed_nodes, 0)
    nodes = [tuple(np.array(node) - selection_center_of_mass) for node in nodes]

    return nodes


def objective(nodes, panels, supports, loads, analyInputOpt, shape, config, size, is_itr,
              symmetric, middle_zero, coords_and_rbm, smoothing, design_variables):

    outpath = f'{root_dir}/outputs/{config}'
    counter_all, counter_itr = 0, 0
    counter_all = increase_counter(f'{outpath}/counter-all.txt')
    if is_itr:
        counter_itr = increase_counter(f'{outpath}/counter-itr.txt')

    if symmetric and middle_zero and coords_and_rbm:
        design_variables = list(design_variables)

        n_coords = 2 * (size[0] // 2) * (size[1] + 1) + size[1] + 1
        flat_coords = design_variables[:n_coords]
        flat_nodes = make_nodes_from_coords(flat_coords, size, symmetric, middle_zero)

        # initial compression to make the origami 3D
        nodes2, _, _, _, _ = initial_compression(flat_nodes, panels, size, analyInputOpt, shape=shape)

        pitch, ytrans, ztrans = design_variables[-3:]

    else:
        raise NotImplementedError

    # --- open outputting ---
    truss, angles, analyInputOpt = prepareData(flat_nodes, panels, supports, loads, analyInputOpt)
    nodes, triangles = make_vtk_data(truss, np.zeros((435, 2)))
    make_vtk_file(nodes[-1], triangles, config, filepath=f'{outpath}/all-calcs/{config}-flat-{counter_all}.vtk')
    if is_itr:
        make_vtk_file(nodes[-1], triangles, config, filepath=f'{outpath}/itr-calcs/{config}-flat-{counter_itr}.vtk')
    # --- close outputting ---

    truss, angles, analyInputOpt = prepareData(nodes2, panels, supports, loads, analyInputOpt)

    # --- open outputting ---
    nodes, triangles = make_vtk_data(truss, np.zeros((435, 2)))
    make_vtk_file(nodes[-1], triangles, config, filepath=f'{outpath}/all-calcs/{config}-compressed-{counter_all}.vtk')
    if is_itr:
        make_vtk_file(nodes[-1], triangles, config, filepath=f'{outpath}/itr-calcs/{config}-compressed-{counter_itr}.vtk')
    # --- close outputting ---

    Uhis, Fhis = arc_length_improved_with_target_load_factor(truss, angles, analyInputOpt)

    truss, Uhis = rigid_motion(truss, Uhis, size, pitch, ytrans, ztrans)
    final_deformed_nodes = get_final_deformed_nodes(truss, Uhis)
    bottom_final_deformed_nodes = [list(final_deformed_nodes[i, :]) for i in bottom_nodes(size)]
    max_pseudo_distance = max_pseudo_distance_from_shape(bottom_final_deformed_nodes, shape, smoothing)

    # --- open outputting ---
    nodes, triangles = make_vtk_data(truss, Uhis)
    make_vtk_file(nodes[-1], triangles, config, filepath=f'{outpath}/all-calcs/{config}-folded-{counter_all}.vtk')
    if is_itr:
        make_vtk_file(nodes[-1], triangles, config, filepath=f'{outpath}/itr-calcs/{config}-folded-{counter_itr}.vtk')
        print(get_inertia_tensor(final_deformed_nodes))
    # --- close outputting ---

    # print(f'smooth maximum pseudo distance: {max_pseudo_distance}')
    # print(f'non-smooth maximum pseudo distance: {max_pseudo_distance_from_shape(bottom_final_deformed_nodes, shape,
    #                                                                             smoothing=False)}')

    if is_itr:
        return max_pseudo_distance, max_pseudo_distance_from_shape(bottom_final_deformed_nodes, shape, smoothing=False)
    else:
        return max_pseudo_distance


def create_initial_design(nodes, size, symmetric, middle_zero, coords_and_rbm):
    if symmetric and middle_zero and coords_and_rbm:
        coords = []
        n_unique_nodes = ((size[0] + 1) // 2) * (size[1] + 1)  # does not include middle column

        for node in nodes[:n_unique_nodes]:
            coords.append(node[0])
            coords.append(node[1])

        for node in nodes[n_unique_nodes:n_unique_nodes + size[1] + 1]:  # middle column
            coords.append(node[1])

        rbm = [0.0, 0.0, 0.0]

    else:
        raise NotImplementedError

    initial_design = np.array(coords + rbm)

    return initial_design


def create_results_csv(x0, f0, f0plus, config):
    filename = f'{root_dir}/outputs/{config}/minimize-results.csv'
    with open(filename, 'w') as file:
        my_writer = csv.DictWriter(file, fieldnames=['itr', 'x', 'f'], delimiter=',')
        my_writer.writeheader()
        my_writer.writerow({'itr': 0, 'x': list(x0), 'f': f0})

    filename = f'{root_dir}/outputs/{config}/minimize-results-plus.csv'
    with open(filename, 'w') as f:
        my_writer = csv.DictWriter(f, fieldnames=['itr', 'x', 'f', 'fplus'], delimiter=',')
        my_writer.writeheader()
        my_writer.writerow({'itr': 0, 'x': list(x0), 'f': f0, 'fplus': f0plus})


def add_intermediate_optimization_results(nodes, panels, supports, loads, analyInputOpt, shape, config, size,
                                          symmetric, middle_zero, coords_and_rbm, smoothing, intermediate_result):
    print(f'intermediate result: {intermediate_result}')
    x = intermediate_result
    f, fplus = objective(nodes, panels, supports, loads, analyInputOpt, shape, config, size, True,
                         symmetric, middle_zero, coords_and_rbm, smoothing, x)

    with open(f'{root_dir}/outputs/{config}/counter-itr.txt', 'r') as file:
        for line in file:
            counter_itr = int(line.strip())

    filename = f'{root_dir}/outputs/{config}/minimize-results.csv'
    with open(filename, 'a') as file:
        my_writer = csv.DictWriter(file, fieldnames=['itr', 'x', 'f'], delimiter=',')
        my_writer.writerow({'itr': counter_itr, 'x': list(x), 'f': f})

    filename = f'{root_dir}/outputs/{config}/minimize-results-plus.csv'
    with open(filename, 'a') as file:
        my_writer = csv.DictWriter(file, fieldnames=['itr', 'x', 'f', 'fplus'], delimiter=',')
        my_writer.writerow({'itr': counter_itr, 'x': list(x), 'f': f, 'fplus': fplus})


def total_optimization(shape):
    start_time = time.perf_counter()

    config = f'{shape}-opt2'
    create_output_folder(config)
    create_empty_dir(f'{root_dir}/outputs/{config}/itr-calcs')
    create_empty_dir(f'{root_dir}/outputs/{config}/all-calcs')

    size = (8, 8)  # number of panels in each direction, not number of miura units
    symmetric = True
    middle_zero = True
    coords_and_rbm = True
    smoothing = True
    nodes, panels, supports, loads, relevant_dof, analyInputOpt = read_from_input(config)

    nodes = initial_rigid_motion(nodes)

    x0 = create_initial_design(nodes, size, symmetric, middle_zero, coords_and_rbm)
    my_bounds = get_bounds(x0, size, symmetric, middle_zero, coords_and_rbm)

    def f(*args):
        return objective(nodes, panels, supports, loads, analyInputOpt, shape, config, size, False,
                         symmetric, middle_zero, coords_and_rbm, smoothing, *args)

    def add_intermediate_results(*args):
        return add_intermediate_optimization_results(nodes, panels, supports, loads, analyInputOpt, shape, config, size,
                                                     symmetric, middle_zero, coords_and_rbm, smoothing, *args)

    create_counter(f'{root_dir}/outputs/{config}/counter-all.txt')
    create_counter(f'{root_dir}/outputs/{config}/counter-itr.txt')

    f0, f0_plus = objective(nodes, panels, supports, loads, analyInputOpt, shape, config, size, True,
                            symmetric, middle_zero, coords_and_rbm, smoothing, x0)

    create_results_csv(x0, f0, f0_plus, config)

    final_result = minimize(f, x0, method='BFGS',
                            callback=add_intermediate_results,
                            options={
                                'finite_diff_rel_step': 1e-1,
                                'disp': True, 'return_all': True, 'maxiter': 5})

    # final_result = minimize(f, x0, method='Nelder-Mead',
    #                         callback=add_intermediate_results,
    #                         bounds=my_bounds,
    #                         options={'disp': True, 'maxiter': 100000, 'maxfev': 100000, 'return_all': True,
    #                                  'xatol': 1e-5, 'fatol': 1e-5})

    optimal_design = final_result['x']
    optimal_objective = final_result['fun']

    print(f'optimal objective: {optimal_objective}')
    print(f'optimal design: {optimal_design}')

    end_time = time.perf_counter()
    print(f'optimization time: {round((end_time - start_time)/60, 0)} minutes')

    # truss = get_truss(optimal_nodes, panels)
    # make_vtk_file(truss['nodes'], truss['trigl'], config, 0,
    #               filepath=f'{root_dir}/outputs/{config}/{config}-optimal-8-8.vtk')

    return optimal_design, panels


if __name__ == '__main__':
    total_optimization('saddle')
