import os
import sys
from math import pi
import numpy as np


def pathAnalysis(truss, angles, analyInputOpt):
    original_stdout = sys.stdout
    if not analyInputOpt['verbose']:    # block printing
        sys.stdout = open(os.devnull, 'w')

    tol = 1e-6
    maxIter = 50
    nodes = truss['nodes']

    if 'U0' not in truss.keys():
        truss['U0'] = np.zeros(3 * len(truss['nodes']))
    U = truss['U0']

    maxIcrm = analyInputOpt['maxIcr']
    b_lambda = analyInputOpt['initialLoadFactor']
    Uhis = np.zeros((3*len(nodes), maxIcrm))
    freeDOFs = np.array(list(set(range(3*len(nodes))).difference(set(truss['fixedDOFs']))))
    lmd, icrm = 0, 0
    MUL = np.tile(U, (2, 1)).T
    Fhis = np.zeros(maxIcrm)

    if 'load' in analyInputOpt.keys():
        F = analyInputOpt['load']
    else:
        raise Exception('no load assigned')

    # while icrm < maxIcr and not analyInputOpt.stopCriterion(nodes, U, icrm):  # this is not implemented in MERLIN2
    solve_params = {'dupp1': None, 'sinal': None, 'dupc1': None, 'numgsp': None}   # dupp1, sinal, dupc1, numgsp
    while icrm < maxIcrm:   # we just continue and make a path
        icrm += 1
        itr = 0
        err = 1
        print(f'icrm = {icrm}, lambda = {lmd}')
        while err > tol and itr < maxIter:
            itr += 1
            Fint, K = globalK_fast_ver(U, nodes, truss, angles)   # internal forces, tangential stiffness

            R = lmd*F - Fint  # residual - shape is ndofs x ,
            MRS = np.stack((F, R), 0).T     # shape is ndofs x 2

            MUL[freeDOFs, :] = np.linalg.solve(K[freeDOFs][:, freeDOFs], MRS[freeDOFs, :])

            dUp = MUL[:, 0]     # delta u load
            dUr = MUL[:, 1]     # delta u residual
            if itr == 1:
                dUr = 0*dUr

            dlmd, solve_params = nlsmgd(icrm, itr, dUp, dUr, b_lambda, solve_params)
            dUt = dlmd*dUp + dUr    # delta u
            U = U + dUt
            err = np.linalg.norm(dUt[freeDOFs])
            lmd = lmd + dlmd

            # saved_thing = K
            # saved_thing = saved_thing.flatten('F')
            # with open('/home/niels/Documents/TUDelft/MasterHTE/Thesis/PyN5B8/tests/thing_python.txt', 'w') as f:
            #     for i in range(len(saved_thing)):
            #         f.write(f'{saved_thing[i]}\n')
            print(f'    iter = {itr},   err = {round(err, 10)},    dlambda = {round(dlmd, 10)}')
            if err > 1e8:
                raise Exception('Divergence! Error is too large.')

        if itr > 15:
            b_lambda = b_lambda / 2
            print('Reducing constraint radius...')
            icrm -= 1
            U = Uhis[:, max(icrm, 1) - 1]   # restore displacement
            lmd = Fhis[max(icrm, 1) - 1]    # restore load
        elif itr < 3:
            print('Increase constraint radius...')
            b_lambda = b_lambda * 1.5
            Uhis[:, icrm-1] = U
            Fhis[icrm-1] = lmd
        else:
            Uhis[:, icrm-1] = U
            Fhis[icrm-1] = lmd

    # icrm += 1
    # Uhis
    print('')

    if not analyInputOpt['verbose']:
        sys.stdout.close()      # unblock printing
    sys.stdout = original_stdout

    return Uhis, Fhis


def globalK_edu_ver():
    raise Exception('the globalK_edu_ver function is not implemented')


def globalK_fast_ver(Ui, nodes, truss, angles):
    """the globalK function from MERLIN2"""
    Ui = np.array(Ui)
    truss['bars'] = np.array(truss['bars'])
    truss['lengths'] = np.array(truss['lengths'])
    nnodes = len(nodes)
    nodes_new = []
    for i in range(len(nodes)):
        nodes_new.append([nodes[i][0] + Ui[3 * i], nodes[i][1] + Ui[3 * i + 1], nodes[i][2] + Ui[3 * i + 2]])
    nodes_new = np.array(nodes_new)

    eDOFb = np.kron(truss['bars'], 3*np.ones((1, 3))) + np.tile(np.array([0, 1, 2]), (len(truss['bars']), 2))
    eDOFb = np.array(eDOFb, dtype=int)  # shape is nbars x 6
    du = Ui[eDOFb[:, 0:3]] - Ui[eDOFb[:, 3:6]]  # result has shape n_bars x 3
    # result of Ex has shape n_bars x 1
    Ex = truss['B'] @ Ui / truss['lengths'] + 0.5 * np.sum(np.square(du), 1) / np.square(truss['lengths'])
    Sx, Et, _ = truss['CM'](Ex, truss['C0'])   # the ogden model
    # print(np.expand_dims(Sx, axis=0).T)
    duelem = np.append(du, -du, axis=1)  # shape is nbars x 6

    thing1 = np.reshape(np.tile(np.array(range(len(Et))), (1, 6)), len(Et)*6)  # shape is nbars*6 x 1
    thing2 = eDOFb.flatten('F')      # shape is nbars*6 x 1
    thing3 = duelem.flatten('F')     # shape is nbars*6 x 1
    Du = np.zeros((len(Et), len(Ui)))   # shape is nbars x ndofs
    for i in range(len(thing1)):
            Du[thing1[i], thing2[i]] = thing3[i]
    Fx = Sx * truss['A']     # forces in the bars
    IFb = np.sum(truss['B'] * Fx[:, np.newaxis], 0) + \
        np.sum((Du * (Fx / truss['lengths'])[:, np.newaxis]), 0)

    # nbars x ndofs @ nbars x nbars @ nbars x ndofs
    Kel = truss['B'].T @ np.diag(Et * truss['A'] / truss['lengths']) @ truss['B']

    # ndofs x nbars @ nbars x nbars @ nbars x ndofs
    # ndofs x nbars @ nbars x nbars @ nbars x ndofs
    K1 = Du.T @ np.diag(Et * truss['A'] / np.square(truss['lengths'])) @ truss['B'] + \
        truss['B'].T @ np.diag(Et * truss['A']) / np.square(truss['lengths']) @ Du

    # ndofs x nbars @ nbars x nbars @ nbars x ndofs
    K2 = Du.T @ np.diag(Et * truss['A'] / truss['lengths']**3) @ Du

    thing1 = np.reshape(np.tile(np.array(range(len(Et))), (1, 2)), len(Et) * 2)  # shape is nbars*2 x 1
    thing2 = truss['bars'].flatten('F')     # shape is nbars*2 x 1
    thing3 = np.array([list(np.ones(len(Et))), list(-np.ones(len(Et)))]).flatten('C')  # shape is nbars*2 x 1
    G = np.zeros((len(Et), nnodes))  # shape is nbars x nnodes
    for i in range(len(thing1)):
        G[thing1[i], thing2[i]] += thing3[i]

    # nnodes x nbars @ (nbars x nnodes)
    maurits = G.T @ (G * (Fx / truss['lengths'])[:, np.newaxis])
    ia, ja = np.where(maurits)

    iaja = list(zip(ia, ja))
    iaja_sorted = sorted(iaja, key=lambda x: x[1])
    ia, ja, sa = [], [], []
    for i, ia_and_ja in enumerate(iaja_sorted):
        ia.append(ia_and_ja[0])
        ja.append(ia_and_ja[1])
        sa.append(maurits[ia[i], ja[i]])
    ia = np.reshape(np.array(ia), (len(ia), 1))
    ja = np.reshape(np.array(ja), (len(ja), 1))
    sa = np.reshape(np.array(sa), (len(sa), 1))

    ik = np.tile(3 * ia, (1, 3))
    ik[:, 1] += 1
    ik[:, 2] += 2

    jk = np.tile(3 * ja, (1, 3))
    jk[:, 1] += 1
    jk[:, 2] += 2

    thing3 = np.tile(sa, (1, 3))
    Kg = np.zeros((3*nnodes, 3*nnodes))
    for i in range(thing3.shape[0]):
        for j in range(thing3.shape[1]):
            Kg[ik[i, j], jk[i, j]] += thing3[i, j]
    Kg = 0.5 * (Kg + Kg.T)

    Kb = (Kel + K1 + K2) + Kg   # the stiffness matrix for the bars, shape: ndofs x ndofs

    #################################################################################

    angles['bends'] = np.array(angles['bends'])
    angles['folds'] = np.array(angles['folds'])
    angles['pb0'] = np.array(angles['pb0'])
    angles['pf0'] = np.array(angles['pf0'])

    rotspr = np.concatenate((angles['bends'], angles['folds']), axis=0)    # the rotational springs from bends and folds
    eDOFd = (np.kron(rotspr, 3*np.ones((1, 3))) + np.tile(np.array([0, 1, 2]), (len(rotspr), 4))).T
    eDOFd = np.array(eDOFd, dtype=int)  # shape is 12 x nbends+nfolds

    rkj = (nodes_new[rotspr[:, 1], :] - nodes_new[rotspr[:, 0], :]).T   # shape is 3 x nbends+nfolds
    rij = (nodes_new[rotspr[:, 2], :] - nodes_new[rotspr[:, 0], :]).T   # shape is 3 x nbends+nfolds
    rkl = (nodes_new[rotspr[:, 1], :] - nodes_new[rotspr[:, 3], :]).T   # shape is 3 x nbends+nfolds
    rmj = icross(rij, rkj)   # shape is 3 x nbends+nfolds
    rnk = icross(rkj, rkl)   # shape is 3 x nbends+nfolds

    dt_rnkrij = np.sum(rnk * rij, 0)    # shape is nbends+nfolds x ,

    sgn = np.greater(np.absolute(dt_rnkrij), 1e-8) * np.sign(dt_rnkrij) + \
        np.less_equal(np.absolute(dt_rnkrij), 1e-8) * 1     # shape is nbends+nfolds x

    dt_rmjrnk = np.sum(rmj * rnk, 0)    # shape is nbends+nfolds x

    # these all have shapes nbends+nfolds x ,
    rmj2 = np.sum(rmj**2, 0)
    norm_rmj = np.sqrt(rmj2)
    rkj2 = np.sum(rkj**2, 0)
    norm_rkj = np.sqrt(rkj2)
    rnk2 = np.sum(rnk**2, 0)
    norm_rnk = np.sqrt(rnk2)

    intermediate_thing = list(dt_rmjrnk / (norm_rmj * norm_rnk))
    sgn = list(sgn)
    he = []
    for i, thing in enumerate(intermediate_thing):
        if -1.0000000001 < thing < -0.9999999999:
            to_append = sgn[i] * pi

        elif 0.9999999999 < thing < 1.0000000001:
            to_append = sgn[i] * 0

        elif -0.9999999999 <= thing <= 0.9999999999:
            to_append = sgn[i] * np.arccos(thing)

        else:
            raise Exception('he is not in a valid domain')

        if to_append < 0:
            to_append += 2 * pi
        he.append(to_append)
    he = np.array(he)

    # print(intermediate_thing)
    # intermediate_thing = [-1 if -1 > val > -1.0000000000001 else val for val in intermediate_thing]
    # intermediate_thing = [1 if 1 < val < 1.0000000000001 else val for val in intermediate_thing]
    # intermediate_thing = np.array(intermediate_thing)
    #
    # if -1.0000000001 < intermediate_thing < -0.9999999999:
    #     he = pi
    # elif 0.9999999999 < intermediate_thing < 1.0000000001:
    #     he = 0
    # elif -0.9999999999 <= intermediate_thing <= 0.9999999999:
    #     he = np.arccos(intermediate_thing)
    # else:
    #     raise Exception('he is not in a valid domain')

    # he = sgn * np.real(np.arccos(dt_rmjrnk / (norm_rmj * norm_rnk)))
    # he = sgn * np.real(np.arccos(intermediate_thing))
    # he[np.where(he < 0)] += 2*pi     # shape is nbends+nfolds x ,
    # print(he)

    Rspr = np.zeros(len(he))    # resistant moments
    Kspr = np.zeros(len(he))    # tangent rotational modulus

    # they have size nbends x ,
    Rsprb, Ksprb, _ = angles['CMbend'](he[:len(angles['bends'])].T,
                                       angles['pb0'], angles['Kb'],
                                       truss['lengths'][:len(angles['bends'])])   # this is superLinearBend

    # they have size nfolds x ,
    Rsprf, Ksprf, _ = angles['CMfold'](he[len(angles['bends']):].T,
                                       angles['pf0'], angles['Kf'],
                                       truss['lengths'][len(angles['bends']):
                                                        len(angles['bends'])+len(angles['folds'])],
                                       45, 315)     # this is enhancedLinear

    # print(Ksprb)

    Rspr[:len(angles['bends'])] = Rsprb
    Kspr[:len(angles['bends'])] = Ksprb
    Rspr[len(angles['bends']):] = Rsprf
    Kspr[len(angles['bends']):] = Ksprf

    dt_rijrkj = np.sum(rij * rkj, 0)
    dt_rklrkj = np.sum(rkl * rkj, 0)

    di = rmj * norm_rkj / rmj2
    dl = - rnk * norm_rkj / rnk2
    dj = di * ((dt_rijrkj / rkj2) - 1) - dl * dt_rklrkj / rkj2
    dk = - di * dt_rijrkj / rkj2 + dl * ((dt_rklrkj / rkj2) - 1)
    Jhe_dense = np.concatenate((dj, dk, di, dl), 0)     # shape 12 x nbends+nfolds

    thing1 = eDOFd.flatten('F')     # shape is 12*(nbends+nfolds) x ,
    thing2 = np.tile(np.array(range(len(he))), (12, 1)).flatten('F')  # shape is 12*(nbends+nfolds) x ,
    thing3 = Jhe_dense.flatten('F')     # shape is 12*(nbends+nfolds) x ,
    Jhe = np.zeros((len(Ui), len(he)))  # shape is ndofs x nbends+nfolds
    for i in range(len(thing1)):
        Jhe[thing1[i], thing2[i]] += thing3[i]

    IFbf = np.sum(Jhe * Rspr, 1)

    IF = IFb + IFbf

    ###################################################################

    dii = -(permute(rmj, (0, 2, 1)) * permute(icross(rkj, rmj), (2, 0, 1)) +
            permute(permute(rmj, (0, 2, 1)) * permute(icross(rkj, rmj), (2, 0, 1)), (1, 0, 2))) * \
        permute(norm_rkj / rmj2**2, (2, 0, 1))

    dij = -(permute(rmj, (0, 2, 1)) * permute(rkj, (2, 0, 1))) * permute(1 / (rmj2 * norm_rkj), (2, 0, 1)) + \
           (permute(rmj, (0, 2, 1)) * permute(icross(rkj-rij, rmj), (2, 0, 1)) +
            permute(permute(rmj, (0, 2, 1)) * permute(icross(rkj-rij, rmj), (2, 0, 1)), (1, 0, 2))) * \
        permute(norm_rkj / rmj2**2, (2, 0, 1))

    dik = (permute(rmj, (0, 2, 1)) * permute(rkj, (2, 0, 1))) * permute(1 / (rmj2 * norm_rkj), (2, 0, 1)) + \
          (permute(rmj, (0, 2, 1)) * permute(icross(rij, rmj), (2, 0, 1)) +
           permute(permute(rmj, (0, 2, 1)) * permute(icross(rij, rmj), (2, 0, 1)), (1, 0, 2))) * \
        permute(norm_rkj / rmj2**2, (2, 0, 1))

    dll = (permute(rnk, (0, 2, 1)) * permute(icross(rkj, rnk), (2, 0, 1)) +
           permute(permute(rnk, (0, 2, 1)) * permute(icross(rkj, rnk), (2, 0, 1)), (1, 0, 2))) * \
        permute(norm_rkj / rnk2**2, (2, 0, 1))

    dlk = -(permute(rnk, (0, 2, 1)) * permute(rkj, (2, 0, 1))) * permute(1 / (rnk2 * norm_rkj), (2, 0, 1)) - \
           (permute(rnk, (0, 2, 1)) * permute(icross(rkj - rkl, rnk), (2, 0, 1)) +
            permute(permute(rnk, (0, 2, 1)) * permute(icross(rkj - rkl, rnk), (2, 0, 1)), (1, 0, 2))) * \
        permute(norm_rkj / rnk2 ** 2, (2, 0, 1))

    dlj = (permute(rnk, (0, 2, 1)) * permute(rkj, (2, 0, 1))) * permute(1 / (rnk2 * norm_rkj), (2, 0, 1)) - \
          (permute(rnk, (0, 2, 1)) * permute(icross(rkl, rnk), (2, 0, 1)) +
           permute(permute(rnk, (0, 2, 1)) * permute(icross(rkl, rnk), (2, 0, 1)), (1, 0, 2))) * \
        permute(norm_rkj / rnk2**2, (2, 0, 1))

    dT1jj = (rkj * (2*dt_rijrkj / rkj2 - 1) - rij) * (1 / rkj2)
    dT2jj = (rkj * (2*dt_rklrkj / rkj2) - rkl) * (1 / rkj2)
    djj = permute(di, (0, 2, 1)) * permute(dT1jj, (2, 0, 1)) + \
        dij * permute((dt_rijrkj / rkj2) - 1, (2, 0, 1)) - \
        permute(dl, (0, 2, 1)) * permute(dT2jj, (2, 0, 1)) - \
        dlj * permute(dt_rklrkj / rkj2, (2, 0, 1))

    dT1jk = (rkj * (-2 * dt_rijrkj / rkj2) + rij) * (1 / rkj2)
    dT2jk = (rkj * (-2 * dt_rklrkj / rkj2 + 1) + rkl) * (1 / rkj2)
    djk = permute(di, (0, 2, 1)) * permute(dT1jk, (2, 0, 1)) + \
        dik * permute((dt_rijrkj / rkj2) - 1, (2, 0, 1)) - \
        permute(dl, (0, 2, 1)) * permute(dT2jk, (2, 0, 1)) - \
        dlk * permute(dt_rklrkj / rkj2, (2, 0, 1))

    dT1kk = dT2jk
    dT2kk = dT1jk
    dkk = permute(dl, (0, 2, 1)) * permute(dT1kk, (2, 0, 1)) + \
        dlk * permute(dt_rklrkj / rkj2 - 1, (2, 0, 1)) - \
        permute(di, (0, 2, 1)) * permute(dT2kk, (2, 0, 1)) - \
        dik * permute(dt_rijrkj / rkj2, (2, 0, 1))

    Hp = np.zeros((12, 12, len(he)))
    Hp[0:3, 0:3, :] = djj
    Hp[6:9, 6:9, :] = dii
    Hp[3:6, 3:6, :] = dkk
    Hp[9:12, 9:12, :] = dll
    Hp[0:3, 3:6, :] = djk
    Hp[3:6, 0:3, :] = permute(djk, (1, 0, 2))
    Hp[6:9, 0:3, :] = dij
    Hp[0:3, 6:9, :] = permute(dij, (1, 0, 2))
    Hp[9:12, 0:3, :] = dlj
    Hp[0:3, 9:12, :] = permute(dlj, (1, 0, 2))
    Hp[6:9, 3:6, :] = dik
    Hp[3:6, 6:9, :] = permute(dik, (1, 0, 2))
    Hp[9:12, 3:6, :] = dlk
    Hp[3:6, 9:12, :] = permute(dlk, (1, 0, 2))

    Khe_dense = (permute(Jhe_dense, (0, 2, 1)) * permute(Jhe_dense, (2, 0, 1))) * permute(Kspr, (2, 0, 1)) + \
        Hp * permute(Rspr, (2, 0, 1))   # shape 12 x 12 x nbends+nfolds

    dof_ind1 = permute(np.tile(np.expand_dims(eDOFd, axis=2), (1, 1, 12)), (0, 2, 1))
    dof_ind2 = permute(dof_ind1, (1, 0, 2))

    thing1 = dof_ind1.flatten('F')  # shape is 12*12*(nbends+nfolds) x ,
    thing2 = dof_ind2.flatten('F')  # shape is 12*12*(nbends+nfolds) x ,
    thing3 = Khe_dense.flatten('F')  # shape is 12*12*(nbends+nfolds) x ,
    Kbf = np.zeros((3*nnodes, 3*nnodes))  # shape is ndofs x ndofs, stiffness matrix for bends and folds
    for i in range(len(thing1)):
        Kbf[thing1[i], thing2[i]] += thing3[i]

    K = Kb + Kbf    # bar matrix + bend & fold matrix
    K = (K + K.T)/2

    thing1 = np.arange(0, 3*nnodes)  # shape is ndofs x ,
    thing2 = np.arange(0, 3*nnodes)  # shape is ndofs x ,
    thing3 = 1e-8*np.ones(3*nnodes)  # shape is ndofs x ,
    extra_K = np.zeros((3 * nnodes, 3 * nnodes))  # shape is ndofs x ndofs
    for i in range(len(thing1)):
        extra_K[thing1[i], thing2[i]] += thing3[i]
    K = K + extra_K

    return IF, K


def icross(a, b):
    top = np.reshape(np.multiply(a[1, :], b[2, :]) - np.multiply(b[1, :], a[2, :]), (1, a.shape[1]))
    middle = np.reshape(np.multiply(a[2, :], b[0, :]) - np.multiply(b[2, :], a[0, :]), (1, a.shape[1]))
    bottom = np.reshape(np.multiply(a[0, :], b[1, :]) - np.multiply(b[0, :], a[1, :]), (1, a.shape[1]))
    c = np.concatenate((top, middle, bottom), 0)
    return c


def nlsmgd(step, ite, dup, dur, cmp, my_solve_params):
    dupp1, sinal, dupc1, numgsp = my_solve_params['dupp1'], my_solve_params['sinal'], \
        my_solve_params['dupc1'], my_solve_params['numgsp']

    if ite == 1:    # first iteration
        if step == 1:   # first increment (so the very first step in solving)
            sinal = np.sign(np.dot(dup, dup))   # plus or minus
            dl = cmp
            numgsp = np.dot(dup, dup)
        else:   # not first increment, but still first iteration in an increment
            sinal = sinal * np.sign(np.dot(dupp1, dup))     # plus or minus
            gsp = numgsp / np.dot(dup, dup)
            dl = sinal * cmp * np.sqrt(gsp)
        dupp1 = dup.copy()
        dupc1 = dup.copy()

    else:
        dl = - np.dot(dupc1, dur) / np.dot(dupc1, dup)  # linearized constraint condition
    new_solve_params = {'dupp1': dupp1, 'sinal': sinal, 'dupc1': dupc1, 'numgsp': numgsp}
    return dl, new_solve_params


def permute(arr, perm):
    if arr.ndim == 1:
        expanded_arr = np.expand_dims(arr, axis=0)
        return np.transpose(np.expand_dims(expanded_arr, axis=2), perm)
    if arr.ndim == 2:
        return np.transpose(np.expand_dims(arr, axis=2), perm)
    elif arr.ndim == 3:
        return np.transpose(arr, perm)
    else:
        raise Exception('Something is wrong with the permutation')
