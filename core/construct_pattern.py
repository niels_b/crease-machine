from math import pi, sin, cos, tan, sqrt
import numpy as np


def construct_nodes(size):
    unit = 0.1
    nodes = []
    for j in range(size[1] + 1):
        for i in range(size[0] + 1):
            # nodes.append((i / size[0], j / size[1], 0))
            nodes.append((i*unit, j*unit, 0))
    return nodes


def construct_panels(size):
    """ Making a list of panels with the nodes of the panels ccw"""
    panels = []
    for j in range(size[1]):
        for i in range(size[0]):
            south_west = j * size[0] + i + j
            south_east = j * size[0] + i + j + 1
            north_west = (j + 1) * size[0] + i + j + 1
            north_east = (j + 1) * size[0] + i + j + 2
            panels.append((south_west, south_east, north_east, north_west))
    return panels


def old_miura_pattern(size, theta, a, b, gamma):
    # pattern angle, vert dist, hor dist, fold angle
    theta = theta / 180 * pi
    gamma = gamma / 180 * pi
    numx = size[0]*2
    numy = size[1]*2
    h = a * sin(theta) * sin(gamma)
    s = b * cos(gamma) * tan(theta) / sqrt(1 + cos(gamma)**2 * tan(theta)**2)
    el = a * sqrt(1 - sin(gamma)**2 * sin(theta)**2)
    v = b / sqrt(1 + cos(gamma)**2 * tan(theta)**2)
    X = np.zeros((numx + 1) * (numy + 1))
    Y = np.zeros((numx + 1) * (numy + 1))
    Z = np.zeros((numx + 1) * (numy + 1))

    nodes = []
    for i in range((numx + 1) * (numy + 1)):
        X[i] = i // (numx + 1) * s
        Y[i] = i % (numy + 1) * el
        if (i // (numx + 1)) % 2 == 1:
            Y[i] += v
        if i % (numx + 1) % 2 == 1:
            Z[i] = h
        nodes.append((X[i], Y[i], Z[i]))

    panels = []
    hor_panels, ver_panels = 2*size[1], 2*size[0]
    for j in range(ver_panels):
        for i in range(hor_panels):
            south_west = i * ver_panels + j + i
            south_east = (i + 1) * ver_panels + j + i + 1
            north_west = i * ver_panels + j + i + 1
            north_east = (i + 1) * ver_panels + j + i + 2
            panels.append((south_west, south_east, north_east, north_west))

    return nodes, panels


def miura_pattern(size, theta, a, b, gamma):
    # pattern angle, vert dist, hor dist, fold angle
    theta = theta / 180 * pi
    gamma = gamma / 180 * pi
    numx = size[0] + 1
    numy = size[1] + 1
    h = a * sin(theta) * sin(gamma)
    s = b * cos(gamma) * tan(theta) / sqrt(1 + cos(gamma) ** 2 * tan(theta) ** 2)
    el = a * sqrt(1 - sin(gamma) ** 2 * sin(theta) ** 2)
    v = b / sqrt(1 + cos(gamma) ** 2 * tan(theta) ** 2)

    nodes = []
    for i in range(numx * numy):
        Z = 0.0
        X = i // numy * s
        Y = i % numy * el
        if (i // numy) % 2 == 1:
            Y += v
        if (i % numy) % 2 == 1:
            Z = h
        nodes.append((X, Y, Z))

    panels = []
    hor_panels, ver_panels = size[0], size[1]
    for j in range(ver_panels):
        for i in range(hor_panels):
            south_west = i * ver_panels + j + i
            south_east = (i + 1) * ver_panels + j + i + 1
            north_west = i * ver_panels + j + i + 1
            north_east = (i + 1) * ver_panels + j + i + 2
            panels.append((south_west, south_east, north_east, north_west))

    return nodes, panels


def miura_supports_step1(size):
    cols, rows = size[0], size[1]
    supports = [[0, 1, 1, 1]]

    z_supports = []
    for i in range(cols+1):
        for j in range(rows//2 + 1):
            z_supports.append(i*(rows+1) + 2*j)

    z_supports.pop(0)

    for z_support in z_supports:
        supports.append([z_support, 0, 0, 1])

    return supports


def miura_loads_step1(size):
    cols, rows = size[0], size[1]

    loads = []

    load_nodes = []

    for i in range(cols // 2):
        for j in range(rows // 2):
            load_nodes.append((2*i+1) * (rows + 1) + 2 * j + 1)

    for load_node in load_nodes:
        loads.append([load_node, 0.0, 0.0, 2])

    return loads


def miura_supports_step1_v2(size):
    supports = [
        [40, 1, 1, 1],
    ]

    no_x_movement = [37, 39, 41, 43]

    # no_z_movement = [0, 9, 18, 27, 45, 63, 72,
    #                  2, 11, 20, 29, 47, 56, 65, 74,
    #                  13, 31, 49, 67,
    #                  6, 15, 24, 33, 51, 60, 69, 78,
    #                  8, 17, 26, 35, 53, 62, 71, 80]

    no_z_movement = [0, 9, 18, 27, 45, 54, 63, 72,
                     2, 11, 20, 29, 47, 56, 65, 74,
                     13, 31, 49, 67,
                     6, 15, 24, 33, 51, 60, 69, 78,
                     8, 17, 26, 35, 53, 62, 71, 80]

    no_x_and_z_movement = [36, 38, 40, 42, 44]

    no_y_and_z_movement = [4, 22, 58, 76]

    for node in no_x_movement:
        supports.append([node, 1, 0, 0])

    for node in no_z_movement:
        supports.append([node, 0, 0, 1])

    for node in no_x_and_z_movement:
        supports.append([node, 1, 0, 1])

    for node in no_y_and_z_movement:
        supports.append([node, 0, 1, 1])

    return supports


def miura_loads_step1_v2(size):
    loads = [

    ]

    force_x_down = [49, 58, 67, 76]

    force_x_up = [4, 13, 22, 31]

    force_y_down = [42, 44]

    force_y_down_z_up = [41, 43]

    force_y_up = [36, 38]

    force_y_up_z_up = [37, 39]

    force_x_down_y_down = [51, 60, 69, 78, 53, 62, 71, 80]

    force_x_down_y_down_z_up = [50, 59, 68, 77, 52, 61, 70, 79]

    force_x_up_y_down = [6, 15, 24, 33, 8, 17, 26, 35]

    force_x_up_y_down_z_up = [5, 14, 23, 32, 7, 16, 25, 34]

    force_x_up_y_up = [0, 9, 18, 27, 2, 11, 20, 29]

    force_x_up_y_up_z_up = [1, 10, 19, 28, 3, 12, 21, 30]

    force_x_down_y_up = [45, 54, 63, 72, 47, 56, 65, 74]

    force_x_down_y_up_z_up = [46, 55, 64, 73, 48, 57, 66, 75]

    for node in force_x_down:
        loads.append([node, -1e-5, 0, 0])

    for node in force_x_up:
        loads.append([node, 1e-5, 0, 0])

    for node in force_y_down:
        loads.append([node, 0, -1e-5, 0])

    for node in force_y_down_z_up:
        loads.append([node, 0, -1e-5, 1e-4])

    for node in force_y_up:
        loads.append([node, 0, 1e-5, 0])

    for node in force_y_up_z_up:
        loads.append([node, 0, 1e-5, 1e-4])

    for node in force_x_down_y_down:
        loads.append([node, -1e-5, -1e-5, 0])

    for node in force_x_down_y_down_z_up:
        loads.append([node, -1e-5, -1e-5, 1e-4])

    for node in force_x_up_y_down:
        loads.append([node, 1e-5, -1e-5, 0])

    for node in force_x_up_y_down_z_up:
        loads.append([node, 1e-5, -1e-5, 1e-4])

    for node in force_x_up_y_up:
        loads.append([node, 1e-5, 1e-5, 0])

    for node in force_x_up_y_up_z_up:
        loads.append([node, 1e-5, 1e-5, 1e-4])

    for node in force_x_down_y_up:
        loads.append([node, -1e-5, 1e-5, 0])

    for node in force_x_down_y_up_z_up:
        loads.append([node, -1e-5, 1e-5, 1e-4])

    return loads
