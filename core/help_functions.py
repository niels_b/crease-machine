import numpy as np
import os
import shutil
import json
from PIL import Image
import matplotlib.pyplot as plt
import subprocess
from core.directories import root_dir

background_colour = 255   # dark mode is 32
background_colour_vid = 255   # dark mode is 34


def print_info(myTruss, myAngles, myAnalyInputOpt, myUhis, myFhis):
    print('--- printing analyInputOpt ---')
    for key, value in myAnalyInputOpt.items():
        print(f'{key}:{value}')
    print('\n')

    print('--- printing truss ---')
    for key, value in myTruss.items():
        print(f'{key}:\n')
        print(f'{value}\n')

        # if key == 'B':
        #     for col_index in range(value.shape[1]):
        #         for row_index in range(value.shape[0]):
        #             if abs(value[row_index, col_index]) > 0.001:
        #                 print(f'{row_index + 1, col_index + 1}   {value[row_index, col_index]}')

    print('')

    print('--- printing angles ---')
    for key, value in myAngles.items():
        print(f'{key}:\n')
        print(f'{value}\n')

    print('--- printing Uhis ---')
    print(f'shape: {myUhis.shape}')
    print(f'{myUhis}\n')

    print('--- printing Fhis ---')
    print(f'shape: {myFhis.shape}')
    print(f'{myFhis}\n')


def create_empty_dir(path):
    if os.path.exists(path):
        shutil.rmtree(path)
    os.makedirs(path)


def create_output_folder(config, make_vids=False):
    output_directory = f'{root_dir}/outputs/{config}'
    if os.path.exists(output_directory):
        shutil.rmtree(output_directory)
    os.makedirs(output_directory)
    if make_vids:
        os.makedirs(f'{output_directory}/icrm-load-factor-vid')
        os.makedirs(f'{output_directory}/disp-load-factor-vid')


def make_vtk_data(truss, Uhis):
    # convert from tuples to lists
    n_dof = Uhis.shape[0]
    n_icrm = Uhis.shape[1]

    undeformed_nodes = np.array(truss['nodes'])
    nodes = np.zeros((n_icrm, int(n_dof/3), 3))
    for i in range(n_icrm):
        nodes[i] += undeformed_nodes

    # add displacements
    for icrm in range(n_icrm):
        for dof in range(n_dof):
            node_index = dof//3
            x_y_z = dof % 3
            nodes[icrm][node_index][x_y_z] += Uhis[dof, icrm]

    # triangles are fine
    triangles = truss['trigl']
    return nodes, triangles


def make_vtk_file(nodes, triangles, config, time='', filepath=None):
    if filepath is None:
        filepath = f'{root_dir}/outputs/{config}/{config}-t{time}.vtk'
    with open(filepath, 'w') as f:
        f.write('# vtk DataFile Version 2.0\n')
        f.write(f'{config}_results\n')
        f.write('ASCII\n')
        f.write('DATASET UNSTRUCTURED_GRID\n\n')

        # here starts the points
        f.write(f'POINTS {len(nodes)} float\n')
        for node in nodes:
            f.write(f'{node[0]} {node[1]} {node[2]}\n')
        f.write('\n')

        # here starts the cells
        f.write(f'CELLS {len(triangles)} {4*len(triangles)}\n')
        for tri in triangles:
            f.write(f'3 {tri[0]} {tri[1]} {tri[2]}\n')
        f.write('\n')

        # here starts the cell_types
        f.write(f'CELL_TYPES {len(triangles)}\n')
        for _ in triangles:
            f.write(f'5\n')
        f.write('\n')


def make_vtk_file_supports(nodes, config, time=''):
    with open(f'{root_dir}/outputs/{config}/{config}_supports-t{time}.vtk', 'w') as f:
        f.write('# vtk DataFile Version 2.0\n')
        f.write(f'{config}_results\n')
        f.write('ASCII\n')
        f.write('DATASET UNSTRUCTURED_GRID\n\n')

        # here starts the points
        f.write(f'POINTS {len(nodes)} float\n')
        for node in nodes:
            f.write(f'{node[0]} {node[1]} {node[2]}\n')
        f.write('\n')


def make_vtk_file_loads(nodes, loads, config, time=''):
    with open(f'{root_dir}/outputs/{config}/{config}_loads-t{time}.vtk', 'w') as f:
        f.write('# vtk DataFile Version 2.0\n')
        f.write(f'{config}_results\n')
        f.write('ASCII\n')
        f.write('DATASET UNSTRUCTURED_GRID\n\n')

        # here starts the points
        f.write(f'POINTS {len(nodes)} float\n')
        for node in nodes:
            f.write(f'{node[0]} {node[1]} {node[2]}\n')
        f.write('\n')

    with open(f'{root_dir}/outputs/{config}/{config}_loads-t{time}.txt', 'w') as f:
        f.write('x, y, z, vx, vy, vz\n')
        for i, node in enumerate(nodes):
            f.write(f'{node[0]}, {node[1]}, {node[2]}, {loads[i][1]}, {loads[i][2]}, {loads[i][3]}\n')


def make_vtk_file_relevant_dof(nodes, config, time=''):
    with open(f'{root_dir}/outputs/{config}/{config}_dof-t{time}.vtk', 'w') as f:
        f.write('# vtk DataFile Version 2.0\n')
        f.write(f'{config}_results\n')
        f.write('ASCII\n')
        f.write('DATASET UNSTRUCTURED_GRID\n\n')

        # here starts the points
        f.write(f'POINTS {len(nodes)} float\n')
        for node in nodes:
            f.write(f'{node[0]} {node[1]} {node[2]}\n')
        f.write('\n')


def make_vtk_sequence(nodes, triangles, config, supports, loads, dof):
    for i in range(len(nodes)):
        time = str(i)
        nodes_i = nodes[i]
        make_vtk_file(nodes_i, triangles, config, time)

    support_indices = []
    for support in supports:
        support_indices.append(support[0])
    for i in range(len(nodes)):
        time = str(i)
        nodes_i = nodes[i]
        nodes_i = nodes_i[support_indices, :]
        make_vtk_file_supports(nodes_i, config, time)

    load_indices = []
    for load in loads:
        load_indices.append(load[0])
    for i in range(len(nodes)):
        time = str(i)
        nodes_i = nodes[i]
        nodes_i = nodes_i[load_indices, :]
        make_vtk_file_loads(nodes_i, loads, config, time)

    for i in range(len(nodes)):
        time = str(i)
        nodes_i = nodes[i]
        nodes_i = nodes_i[[dof//3], :]
        make_vtk_file_relevant_dof(nodes_i, config, time)


def set_background(grey):
    if grey < 100:
        plt.style.use('dark_background')
        plt.rcParams['axes.facecolor'] = (grey/255, grey/255, grey/255)
        plt.rcParams['savefig.facecolor'] = (grey/255, grey/255, grey/255)


def plot_icrm_load_factor(config, load_factors, video=True):
    set_background(background_colour)
    icrms = range(len(load_factors))
    load_factors = list(load_factors)
    plt.figure()
    plt.scatter(icrms, load_factors, s=4)
    plt.plot(icrms, load_factors)
    available_divisors = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000]
    a = len(load_factors)
    i = 0
    while a / 10 > available_divisors[i]:
        i += 1
    chosen_divisor = available_divisors[i]
    xticks = [i for i in range(a) if i % chosen_divisor == 0]

    plt.xticks(xticks, xticks)
    plt.xlabel('increment number')
    plt.ylabel('load factor')
    plt.tight_layout()
    plt.savefig(f'{root_dir}/outputs/{config}/icrm-load-factor.pdf')

    yticks = plt.yticks()
    xlim = plt.xlim()
    ylim = plt.ylim()
    plt.close()

    if video:
        set_background(background_colour_vid)
        px = 1/plt.rcParams['figure.dpi']
        for i in range(len(icrms)):
            plt.figure(figsize=(540*px, 540*px))
            plt.xticks(xticks, xticks)
            plt.yticks(yticks[0], yticks[1])
            plt.xlim(xlim)
            plt.ylim(ylim)
            plt.xlabel('increment number')
            plt.ylabel('load factor')
            plt.scatter(icrms[:i+1], load_factors[:i+1], s=4)
            plt.plot(icrms[:i+1], load_factors[:i+1])

            plt.tight_layout()
            plt.savefig(f'{root_dir}/outputs/{config}/icrm-load-factor-vid/frame{i}.png')
            plt.close()

        video_length = 6    # number of seconds
        fps = str(len(icrms)//video_length)     # frames per second, match this with paraview export
        if fps == '0':
            fps = '1'
        width, height = Image.open(f'{root_dir}/outputs/{config}/icrm-load-factor-vid/frame0.png').size
        width_height = f'{width}x{height}'
        command = (f'cd; ffmpeg -hide_banner -loglevel error -r {fps} -f image2 -s {width_height} -i '
                   f'{root_dir}/outputs/{config}/icrm-load-factor-vid/frame%d.png '
                   f'-vcodec libx264 -crf 15 -pix_fmt yuv420p '
                   f'{root_dir}/outputs/{config}/icrm-load-factor-vid/icrm-load-factor-video.mp4')
        subprocess.run(command, shell=True)


def plot_load_factor_displacement(config, load_factors, displacements, dof):
    set_background(background_colour)
    load_factors = list(load_factors)
    displacement = list(displacements[dof, :])
    plt.figure()
    plt.scatter(load_factors, displacement, s=4)
    plt.plot(load_factors, displacement)
    plt.xlabel('load factor')
    plt.ylabel(f'displacement DOF {dof} (m)')
    plt.tight_layout()
    plt.savefig(f'{root_dir}/outputs/{config}/load-factor-displacement.pdf')
    plt.close()


def plot_displacement_load_factor(config, load_factors, displacements, dof, video=True):
    set_background(background_colour)
    load_factors = list(load_factors)
    displacement = list(displacements[dof, :])
    plt.figure()
    plt.scatter(displacement, load_factors, s=4)
    plt.plot(displacement, load_factors)
    xticks = plt.xticks()
    yticks = plt.yticks()
    xlim = plt.xlim()
    ylim = plt.ylim()
    plt.xlabel(f'displacement DOF {dof} (m)')
    plt.ylabel('load factor')
    plt.tight_layout()
    plt.savefig(f'{root_dir}/outputs/{config}/displacement-load-factor.pdf')
    plt.close()

    if video:
        set_background(background_colour_vid)
        px = 1 / plt.rcParams['figure.dpi']
        for i in range(len(displacement)):
            plt.figure(figsize=(540*px, 540*px))
            plt.xticks(xticks[0], xticks[1])
            plt.yticks(yticks[0], yticks[1])
            plt.xlim(xlim)
            plt.ylim(ylim)

            plt.scatter(displacement[:i+1], load_factors[:i+1], s=4)
            plt.plot(displacement[:i+1], load_factors[:i+1])
            plt.xlabel(f'displacement DOF {dof} (m)')
            plt.ylabel('load factor')

            plt.tight_layout()
            plt.savefig(f'{root_dir}/outputs/{config}/disp-load-factor-vid/frame{i}.png')
            plt.close()

        video_length = 6    # number of seconds
        fps = str(len(displacement)//video_length)     # frames per second, match this with paraview export
        if fps == '0':
            fps = '1'
        width, height = Image.open(f'{root_dir}/outputs/{config}/disp-load-factor-vid/frame0.png').size
        width_height = f'{width}x{height}'
        command = (f'cd; ffmpeg -hide_banner -loglevel error -r {fps} -f image2 -s {width_height} -i '
                   f'{root_dir}/outputs/{config}/disp-load-factor-vid/frame%d.png '
                   f'-vcodec libx264 -crf 15 -pix_fmt yuv420p '
                   f'{root_dir}/outputs/{config}/disp-load-factor-vid/disp-load-factor-video.mp4')
        subprocess.run(command, shell=True)


def make_output(config, truss, Uhis, Fhis, dof, supports, loads):
    make_vids = True
    create_output_folder(config, make_vids=make_vids)
    nodes, triangles = make_vtk_data(truss, Uhis)
    make_vtk_sequence(nodes, triangles, config, supports, loads, dof)
    plot_icrm_load_factor(config, Fhis, video=make_vids)
    # plot_load_factor_displacement(config, Fhis, Uhis, dof)
    plot_displacement_load_factor(config, Fhis, Uhis, dof, video=make_vids)


def make_matlab_pattern(config, nodes, panels, supports, loads, analyInputOpt):
    with open(f'matlab_{config}.txt', 'w') as f:
        f.write('%% Define geometry\n\n')

        f.write('Node = [\n')
        for node in nodes:
            f.write(f'    {node[0]}, {node[1]}, {node[2]};\n')
        f.write('    ];\n\n')

        f.write('Panel = {\n')
        for panel in panels:
            f.write(f'    [{panel[0]+1}, {panel[1]+1}, {panel[2]+1}, {panel[3]+1}];\n')
        f.write('    };\n\n')

        f.write('%% Define boundary conditions\n\n')

        f.write('Supp = [\n')
        for support in supports:
            f.write(f'    {support[0]+1}, {support[1]}, {support[2]}, {support[3]};\n')
        f.write('    ];\n\n')

        f.write('Load = [\n')
        for load in loads:
            f.write(f'    {load[0] + 1}, {load[1]}, {load[2]}, {load[3]};\n')
        f.write('    ];\n\n')

        f.write('%% Define material and modelling parameters\n\n')

        f.write('AnalyInputOpt = struct(...\n')
        f.write("    'Modeltype', 'N5B8',...\n")
        f.write("    'MaterCalib', 'auto',...\n")
        f.write(f"    'ModElastic', {analyInputOpt['ModElastic']/1e6},...    % in N/mm^2\n")
        f.write(f"    'Poisson', {analyInputOpt['Poisson']},...\n")
        f.write(f"    'Thickness', {analyInputOpt['Thickness']*1000},...    % in mm\n")
        f.write(f"    'LScaleFactor', {analyInputOpt['LScaleFactor']},...\n")
        f.write(f"    'MaxIcr', {analyInputOpt['maxIcr']},...\n")
        f.write("    'LoadType', 'Force'...\n")
        f.write("    );\n")


def save_results_for_tests(config, Uhis, Fhis):
    saved_thing = Uhis
    saved_thing = saved_thing.flatten('F')
    with open(f'{root_dir}/tests/unittests/{config}_Uhis.txt', 'w') as f:
        for i in range(len(saved_thing)):
            f.write(f'{saved_thing[i]}\n')

    saved_thing = Fhis
    saved_thing = saved_thing.flatten('F')
    with open(f'{root_dir}/tests/unittests/{config}_Fhis.txt', 'w') as f:
        for i in range(len(saved_thing)):
            f.write(f'{saved_thing[i]}\n')


def print_time_info(t0, t1, t2, t3):
    print(f'Time to prepare data: {round(t1 - t0, 3)} s')
    print(f'Time to analyse path: {round(t2-t1, 3)} s')
    print(f'Time to post-process: {round(t3-t2, 3)} s')


def create_new_nodes(nodes, Uhis):
    nnodes = len(nodes)
    final_disp = Uhis[:, -1].flatten('F')

    new_nodes = []
    for i in range(nnodes):
        old_node = nodes[i]
        disp = final_disp[i*3:i*3+3]
        new_nodes.append(tuple([old_node[j]+disp[j] for j in range(3)]))

    return new_nodes


def bottom_nodes(size):
    cols, rows = size[0], size[1]
    node_indices = []
    for i in range(cols+1):
        for j in range(rows//2 + 1):
            node_indices.append(i*(rows+1) + 2*j)
    return node_indices


def top_nodes(size):
    cols, rows = size[0], size[1]
    node_indices = []
    for i in range(cols+1):
        for j in range(rows//2):
            node_indices.append(i*(rows+1) + 2*j + 1)
    return node_indices


def adapt_nodes(nodes, size, offset):
    new_nodes = []

    nodes_to_push_down = bottom_nodes(size)
    nodes_to_push_up = top_nodes(size)

    for i, node in enumerate(nodes):
        if i in nodes_to_push_down:
            new_nodes.append((node[0], node[1], node[2] - offset))
        elif i in nodes_to_push_up:
            new_nodes.append((node[0], node[1], node[2] + offset))
        else:
            new_nodes.append(node)

    return new_nodes


def read_from_input(config):
    with open(f'{root_dir}/inputs/{config}.json') as f:
        data = json.load(f)

    nodes = [tuple(node) for node in data['nodes']]
    panels = [tuple(panel) for panel in data['panels']]
    supports = data['supports']
    loads = data['loads']
    relevant_dof = data['relevant_dof']
    analyInputOpt = {'ZeroBend': 'AsIs',
                     'MaterCalib': 'auto',
                     'verbose': True,
                     'ModElastic': data['ModElastic'],
                     'Poisson': data['Poisson'],
                     'Thickness': data['Thickness'],
                     'maxIcr': data['maxIcr']}

    optional_keys = ['LScaleFactor', 'maxLmd', 'tol', 'initialLoadFactor', 'multiply_load']
    for optional_key in optional_keys:
        if optional_key in data.keys():
            analyInputOpt[optional_key] = data[optional_key]

    if data['verbose'] == 0:
        analyInputOpt['verbose'] = False
    elif data['verbose'] == 1:
        analyInputOpt['verbose'] = True

    return nodes, panels, supports, loads, relevant_dof, analyInputOpt
