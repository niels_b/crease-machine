# crease-machine

***
## Name
crease-machine

## Description
Code to create origami patterns that can fold in a non-rigid way to a target curved surface. A stand-alone component is the N5B8 bar-and-hinge model for modelling origami elasticity.

## Usage
Before you use the code, change the variable `root_dir` in `core/directories.py` to your directory.

Here I explain how to use the bar-and-hinge model, including the arc-length path-following method, to calculate the folding motion of origami. We will use a minimal example with the `bump` configuration.

The first step is to create an input `json`-file. This file with the name `bump.json` is located in the `inputs` directory and contains all information about the origami to perform the equilibrium analysis. The fields in the file are:

- `config`: The name of the origami configuration
- `nodes`: The list of nodes in three-dimensional space
- `panels`: The list of the node indices that make up the facets of the origami. Every facet has four nodes (only quadrilateral facets are allowed in this model).
- `supports`: The list of support boundary conditions. The first entry in a support is the node index it needs to be applied to. The other three entries correspond to the x-, y-, and z-direction displacements of the node. They can be either a 0 or a 1, where a 0 means that that the node is free in that direction, and 1 means that the node is fixed in that direction.
- `loads`: The list of load boundary conditions. It has a similar structure to the supports list. The first entry in a load is the node index it needs to be applied to. The other three entries correspond to the x-, y-, and z-direction loads that are applied. These can be positive or negative.
- `relevant_dof`: The degree of freedom that is used in the automatically generated displacement-load factor graphs.
- `ModElastic`: The material's Young's modulus in Pa, default is 4e9.
- `Poisson`: The material's Poisson's ratio, default is 0.3.
- `thickness`: The thickness of the flat sheet of material in meter, default is 8e-5.
- `LScaleFactor`: Parameter that is used to determine the rotational modulus of folding rotational
	springs. Default is 2.
- `maxIcr`: The maximum number of arc-length increments. The path-following method will stop when this is reached.
- `maxLmd`: The maximum load factor. The path-following method will stop when this is reached.
- `tol`: The tolerance for the error in the arc-length method. When the error is below the tolerance, we start a new increment. Default is 1e-5.
- `initialLoadFactor`: The initial load factor for the arc-length method. Default is 0.01.
- `multiply_load`: A factor by which to multiply the nominal load. If you simply want to impose the load given in the `loads` variable above, use a value of 1.
- `verbose`: When this variable is set to 1, the program will print the status of the arc-length path-following technique as it is executed. When the variable is set to 0, it will print nothing.

With the input file completed, the next step is to run the Python file that will be interpreted by the Python interpreter and perform the mechanical calculation. This file, named `bump.py` for this example, is contained in the `configs` directory. It reads the input from our input file, pre-processes the data, executes the arc-length path following method, and finally post-processes the results. 

The output files are found in the `outputs/bump` directory, which contains a graph of the load factor vs the displacement, a graph of the load factor vs the increment number, videos of these graphs, and `.vtk` files that can be interpreted by `Paraview` to show the origami deformation.

## Support
Email at [nielsbeaufort@proton.me](mailto:nielsbeaufort@proton.me).

## Authors and acknowledgment
This code accompanies the MSc Thesis by Niels Beaufort, which is published here: https://repository.tudelft.nl/record/uuid:374525bb-1ed9-4a05-a967-5aca18371820.
Large parts of this code are adopted from "Ke Liu and G. H. Paulino. Highly efficient nonlinear structural assemblages using the MERLIN2 software 2018". 

## License
GNU AGPLv3  
https://www.gnu.org/licenses/agpl-3.0.en.html  

## Project status
Functional. No development ongoing.
