import numpy as np
from core.directories import root_dir
from core.help_functions import create_output_folder, read_from_input, make_vtk_data, make_vtk_file, create_empty_dir
from core.surface_matching_clean import (initial_rigid_motion, create_initial_design, make_nodes_from_coords,
                                         initial_compression, rigid_motion, rotate_with_matrix, objective, create_counter)
from rigid.match_shape import get_truss
from core.part1_prepare_data import prepareData
from core.part2_path_analysis_sparse import arc_length_improved_with_target_load_factor

config = 'convexity'

create_output_folder(config)
create_empty_dir(f'{root_dir}/outputs/{config}/itr-calcs')
create_empty_dir(f'{root_dir}/outputs/{config}/all-calcs')
create_counter(f'{root_dir}/outputs/{config}/counter-all.txt')
create_counter(f'{root_dir}/outputs/{config}/counter-itr.txt')

size = (8, 8)  # number of panels in each direction, not number of miura units
symmetric = True
middle_zero = True
coords_and_rbm = True
nodes, panels, supports, loads, relevant_dof, analyInputOpt = read_from_input(config)

nodes = initial_rigid_motion(nodes)

x0 = list(create_initial_design(nodes, size, symmetric, middle_zero, coords_and_rbm))
result = objective(nodes, panels, supports, loads, analyInputOpt, 'saddle', config, size, False,
                   symmetric, middle_zero, coords_and_rbm, True, x0)
print(result)

x0[15] += 0.01
result = objective(nodes, panels, supports, loads, analyInputOpt, 'saddle', config, size, False,
                   symmetric, middle_zero, coords_and_rbm, True, x0)
print(result)

x0[15] -= 0.02
result = objective(nodes, panels, supports, loads, analyInputOpt, 'saddle', config, size, False,
                   symmetric, middle_zero, coords_and_rbm, True, x0)
print(result)
