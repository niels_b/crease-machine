from math import sin, cos, pi
import numpy as np
import random
import importlib
from core.part1_prepare_data import prepareData
from core.part2_path_analysis import pathAnalysis
from core.help_functions import make_output

config_to_check = 'curl'
how_many_checks = 10
config = importlib.import_module(f'configs.{config_to_check}')
nodes, panels, supports, loads, analyInputOpt, Uhis, Fhis, relevant_dof = (
    config.nodes, config.panels, config.supports, config.loads, config.analyInputOpt, config.Uhis, config.Fhis,
    config.relevant_dof)
analyInputOpt['verbose'] = False


def transform(vector, my_rots, inverse=False):
    alpha, beta, gamma = my_rots
    Rx = np.array([[1,  0,              0],
                   [0,  cos(alpha),     sin(alpha)],
                   [0,  -sin(alpha),    cos(alpha)]])

    Ry = np.array([[cos(beta),  0,  -sin(beta)],
                   [0,          1,  0],
                   [sin(beta),  0,  cos(beta)]])

    Rz = np.array([[cos(gamma),     sin(gamma), 0],
                   [-sin(gamma),    cos(gamma), 0],
                   [0,              0,          1]])

    R_tot = Rz @ Ry @ Rx

    if inverse:
        R_tot = R_tot.T

    new_vector = R_tot @ vector

    return new_vector


def transform_nodes(old_nodes, my_rots, my_trls, inverse=False):
    new_nodes = []
    for old_node in old_nodes:
        old_node_array = np.array([old_node[0], old_node[1], old_node[2]])
        new_node = transform(old_node_array, my_rots, inverse=inverse)
        new_node = (new_node[0] + my_trls[0], new_node[1] + my_trls[1], new_node[2] + my_trls[2])
        new_nodes.append(new_node)
    return new_nodes


def transform_loads(old_loads, my_rots):
    new_loads = []
    for old_load in old_loads:
        old_load_array = np.array([old_load[1], old_load[2], old_load[3]])
        new_load = transform(old_load_array, my_rots)
        new_load = [old_load[0], new_load[0], new_load[1], new_load[2]]
        new_loads.append(new_load)
    return new_loads


def transform_Uhis(old_Uhis, my_rots):
    n_dof = old_Uhis.shape[0]
    n_icrm = old_Uhis.shape[1]
    n_nodes = int(n_dof / 3)

    new_Uhis = np.zeros((n_dof, n_icrm))

    for icrm in range(n_icrm):
        for node_i in range(n_nodes):
            old_u = np.array([old_Uhis[node_i*3, icrm], old_Uhis[node_i*3+1, icrm], old_Uhis[node_i*3+2, icrm]])
            new_u = transform(old_u, my_rots, inverse=True)
            new_Uhis[node_i*3:(node_i+1)*3, icrm] = new_u

    return new_Uhis


def transform_config(old_nodes, old_panels, old_supports, old_loads, my_analyInputOpt, my_rots, my_trls):
    new_nodes = transform_nodes(old_nodes, my_rots, my_trls)
    new_panels = old_panels
    new_supports = old_supports
    new_loads = transform_loads(old_loads, my_rots)

    # Assemble input data and perform path-following analysis
    my_truss, angles, AnalyInputOpt = prepareData(new_nodes, new_panels, new_supports, new_loads, my_analyInputOpt)
    my_Uhis, my_Fhis = pathAnalysis(my_truss, angles, analyInputOpt)
    new_Uhis = transform_Uhis(my_Uhis, my_rots)
    return new_Uhis, my_Uhis, my_Fhis, my_truss


def compare_Uhis(Uhis0, Uhisx):
    if Uhis0.shape != Uhisx.shape:
        raise Exception('Uhis0 and Uhisx not the same shape')
    nrows, ncols = Uhis0.shape
    flat_Uhis0 = Uhis0.flatten('F')
    flat_Uhisx = Uhisx.flatten('F')

    length = nrows * ncols
    for k in range(length):
        if abs(flat_Uhis0[k]) > 1e-12 or abs(flat_Uhisx[k]) > 1e-12:
            if (abs(flat_Uhisx[k] - flat_Uhis0[k]) > 0.001 or
                    abs((flat_Uhisx[k] - flat_Uhis0[k]) / flat_Uhis0[k]) > 0.001):
                row = k // nrows  # starting at 0
                col = k % nrows     # starting at 0
                print(f'Entries at {(row, col)} are not the same\n'
                      f'Uhis0: {flat_Uhis0[k]}\nUhisx: {flat_Uhisx[k]}')


for i in range(how_many_checks):
    config = f'{config_to_check}_rot{i+1}'
    print(config)
    rots = random.uniform(0, 2*pi), random.uniform(0, 2*pi), random.uniform(0, 2*pi)    # rotations
    trls = random.uniform(0, 1), random.uniform(0, 1), random.uniform(0, 1)     # translations
    Uhis_rot, Uhis_rot_not_backtransformed, Fhis_rot, truss = (
        transform_config(nodes, panels, supports, loads, analyInputOpt, rots, trls))
    make_output(config, truss, Uhis_rot_not_backtransformed, Fhis_rot, relevant_dof, supports, loads)
    compare_Uhis(Uhis, Uhis_rot)
