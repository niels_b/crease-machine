import csv
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from core.directories import root_dir

config = 'min-example'
filename = f'{root_dir}/outputs/{config}/results.csv'


def booth(design_variables):
    x, y = design_variables[0], design_variables[1]
    return (x + 2*y - 7)**2 + (2*x + y - 5)**2


def booth_sensitivities(design_variables):
    x, y = design_variables[0], design_variables[1]
    dbooth_dx = 10*x + 8*y - 34
    dbooth_dy = 8*x + 10*y - 38
    return [dbooth_dx, dbooth_dy]


def create_results_csv(start_x, start_f):
    with open(filename, 'w') as f:
        my_writer = csv.DictWriter(f, fieldnames=['x', 'obj'], delimiter=',')
        my_writer.writeheader()
        my_writer.writerow({'x': list(start_x), 'obj': start_f})


def add_intermediate_results(intermediate_result):
    with open(filename, 'a') as f:
        my_writer = csv.DictWriter(f, fieldnames=['x', 'obj'], delimiter=',')
        my_writer.writerow({'x': list(intermediate_result['x']), 'obj': intermediate_result['fun']})


def read_results():
    my_designs, my_objectives = [], []
    with open(filename, 'r') as f:
        my_reader = csv.DictReader(f)
        for row in my_reader:

            my_design = row['x']
            my_x = my_design.split(' ')[0].strip()
            my_y = my_design.split(' ')[1].strip()
            my_x = ''.join(c for c in my_x if c not in '[] ,')
            my_y = ''.join(c for c in my_y if c not in '[] ,')

            my_designs.append([float(my_x), float(my_y)])
            my_objectives.append(float(row['obj']))
    return my_designs, my_objectives


def create_plot(my_designs):
    grey = 32
    plt.style.use('dark_background')
    plt.rcParams['axes.facecolor'] = (grey/255, grey/255, grey/255)
    plt.rcParams['savefig.facecolor'] = (grey/255, grey/255, grey/255)

    designs_x = [el[0] for el in my_designs]
    designs_y = [el[1] for el in my_designs]

    # generate 2 2d grids for the x & y bounds
    color_y, color_x = np.meshgrid(np.linspace(-10, 10, 1000), np.linspace(-10, 10, 1000))

    color_z = (color_x + 2*color_y - 7)**2 + (2*color_x + color_y - 5)**2
    # x and y are bounds, so z should be the value *inside* those bounds.
    # Therefore, remove the last value from the z array.
    color_z = color_z[:-1, :-1]

    plt.figure()
    plt.title('Booth function')

    c = plt.pcolormesh(color_x, color_y, color_z, cmap='RdBu', norm=matplotlib.colors.LogNorm(), rasterized=True)

    # set the limits of the plot to the limits of the data
    plt.xlim([color_x.min(), color_x.max()])
    plt.ylim([color_y.min(), color_y.max()])
    plt.colorbar(c)

    plt.plot(designs_x, designs_y, color='black', marker='o', linestyle='dashed', linewidth=0.2, markersize=1)

    # loop through each x,y pair
    itr = 0
    for i, j in zip(designs_x, designs_y):
        plt.annotate(str(itr), xy=(i+0.1, j), color='black', fontsize=8)
        itr += 1

    plt.tight_layout()
    plt.savefig(f'{root_dir}/outputs/{config}/heatmap.pdf')
    plt.close()


x0 = np.array([-5.0, -7.5])
f0 = booth(x0)
create_results_csv(x0, f0)

final_design = minimize(booth, x0, jac=booth_sensitivities, method='BFGS', callback=add_intermediate_results)

designs, objectives = read_results()
create_plot(designs)
