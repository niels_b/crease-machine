import numpy as np

from rigid.match_shape import saddle_creator
from rigid.make_developable import (objective, objective_sens, constraints, constraints_sens,
                                    list_of_pattern_and_cross_edges, list_of_interior_vertices,
                                    list_of_clockwise_panels, list_of_fixed_and_free_nodes,
                                    create_initial_design, initial_edge_lengths,
                                    concave_quadrilateral_constraint, concave_quadrilateral_constraint_sens,
                                    concave_quadrilateral_constraint_initial_signs,
                                    concave_quadrilateral_constraint_2, concave_quadrilateral_constraint_sens_2)

nodes, panels, miura_units_x, miura_units_y = saddle_creator()
edges = list_of_pattern_and_cross_edges(miura_units_x, miura_units_y)
interior_nodes, neighbours_clockwise = list_of_interior_vertices(nodes, miura_units_x, miura_units_y)
clockwise_panels = list_of_clockwise_panels(panels)
fixed_nodes, free_nodes = list_of_fixed_and_free_nodes(nodes, miura_units_x, miura_units_y)
x0 = create_initial_design(nodes, free_nodes)
initial_lengths = initial_edge_lengths(nodes, edges)
initial_concave_signs = concave_quadrilateral_constraint_initial_signs(nodes, panels)

random_noise = np.random.rand(*x0.shape)
# x0 = x0 + random_noise


def f(*args):
    return objective(nodes, free_nodes, edges, initial_lengths, *args)


def df_dx(*args):
    return objective_sens(nodes, free_nodes, edges, initial_lengths, *args)


def g(*args):
    return np.array(constraints(nodes, free_nodes, interior_nodes, neighbours_clockwise, clockwise_panels, *args))


def dg_dx(*args):
    return constraints_sens(nodes, free_nodes, interior_nodes, neighbours_clockwise, clockwise_panels, *args)


def concave(*args):
    return concave_quadrilateral_constraint(nodes, free_nodes, panels, initial_concave_signs, *args)


def concave_dx(*args):
    return concave_quadrilateral_constraint_sens(nodes, free_nodes, panels, initial_concave_signs, *args)


f0 = f(x0)
df0_dx = df_dx(x0)
g0 = g(x0)
dg0_dx = dg_dx(x0)
concave0 = concave(x0)
dconcave0_dx = concave_dx(x0)


jac_f = np.zeros(len(free_nodes) * 3)
jac_g = np.zeros((len(interior_nodes) + len(clockwise_panels), len(free_nodes) * 3))
jac_concave = np.zeros((len(panels) * 4, len(free_nodes) * 3))

for i in range(len(free_nodes)*3):
    deviation = 1e-6
    deviation_left, deviation_right = np.zeros(len(free_nodes)*3), np.zeros(len(free_nodes)*3)
    deviation_left[i] -= deviation
    deviation_right[i] += deviation
    x_left = x0 + deviation_left
    x_right = x0 + deviation_right
    df_dxi = (f(x_right) - f(x_left)) / (2 * deviation)
    dg_dxi = (g(x_right) - g(x_left)) / (2 * deviation)
    dconcave_dxi = (concave(x_right) - concave(x_left)) / (2 * deviation)
    jac_f[i] = df_dxi
    jac_g[:, i] = dg_dxi
    jac_concave[:, i] = dconcave_dxi


# print(f'finite differences - objective\n{jac_f}\n')
# print(f'analytical - objective\n{df0_dx}\n')
# print(f'finite differences - developability constraint\n{jac_g[:len(interior_nodes), :]}\n')
# print(f'analytical - developability constraint\n{dg0_dx[:len(interior_nodes), :]}\n')
# print(f'finite differences - planarity constraint\n{jac_g[len(interior_nodes):, :]}\n')
# print(f'analytical - planarity constraint\n{dg0_dx[len(interior_nodes):, :]}\n')
print(f'finite differences - concave quadrilaterals constraint\n{jac_concave}\n')
print(f'analytical - concave quadrilaterals constraint\n{dconcave0_dx}\n')

print(np.allclose(df0_dx, jac_f))
print(np.allclose(dg0_dx, jac_g))
print(np.allclose(dconcave0_dx, jac_concave))
