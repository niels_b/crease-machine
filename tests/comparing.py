is_matrix = False
nrows, ncols = 39, 39

thing_python = []
with open('thing_python.txt', 'r') as f:
    for line in f:
        thing_python.append(float(line.strip()))

thing_matlab = []
with open('thing_matlab.txt', 'r') as f:
    for line in f:
        thing_matlab.append(float(line.strip()))


if len(thing_python) != len(thing_matlab):
    raise Exception('the arrays are not the same size')

if is_matrix:
    total = nrows * ncols
    print(f'Total number of entries in the matrix: {total}')
else:
    total = len(thing_python)
    print(f'Total number of entries in thing: {total}')


for i in range(total):
    if abs(thing_python[i]) > 1e-12 or abs(thing_matlab[i]) > 1e-12:
        if (abs(thing_python[i] - thing_matlab[i]) > 0.001 or
                abs((thing_python[i] - thing_matlab[i]) / thing_matlab[i]) > 0.001):
            if is_matrix:
                row = i // nrows  # starting at 0
                col = i % nrows     # starting at 0
                print(f'Entries at {(row, col)} are not the same\n'
                      f'python: {thing_python[i]}\nmatlab: {thing_matlab[i]}')
            else:
                print(f'Entries at {i} are not the same\npython: {thing_python[i]}\nmatlab: {thing_matlab[i]}')


# ------------- for creating a file -----------

# in python
#
# saved_thing = thing
# saved_thing = saved_thing.flatten('F')
# with open('/home/niels/Documents/TUDelft/MasterHTE/Thesis/PyN5B8/tests/thing_python.txt', 'w') as f:
#     for i in range(len(saved_thing)):
#         f.write(f'{saved_thing[i]}\n')

# in matlab
#
# saved_thing = full(thing);
# saved_thing = reshape(saved_thing.',1,[]);
# fileID = fopen('thing_matlab.txt', 'w');
# for num = 1:length(saved_thing)
#     fprintf(fileID, '%18.14f\n%', saved_thing(num));
# end
# fclose(fileID);
