import csv
import numpy as np
from scipy.optimize import minimize, NonlinearConstraint
from core.directories import root_dir
from core.help_functions import create_output_folder
import matplotlib
import matplotlib.pyplot as plt

config = 'booth'


def create_initial_design():
    x0 = np.array([-5, -7.5])
    return x0


def objective(x):
    print(f'calculating objective with {x}')
    x1, x2 = x
    obj = (x1 + 2*x2 - 7)**2 + (2*x1 + x2 - 5)**2  # booth
    return obj


def create_results_csv(x0, f0):

    filename = f'{root_dir}/outputs/{config}/minimize_results.csv'
    with open(filename, 'w') as file:
        my_writer = csv.DictWriter(file, fieldnames=['x', 'f'], delimiter=',')
        my_writer.writeheader()
        # I think with some methods, the result of the first iteration is x0, f0
        my_writer.writerow({'x': list(x0), 'f': f0})


def add_intermediate_optimization_results(intermediate_result):
    filename = f'{root_dir}/outputs/{config}/minimize_results.csv'
    x = intermediate_result
    f = objective(x)

    with open(filename, 'r') as file:
        temp_reader = csv.reader(file, delimiter=',')
        iteration = sum(1 for _ in temp_reader) - 1

    with open(filename, 'a') as file:
        my_writer = csv.DictWriter(file, fieldnames=['x', 'f'], delimiter=',')
        my_writer.writerow({'x': list(x), 'f': f})


"""only for 2D"""


def read_results():
    my_designs, my_objectives = [], []
    with open(f'{root_dir}/outputs/{config}/minimize_results.csv', 'r') as f:
        my_reader = csv.DictReader(f)
        for row in my_reader:

            my_design = row['x']
            my_x1 = my_design.split(' ')[0].strip()
            my_x2 = my_design.split(' ')[1].strip()
            my_x1 = ''.join(c for c in my_x1 if c not in '[] ,')
            my_x2 = ''.join(c for c in my_x2 if c not in '[] ,')

            my_designs.append([float(my_x1), float(my_x2)])
            my_objectives.append(float(row['f']))
    return my_designs, my_objectives


def create_plot(my_designs):
    grey = 32
    plt.style.use('dark_background')
    plt.rcParams['axes.facecolor'] = (grey/255, grey/255, grey/255)
    plt.rcParams['savefig.facecolor'] = (grey/255, grey/255, grey/255)

    designs_x1 = [el[0] for el in my_designs]
    designs_x2 = [el[1] for el in my_designs]

    # generate 2 2d grids for the x1 & x2 bounds
    color_x2, color_x1 = np.meshgrid(np.linspace(-10, 10, 1000), np.linspace(-10, 10, 1000))

    color_f = (color_x1 + 2*color_x2 - 7)**2 + (2*color_x1 + color_x2 - 5)**2
    # x1 and x2 are bounds, so z should be the value *inside* those bounds.
    # Therefore, remove the last value from the z array.
    color_f = color_f[:-1, :-1]

    plt.figure()
    plt.title('Booth function')

    c = plt.pcolormesh(color_x1, color_x2, color_f, cmap='RdBu', norm=matplotlib.colors.LogNorm(), rasterized=True)

    # set the limits of the plot to the limits of the data
    plt.xlim([color_x1.min(), color_x1.max()])
    plt.ylim([color_x2.min(), color_x2.max()])
    plt.colorbar(c)

    plt.plot(designs_x1, designs_x2, color='black', marker='o', linestyle='dashed', linewidth=0.2, markersize=1)

    # loop through each x,y pair
    itr = 0
    for i, j in zip(designs_x1, designs_x2):
        plt.annotate(str(itr), xy=(i+0.1, j), color='black', fontsize=8)
        itr += 1

    plt.tight_layout()
    plt.savefig(f'{root_dir}/outputs/{config}/heatmap.pdf')
    plt.close()


"""end of only for 2D"""


def total_optimization():
    create_output_folder(config)

    x0 = create_initial_design()

    def f(*args):
        return objective(*args)

    def add_intermediate_results(*args):
        return add_intermediate_optimization_results(*args)

    f0 = f(x0)

    create_results_csv(x0, f0)

    result = minimize(f, x0, method='CG',
                      callback=add_intermediate_results,
                      options={'disp': True, 'return_all': True, 'maxiter': 50})

    optimal_design = result['x']
    optimal_objective = result['fun']

    print(f'optimal design: {optimal_design}')
    print(f'optimal objective: {optimal_objective}')

    """only for 2D"""
    designs, objectives = read_results()
    create_plot(designs)
    """end of only for 2D"""

    return result


if __name__ == '__main__':
    total_optimization()
