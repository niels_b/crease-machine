import unittest
from core.directories import root_dir
from core.construct_pattern import construct_nodes, construct_panels, miura_pattern
from core.part1_prepare_data import prepareData
from core.part2_path_analysis_sparse import arc_length

main_path = root_dir + '/tests/unittests/'


class TestConfigs(unittest.TestCase):

    def core_test(self, config, Uhis, Fhis):
        flat_Uhis = Uhis.flatten('F')
        with open(f'{main_path+config}_Uhis.txt') as f:
            for i, line in enumerate(f):
                with self.subTest(i=i, line=line):
                    self.assertAlmostEqual(float(line.strip()), flat_Uhis[i])

        with open(f'{main_path+config}_Fhis.txt') as f:
            for i, line in enumerate(f):
                with self.subTest(i=i, line=line):
                    self.assertAlmostEqual(float(line.strip()), Fhis[i])

    def test_curl(self):
        config = 'curl'
        size = (2, 3)  # number of facets in horizontal and vertical direction

        nodes = construct_nodes(size)  # list of tuples of floats
        panels = construct_panels(size)  # list of tuples of ints, counter-clockwise

        supports = [[0, 1, 1, 1],
                    [1, 1, 1, 1],
                    [2, 1, 1, 1],
                    [3, 1, 1, 1],
                    [4, 1, 1, 1],
                    [5, 1, 1, 1]]

        loads = [[9, 0, -1e-2, 2e-2],
                 [10, 0, -1e-2, 2e-2],
                 [11, 0, -1e-2, 2e-2]]

        analyInputOpt = {'ZeroBend': 'AsIs',
                         'ModElastic': 4e9,  # pascal
                         'Poisson': 0.3,  # no unit
                         'Thickness': 0.08e-3,  # meters
                         'LScaleFactor': 2,  # no units, higher scale factor is lower fold stiffness
                         'maxIcr': 20,  # steps during arc-length
                         'MaterCalib': 'auto',
                         'verbose': False
                         }

        # Assemble input data and perform path-following analysis
        truss, angles, AnalyInputOpt = prepareData(nodes, panels, supports, loads, analyInputOpt)
        Uhis, Fhis = arc_length(truss, angles, analyInputOpt)

        self.core_test(config, Uhis, Fhis)

    def test_bump(self):
        config = 'bump'

        nodes = [
            (0, 0, 0),
            (0.1, 0, 0),
            (0.1 * (1 + 2 ** (-1 / 2)), 0, 0.1 * 2 ** (-1 / 2)),
            (0.1 * (1 + 2 ** (1 / 2)), 0, 0),
            (0.1 * (2 + 2 ** (1 / 2)), 0, 0),
            (0.1 * (2 + 2 ** (1 / 2)), 0.1, 0),
            (0.1 * (1 + 2 ** (1 / 2)), 0.1, 0),
            (0.1 * (1 + 2 ** (-1 / 2)), 0.1, 0.1 * 2 ** (-1 / 2)),
            (0.1, 0.1, 0),
            (0, 0.1, 0),
            (-0.01, 0.1, 0),
            (-0.01, 0, 0)
        ]

        panels = [
            (0, 1, 8, 9),
            (1, 2, 7, 8),
            (2, 3, 6, 7),
            (3, 4, 5, 6),
            (0, 9, 10, 11)
        ]

        supports = [
            [0, 1, 1, 1],
            [9, 1, 1, 1],
            [10, 1, 1, 1],
            [11, 1, 1, 1]
        ]

        loads = [
            [4, 1e-2, 0, 0],
            [5, 1e-2, 0, 0]
        ]

        analyInputOpt = {'ZeroBend': 'AsIs',
                         'ModElastic': 4e9,         # pascal
                         'Poisson': 0.3,            # no unit
                         'Thickness': 0.08e-3,      # meters
                         'LScaleFactor': 2,         # no units
                         'maxIcr': 30,              # steps during arc-length
                         'MaterCalib': 'auto',
                         'verbose': False           # print icrm, error, lambda
                         }

        # Assemble input data and perform path-following analysis
        truss, angles, AnalyInputOpt = prepareData(nodes, panels, supports, loads, analyInputOpt)
        Uhis, Fhis = arc_length(truss, angles, analyInputOpt)

        self.core_test(config, Uhis, Fhis)

    def test_single_miura(self):
        config = 'single_miura'
        size = (2, 2)  # number of units in horizontal and vertical direction

        nodes, panels = miura_pattern(size, 45, 0.2, 0.21, 70)  # pattern angle, vert dist, hor dist, fold angle

        supports = [[1, 1, 1, 1],
                    [2, 1, 1, 1],
                    [4, 1, 1, 1],
                    [5, 1, 1, 1]]

        loads = [[6, 1e-2, 0, 0],
                 [7, 1e-2, 0, 0],
                 [8, 1e-2, 0, 0]]

        analyInputOpt = {'ZeroBend': 'AsIs',
                         'ModElastic': 4e9,  # pascal
                         'Poisson': 0.3,  # no unit
                         'Thickness': 0.08e-3,  # meters
                         'LScaleFactor': 2,  # no units, higher scale factor is lower fold stiffness
                         'maxIcr': 20,  # steps during arc-length
                         'MaterCalib': 'auto',
                         'verbose': False  # print icrm, error, lambda
                         }

        # Assemble input data and perform path-following analysis
        truss, angles, AnalyInputOpt = prepareData(nodes, panels, supports, loads, analyInputOpt)
        Uhis, Fhis = arc_length(truss, angles, analyInputOpt)

        self.core_test(config, Uhis, Fhis)


if __name__ == '__main__':
    unittest.main()
