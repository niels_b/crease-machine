import numpy as np
from core.part1_prepare_data import prepareData
from core.part2_path_analysis_sparse import arc_length_improved_with_target_load_factor
from core.help_functions import print_info, make_output, make_matlab_pattern, save_results_for_tests, read_from_input
from configs.saddle_miura_compression import nodes

np.set_printoptions(edgeitems=30, linewidth=100000)

config = 'saddle-from-compressed'
_, panels, supports, loads, relevant_dof, analyInputOpt = read_from_input(config)

# Assemble input data and perform path-following analysis
truss, angles, AnalyInputOpt = prepareData(nodes, panels, supports, loads, analyInputOpt)
Uhis, Fhis = (arc_length_improved_with_target_load_factor(truss, angles, analyInputOpt))

# Print the info and make output files
# print_info(truss, angles, analyInputOpt, Uhis, Fhis)
make_output(config, truss, Uhis, Fhis, relevant_dof, supports, loads)
# make_matlab_pattern(config, nodes, panels, supports, loads, analyInputOpt)
