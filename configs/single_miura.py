import numpy as np
from core.construct_pattern import miura_pattern
from core.part1_prepare_data import prepareData
from core.part2_path_analysis_sparse import arc_length
from core.help_functions import print_info, make_output, read_from_input

np.set_printoptions(edgeitems=30, linewidth=100000)

config = 'single_miura'
_, _, supports, loads, relevant_dof, analyInputOpt = read_from_input(config)

size = (2, 2)    # number of units in horizontal and vertical direction
nodes, panels = miura_pattern(size, 45, 0.2, 0.21, 70)   # pattern angle, vert dist, hor dist, fold angle

# Assemble input data and perform path-following analysis
truss, angles, AnalyInputOpt = prepareData(nodes, panels, supports, loads, analyInputOpt)
Uhis, Fhis = arc_length(truss, angles, analyInputOpt)

# Print the info and make output files
# print_info(truss, angles, analyInputOpt, Uhis, Fhis)
make_output(config, truss, Uhis, Fhis, relevant_dof, supports, loads)
