import numpy as np
from core.construct_pattern import miura_pattern
from core.part1_prepare_data import prepareData
from core.part2_path_analysis_sparse import arc_length_improved_with_target_load_factor
from core.help_functions import (print_info, make_output, make_matlab_pattern, print_time_info,
                                 read_from_input)
import time

t0 = time.perf_counter()
np.set_printoptions(edgeitems=30, linewidth=100000)

config = 'large-miura'
_, _, supports, loads, relevant_dof, analyInputOpt = read_from_input(config)

size = (5, 6)    # number of panels in horizontal and vertical direction
nodes, panels = miura_pattern(size, 80, 0.10, 0.11, 88)

# Assemble input data and perform path-following analysis
truss, angles, AnalyInputOpt = prepareData(nodes, panels, supports, loads, analyInputOpt)
t1 = time.perf_counter()
Uhis, Fhis = arc_length_improved_with_target_load_factor(truss, angles, analyInputOpt)

t2 = time.perf_counter()

# Print the info and make output files
# print_info(truss, angles, analyInputOpt, Uhis, Fhis)
make_output(config, truss, Uhis, Fhis, relevant_dof, supports, loads)
# make_matlab_pattern(config, nodes, panels, supports, loads, analyInputOpt)
t3 = time.perf_counter()

print_time_info(t0, t1, t2, t3)
