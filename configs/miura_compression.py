import numpy as np
from core.help_functions import print_info, make_output, read_from_input
from core.surface_matching import (initial_rigid_motion, make_nodes_from_coords, initial_compression,
                                   miura_supports_step1_v2, miura_loads_step1_v2)

np.set_printoptions(edgeitems=30, linewidth=100000)

config = 'miura-compression'
size = (8, 8)  # number of panels in each direction, not number of miura units

nodes, panels, supports, loads, relevant_dof, analyInputOpt = read_from_input(config)
nodes = initial_rigid_motion(nodes)
coords = []
n_unique_nodes = ((size[0] + 1) // 2) * (size[1] + 1)  # does not include middle column

for node in nodes[:n_unique_nodes]:
    coords.append(node[0])
    coords.append(node[1])

for node in nodes[n_unique_nodes:n_unique_nodes + size[1] + 1]:  # middle column
    coords.append(node[1])

supports = miura_supports_step1_v2(size)
loads = miura_loads_step1_v2(size)
nodes = make_nodes_from_coords(coords, size, True, True)
nodes, truss, angles, Uhis, Fhis = initial_compression(nodes, panels, size, analyInputOpt)

# Print the info and make output files
# print_info(truss, angles, analyInputOpt, Uhis, Fhis)
make_output(config, truss, Uhis, Fhis, relevant_dof, supports, loads)
# make_matlab_pattern(config, nodes, panels, supports, loads, analyInputOpt)
