// Mesh size
ms = 0.005;

// Points
Point(1) = {-0.25980762, -0.15, 0, ms};
Point(2) = {0.34641016, 0.2, 0, ms};
Point(3) = {-0.1, 0.17320508, 0, ms};

// Lines
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 1};

// Loops
Curve Loop(1) = {1, 2, 3};

// Plane surface
Plane Surface(1) = {1};

// Physical surface
Physical Surface(1) = {1};

