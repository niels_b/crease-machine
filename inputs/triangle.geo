// Mesh size
ms = 0.001;

// Points
Point(1) = {0.25780784, 0.21011094, 0.14105236, ms};
Point(2) = {0.29636127, 0.22319726, 0.05557488, ms};
Point(3) = {0.24990295, 0.25607910, 0.05060971, ms};

// Lines
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 1};

// Loops
Curve Loop(1) = {1, 2, 3};

// Plane surface
Plane Surface(1) = {1};

// Physical surface
Physical Surface(1) = {1};

