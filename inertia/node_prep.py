import numpy as np
from math import sin, cos, pi

n0 = np.array([-0.3, 0, 0])
n1 = np.array([0.4, 0, 0])
n2 = np.array([0, 0.2, 0])

th = 30/180*pi

R = np.array([
    [cos(th), -sin(th), 0],
    [sin(th), cos(th), 0],
    [0, 0, 1]
])

n0_rot = R @ n0
n1_rot = R @ n1
n2_rot = R @ n2

for node in [n0_rot, n1_rot, n2_rot]:
    print(node)
