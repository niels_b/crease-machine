import numpy as np
from math import cos, sin, pi
from core.help_functions import read_from_input
from core.surface_matching_clean import get_final_deformed_nodes


def get_inertia_tensor(nodes):
    Ixx = sum(node[1] ** 2 + node[2] ** 2 for node in nodes)
    Iyy = sum(node[0] ** 2 + node[2] ** 2 for node in nodes)
    Izz = sum(node[0] ** 2 + node[1] ** 2 for node in nodes)
    Ixy = -sum(node[0] * node[1] for node in nodes)
    Ixz = -sum(node[0] * node[2] for node in nodes)
    Iyz = -sum(node[1] * node[2] for node in nodes)

    inertia_tensor = np.array([[Ixx, Ixy, Ixz],
                               [Ixy, Iyy, Iyz],
                               [Ixz, Iyz, Izz]])
    return inertia_tensor


def print_principal_inertia(nodes):
    inertia_tensor = get_inertia_tensor(nodes)
    principle_moments, principle_axes = np.linalg.eig(inertia_tensor)
    print(principle_moments)
    print(principle_axes)


def rotate_with_matrix(truss, Uhis, inertia_tensor, checking=False, pitch=0):
    principle_moments, principle_axes = np.linalg.eig(inertia_tensor)

    reworked_principle_axes = np.zeros((3, 3))
    for i in range(3):
        ind = np.argmax(np.absolute(principle_axes[:, i]))
        reworked_principle_axes[:, ind] = principle_axes[:, i]
    for i in range(3):
        if reworked_principle_axes[i, i] < 0:
            reworked_principle_axes[:, i] = -1 * reworked_principle_axes[:, i]

    R = reworked_principle_axes.T  # actually np.eye(3) @ principle_axes.T

    extra_pitch = np.array([[1, 0,          0],
                            [0, cos(pitch), -sin(pitch)],
                            [0, sin(pitch), cos(pitch)]])
    R = extra_pitch @ R

    nodes = np.array([[node[0], node[1], node[2]] for node in truss['nodes']])

    rotated_nodes = (R @ nodes.T).T
    rotated_nodes = [(rotated_nodes[i, 0], rotated_nodes[i, 1], rotated_nodes[i, 2])
                     for i in range(rotated_nodes.shape[0])]

    rotated_Uhis = np.zeros(Uhis.shape)
    nnodes = Uhis.shape[0] // 3
    for i in range(Uhis.shape[1]):
        Uhis_i = Uhis[:, i]
        Uhis_i_reshaped = np.zeros((3, nnodes))
        for j in range(Uhis_i.shape[0]):
            Uhis_i_reshaped[j % 3, j // 3] = Uhis_i[j]

        Uhis_i_rotated = (R @ Uhis_i_reshaped).T        # nnodes x 3
        Uhis_i_rotated_reshaped = np.zeros((3*nnodes, 1))   # ndofs x 1
        for j in range(Uhis_i_rotated_reshaped.shape[0]):
            Uhis_i_rotated_reshaped[j] = Uhis_i_rotated[j // 3, j % 3]

        rotated_Uhis[:, i] = Uhis_i_rotated_reshaped[:, 0]

    if checking:
        truss['nodes'] = rotated_nodes
        final_deformed_nodes = get_final_deformed_nodes(truss, rotated_Uhis)
        print_principal_inertia(final_deformed_nodes)

    return rotated_nodes, rotated_Uhis


def rotate_nodes(nodes):
    axis_of_rotation = [0.4, 0.6, 0.6928]

    ax, ay, az = axis_of_rotation[0], axis_of_rotation[1], axis_of_rotation[2]
    th = 38 / 180 * pi
    # print(ax, ay, az, th*180/pi)

    R = np.array([[cos(th) + ax ** 2 * (1 - cos(th)), ax * ay * (1 - cos(th)) - az * sin(th),
                   ax * az * (1 - cos(th)) + ay * sin(th)],
                  [ay * ax * (1 - cos(th)) + az * sin(th), cos(th) + ay ** 2 * (1 - cos(th)),
                   ay * az * (1 - cos(th)) - ax * sin(th)],
                  [az * ax * (1 - cos(th)) - ay * sin(th), az * ay * (1 - cos(th)) + ax * sin(th),
                   cos(th) + az ** 2 * (1 - cos(th))]])

    nodes_rot = [tuple(R @ np.array(node)) for node in nodes]
    return nodes_rot


def get_reworked_eigenvectors(eigenvalues, eigenvectors):
    order = []
    eigenvalues = list(eigenvalues)
    for i in range(3):
        ind = np.argmin(np.absolute(eigenvalues))
        order.append(ind)
        eigenvalues[ind] = 1000

    reworked_eigenvectors = np.zeros((3, 3))
    for num in order:
        reworked_eigenvectors[:, num] = eigenvectors[:, num].copy()
    for i in range(3):
        if reworked_eigenvectors[i, i] < 0:
            reworked_eigenvectors[:, i] = -1 * reworked_eigenvectors[:, i].copy()

    return reworked_eigenvectors


def calculation():
    config = 'saddle-opt'
    nodes_1 = [(0.1, 0.4, 0.3), (-0.3, 0.2, -0.5), (0.2, -0.3, 0.1)]
    nodes_2 = rotate_nodes(nodes_1)

    inertia_tensor_1 = get_inertia_tensor(nodes_1)
    inertia_tensor_2 = get_inertia_tensor(nodes_2)

    eigenvalues_1, eigenvectors_1 = np.linalg.eig(inertia_tensor_1)
    eigenvalues_2, eigenvectors_2 = np.linalg.eig(inertia_tensor_2)

    reworked_eigenvectors_1 = get_reworked_eigenvectors(eigenvalues_1, eigenvectors_1)
    reworked_eigenvectors_2 = get_reworked_eigenvectors(eigenvalues_2, eigenvectors_2)

    nodes_1_rot = [tuple(reworked_eigenvectors_2 @ reworked_eigenvectors_1.T @ np.array(node)) for node in nodes_1]
    # nodes_2_rot = [tuple(reworked_eigenvectors_2.T @ np.array(node)) for node in nodes_2]


if __name__ == '__main__':
    calculation()
