from math import pi, cos, sin, acos
import numpy as np

vec_len = np.linalg.norm


def get_nodes():
    n0 = np.array([-0.3, 0, 0])
    n1 = np.array([0.4, 0, 0])
    n2 = np.array([0, 0.2, 0])

    transl = np.array([0.1, 0.6, 0.25])

    n0_transl = n0 + transl
    n1_transl = n1 + transl
    n2_transl = n2 + transl

    th = 30 / 180 * pi

    R = np.array([
        [cos(th), -sin(th), 0],
        [sin(th), cos(th), 0],
        [0, 0, 1]
    ])

    n0_rot = R @ n0_transl
    n1_rot = R @ n1_transl
    n2_rot = R @ n2_transl

    return n0_rot, n1_rot, n2_rot


def make_unit_length(vec):
    return vec / vec_len(vec)


def test_triangle(a0, a1, b2):
    if b2 < 0:
        raise Exception('b2 is negative')
    if a0 > a1:
        raise Exception('a1 is to the left of a0')
    if a0 > 0:
        raise Exception('a0 is positive')
    if a1 < 0:
        raise Exception('a1 is negative')


def tensor_transl(tensor, area, centroid, trans):

    tx, ty, tz = tuple(trans)
    cx, cy, cz = tuple(centroid)

    new_tensor = tensor + area * np.array([
        [ty**2 + tz**2 - 2*ty*cy - 2*tz*cz, -tx*ty + tx*cy + ty*cx,             -tx*tz + tx*cz + tz*cx],
        [-ty*tx + ty*cx + tx*cy,             tx**2 + tz**2 - 2*tx*cx - 2*tz*cz, -ty*tz + ty*cz + tz*cy],
        [-tx*tz + tx*cz + tz*cx,            -ty*tz + ty*cz + tz*cy,              tx**2 + ty**2 - 2*tx*cx - 2*ty*cy]
                                           ])

    return new_tensor


def total_procedure():
    n0, n1, n2 = get_nodes()

    print(n0, n1, n2)

    th = -30 / 180 * pi
    R = np.array([
        [cos(th), -sin(th), 0],
        [sin(th), cos(th), 0],
        [0, 0, 1]
    ])

    n0_inter = R @ n0
    n1_inter = R @ n1
    n2_inter = R @ n2

    n0_ori = n0_inter - np.array([0.1, 0.6, 0.25])
    n1_ori = n1_inter - np.array([0.1, 0.6, 0.25])
    n2_ori = n2_inter - np.array([0.1, 0.6, 0.25])

    centroid = 1 / 3 * (n0_ori + n1_ori + n2_ori)

    a0, a1, b2 = n0_ori[0], n1_ori[0], n2_ori[1]
    test_triangle(a0, a1, b2)

    area_l, area_r = -a0 * b2 / 2, a1 * b2 / 2
    area_lr = area_l + area_r

    Ixx_l, Ixx_r = -1 / 12 * a0 * b2**3, 1 / 12 * a1 * b2**3
    Iyy_l, Iyy_r = -1 / 12 * b2 * a0**3, 1 / 12 * b2 * a1**3
    Izz_l, Izz_r = Iyy_l + Ixx_l, Iyy_r + Ixx_r
    Ixy_l, Ixy_r = 1 / 24 * a0**2 * b2**2, -1 / 24 * a1**2 * b2**2

    tensor_l = np.array([[Ixx_l, Ixy_l, 0],
                         [Ixy_l, Iyy_l, 0],
                         [0,     0,     Izz_l]])
    tensor_r = np.array([[Ixx_r, Ixy_r, 0],
                         [Ixy_r, Iyy_r, 0],
                         [0,     0,     Izz_r]])
    tensor_lr = tensor_l + tensor_r

    # print(tensor_lr)

    tensor_back = tensor_transl(tensor_lr, area_lr, centroid, -np.array([0.1, 0.6, 0.25]))

    tensor_back = R.T @ tensor_back @ R

    print(tensor_back)

    # tensor_back_l = tensor_transl(tensor_l, area_l, -np.array([0.1, 0.6, 0.25]))
    # tensor_back_r = tensor_transl(tensor_r, area_r, -np.array([0.1, 0.6, 0.25]))
    #
    # print(tensor_back_l + tensor_back_r)


if __name__ == '__main__':
    total_procedure()
