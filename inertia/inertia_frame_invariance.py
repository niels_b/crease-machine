import numpy as np
from math import sin, cos, acos, pi
from inertia_cont import total_procedure, tensor_transl, get_nodes_and_triangles


vec_len = np.linalg.norm


def test_frame_invariance_rot():

    th = 32 / 180 * pi  # angle of rotation

    ax, ay, az = 0.71554175, 0.4472136, 0.53665631

    R = np.array([[cos(th) + ax ** 2 * (1 - cos(th)), ax * ay * (1 - cos(th)) - az * sin(th),
                   ax * az * (1 - cos(th)) + ay * sin(th)],
                  [ay * ax * (1 - cos(th)) + az * sin(th), cos(th) + ay ** 2 * (1 - cos(th)),
                   ay * az * (1 - cos(th)) - ax * sin(th)],
                  [az * ax * (1 - cos(th)) - ay * sin(th), az * ay * (1 - cos(th)) + ax * sin(th),
                   cos(th) + az ** 2 * (1 - cos(th))]])

    # rot_nodes = R @ np.array([
    #     [0.25780784, 0.29636127, 0.24990295],
    #     [0.21011094, 0.22319726, 0.2560791],
    #     [0.14105236, 0.05557488,  0.05060971]
    # ])
    # print(rot_nodes.T)

    inertia_1 = total_procedure('triangle')
    inertia_2 = total_procedure('triangle-rot')

    print(inertia_1)
    print(R.T @ inertia_2 @ R)


def test_frame_invariance_transl():

    t = np.array([0.4, 0.3, 0.7])

    # trans_nodes = [np.array([0.25780784, 0.21011094, 0.14105236]) + t,
    #                np.array([0.29636127, 0.22319726, 0.05557488]) + t,
    #                np.array([0.24990295, 0.2560791,  0.05060971]) + t]

    inertia_1 = total_procedure('triangle')
    inertia_2 = total_procedure('triangle-transl')

    nodes, _ = get_nodes_and_triangles('triangle-transl')

    v0, v1, v2 = np.array(nodes[0]), np.array(nodes[1]), np.array(nodes[2])
    v01 = v1 - v0
    v02 = v2 - v0

    area = 1 / 2 * vec_len(np.cross(v01, v02))
    centroid = 1 / 3 * (v0 + v1 + v2)

    print(inertia_1)
    print(tensor_transl(inertia_2, area, centroid, t))


def test_frame_invariance_rot_transl():
    pass


if __name__ == '__main__':
    # test_frame_invariance_rot()
    # test_frame_invariance_transl()
    test_frame_invariance_rot_transl()
