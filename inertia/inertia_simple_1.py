from core.directories import root_dir
from math import pi, cos, sin, acos
import numpy as np
import json

vec_len = np.linalg.norm


def get_nodes():
    n0 = np.array([-0.3, 0, 0])
    n1 = np.array([0.4, 0, 0])
    n2 = np.array([0, 0.2, 0])

    th = 30 / 180 * pi

    R = np.array([
        [cos(th), -sin(th), 0],
        [sin(th), cos(th), 0],
        [0, 0, 1]
    ])

    n0_rot = R @ n0
    n1_rot = R @ n1
    n2_rot = R @ n2

    return n0_rot, n1_rot, n2_rot


def make_unit_length(vec):
    return vec / vec_len(vec)


def test_triangle(a0, a1, b2):
    if b2 < 0:
        raise Exception('b2 is negative')
    if a0 > a1:
        raise Exception('a1 is to the left of a0')
    if a0 > 0:
        raise Exception('a0 is positive')
    if a1 < 0:
        raise Exception('a1 is negative')


def total_procedure():
    n0, n1, n2 = get_nodes()

    th = -30/180*pi
    R = np.array([
        [cos(th), -sin(th), 0],
        [sin(th), cos(th), 0],
        [0, 0, 1]
    ])

    n0_ori = R @ n0
    n1_ori = R @ n1
    n2_ori = R @ n2

    a0, a1, b2 = n0_ori[0], n1_ori[0], n2_ori[1]
    test_triangle(a0, a1, b2)

    # area_l, area_r = -a0 * b2 / 2, a1 * b2 / 2
    # area_lr = area_l + area_r

    Ixx_l, Ixx_r = -1 / 12 * a0 * b2**3, 1 / 12 * a1 * b2**3
    Iyy_l, Iyy_r = -1 / 12 * b2 * a0**3, 1 / 12 * b2 * a1**3
    Izz_l, Izz_r = Iyy_l + Ixx_l, Iyy_r + Ixx_r
    Ixy_l, Ixy_r = 1 / 24 * a0**2 * b2**2, -1 / 24 * a1**2 * b2**2

    tensor_l = np.array([[Ixx_l, Ixy_l, 0],
                         [Ixy_l, Iyy_l, 0],
                         [0,     0,     Izz_l]])
    tensor_r = np.array([[Ixx_r, Ixy_r, 0],
                         [Ixy_r, Iyy_r, 0],
                         [0,     0,     Izz_r]])
    tensor_lr = tensor_l + tensor_r

    tensor_back = R.T @ tensor_lr @ R

    print(tensor_back)


if __name__ == '__main__':
    total_procedure()
