from stl import mesh
from core.directories import root_dir

my_mesh = mesh.Mesh.from_file(f'{root_dir}/inputs/triangle.stl')
volume, cog, inertia = my_mesh.get_mass_properties()

print(volume)
print(cog)
print(inertia)
