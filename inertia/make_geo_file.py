from inertia_cont import get_nodes_and_triangles
from core.directories import root_dir

nodes, triangles = get_nodes_and_triangles()

filename = f'{root_dir}/inputs/saddle.geo'

with open(filename, 'w') as f:
    f.write('// Mesh size\n')
    f.write('ms = 0.001;\n')

    f.write('\n// Nodes\n')
    for i, node in enumerate(nodes):
        f.write(f'Point({i+1}) = \u007b{node[0]}, {node[1]}, {node[2]}, ms\u007d;\n')

    f.write('\n// Lines\n')
    for i, triangle in enumerate(triangles):
        f.write(f'Line({i*3+1}) = \u007b{triangle[0]+1}, {triangle[1]+1}\u007d;\n')
        f.write(f'Line({i*3+2}) = \u007b{triangle[1]+1}, {triangle[2]+1}\u007d;\n')
        f.write(f'Line({i*3+3}) = \u007b{triangle[2]+1}, {triangle[0]+1}\u007d;\n')

    f.write('\n// Loops\n')
    for i in range(len(triangles)):
        f.write(f'Curve Loop({i+1}) = \u007b{i*3+1}, {i*3+2}, {i*3+3}\u007d;\n')

    f.write('\n// Plane surfaces \n')
    for i in range(len(triangles)):
        f.write(f'Plane Surface({i+1}) = \u007b{i+1}\u007d;\n')

    f.write('\n// Physical surfaces \n')
    f.write(f'Physical Surface(1) = \u007b{str([i+1 for i in range(len(triangles))])[1:-1]}\u007d;\n')
