﻿import meshio
import numpy as np
from core.directories import root_dir
from inertia.inertia_cont import vec_len


def main_function():
    shape = 'saddle'
    mesh_file = f'{root_dir}/inputs/{shape}.msh'
    my_mesh = meshio.read(mesh_file)  # get points and cells with meshio
    points = my_mesh.points

    small_triangles = []
    for patch in my_mesh.cells:
        for triangle in patch.data:
            small_triangles.append(triangle)

    Ixx, Iyy, Izz, Ixy, Ixz, Iyz = 0, 0, 0, 0, 0, 0

    for small_triangle in small_triangles:
        ind0, ind1, ind2 = tuple(small_triangle)
        v0, v1, v2 = points[ind0], points[ind1], points[ind2]
        v01 = v1 - v0
        v02 = v2 - v0
        small_area = 1 / 2 * vec_len(np.cross(v01, v02))

        com = 1/3 * (points[ind0] + points[ind1] + points[ind2])

        Ixx += small_area * (com[1]**2 + com[2]**2)
        Iyy += small_area * (com[0]**2 + com[2]**2)
        Izz += small_area * (com[0]**2 + com[1]**2)
        Ixy += small_area * -com[0]*com[1]
        Ixz += small_area * -com[0]*com[2]
        Iyz += small_area * -com[1]*com[2]

    inertia = np.array([[Ixx, Ixy, Ixz],
                        [Ixy, Iyy, Iyz],
                        [Ixz, Iyz, Izz]])

    print(inertia)


if __name__ == '__main__':
    main_function()
