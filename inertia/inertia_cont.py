from core.directories import root_dir
from math import cos, sin, acos
import numpy as np
import json

vec_len = np.linalg.norm


def get_nodes_and_triangles(shape):
    with open(f'{root_dir}/inputs/inertia-{shape}.json') as f:
        data = json.load(f)

    nodes = [tuple(node) for node in data['nodes']]
    triangles = [tuple(panel) for panel in data['triangles']]

    return nodes, triangles


def make_unit_length(vec):
    return vec / vec_len(vec)


def transform_to_xy(n0, n1, n2):
    v01 = make_unit_length(n1 - n0)
    v02 = make_unit_length(n2 - n0)

    normal_vec = make_unit_length(np.cross(v01, v02))

    z_axis = np.array([0, 0, 1])

    axis_of_rotation = make_unit_length(np.cross(normal_vec, z_axis))

    th = acos(np.dot(normal_vec, z_axis))  # angle of rotation

    ax, ay, az = axis_of_rotation[0], axis_of_rotation[1], axis_of_rotation[2]

    R = np.array([[cos(th) + ax ** 2 * (1 - cos(th)), ax * ay * (1 - cos(th)) - az * sin(th),
                   ax * az * (1 - cos(th)) + ay * sin(th)],
                  [ay * ax * (1 - cos(th)) + az * sin(th), cos(th) + ay ** 2 * (1 - cos(th)),
                   ay * az * (1 - cos(th)) - ax * sin(th)],
                  [az * ax * (1 - cos(th)) - ay * sin(th), az * ay * (1 - cos(th)) + ax * sin(th),
                   cos(th) + az ** 2 * (1 - cos(th))]])

    n0_flat = R @ n0
    n1_flat = R @ n1
    n2_flat = R @ n2

    if not np.isclose(R.T, np.linalg.inv(R)).all():
        raise Exception('rotation matrix not ortogonal')

    return n0_flat, n1_flat, n2_flat, R


def transform_to_origin(n0, n1, n2):
    x_axis = np.array([1, 0, 0])
    l01 = vec_len(n1 - n0)
    l12 = vec_len(n2 - n1)
    l20 = vec_len(n0 - n2)

    if l12 > l01 and l12 > l20:
        new_n0 = n1.copy()
        new_n1 = n2.copy()
        new_n2 = n0.copy()
        n0 = new_n0.copy()
        n1 = new_n1.copy()
        n2 = new_n2.copy()
    elif l20 > l01 and l20 > l12:
        new_n0 = n2.copy()
        new_n1 = n0.copy()
        new_n2 = n1.copy()
        n0 = new_n0.copy()
        n1 = new_n1.copy()
        n2 = new_n2.copy()
    else:
        pass

    v01 = make_unit_length(n1 - n0)

    th = acos(np.dot(v01, x_axis))  # angle of rotation
    if np.cross(x_axis, v01)[2] > 0:
        th = -th  # accounts for the direction

    R = np.array([[cos(th), -sin(th), 0],
                  [sin(th), cos(th), 0],
                  [0, 0, 1]])

    n0_xrot = R @ n0
    n1_xrot = R @ n1
    n2_xrot = R @ n2

    T = np.array([-n2_xrot[0], -n0_xrot[1], -n0_xrot[2]])

    n0_ori = n0_xrot + T
    n1_ori = n1_xrot + T
    n2_ori = n2_xrot + T

    if not np.isclose(R.T, np.linalg.inv(R)).all():
        raise Exception('rotation matrix not ortogonal')

    return n0_ori, n1_ori, n2_ori, R, T


def test_triangle(a0, a1, b2):
    if b2 < 0:
        raise Exception('b2 is negative')
    if a0 > a1:
        raise Exception('a1 is to the left of a0')
    if a0 > 0:
        raise Exception('a0 is positive')
    if a1 < 0:
        raise Exception('a1 is negative')


def tensor_transl(tensor, area, centroid, trans):

    tx, ty, tz = tuple(trans)
    cx, cy, cz = tuple(centroid)

    new_tensor = tensor + area * np.array([
        [ty**2 + tz**2 - 2*ty*cy - 2*tz*cz, -tx*ty + tx*cy + ty*cx,             -tx*tz + tx*cz + tz*cx],
        [-ty*tx + ty*cx + tx*cy,             tx**2 + tz**2 - 2*tx*cx - 2*tz*cz, -ty*tz + ty*cz + tz*cy],
        [-tx*tz + tx*cz + tz*cx,            -ty*tz + ty*cz + tz*cy,              tx**2 + ty**2 - 2*tx*cx - 2*ty*cy]
                                           ])

    return new_tensor


def total_procedure(shape):
    inertia = np.zeros((3, 3))

    nodes, triangles = get_nodes_and_triangles(shape)
    for triangle in triangles:
        n0, n1, n2 = np.array(nodes[triangle[0]]), np.array(nodes[triangle[1]]), np.array(nodes[triangle[2]])

        n0_flat, n1_flat, n2_flat, R1 = transform_to_xy(n0, n1, n2)
        n0_ori, n1_ori, n2_ori, R2, T2 = transform_to_origin(n0_flat, n1_flat, n2_flat)
        a0, a1, b2 = n0_ori[0], n1_ori[0], n2_ori[1]
        test_triangle(a0, a1, b2)

        # centroid_l = 1 / 3 * (n0_ori + n2_ori)
        # centroid_r = 1 / 3 * (n1_ori + n2_ori)
        centroid_lr = 1 / 3 * (n0_ori + n1_ori + n2_ori)

        area_l, area_r = -a0 * b2 / 2, a1 * b2 / 2
        area_lr = area_l + area_r

        Ixx_l, Ixx_r = -1 / 12 * a0 * b2**3, 1 / 12 * a1 * b2**3
        Iyy_l, Iyy_r = -1 / 12 * b2 * a0**3, 1 / 12 * b2 * a1**3
        Izz_l, Izz_r = Iyy_l + Ixx_l, Iyy_r + Ixx_r
        Ixy_l, Ixy_r = 1 / 24 * a0**2 * b2**2, -1 / 24 * a1**2 * b2**2

        tensor_l = np.array([[Ixx_l, Ixy_l, 0],
                             [Ixy_l, Iyy_l, 0],
                             [0,     0,     Izz_l]])
        tensor_r = np.array([[Ixx_r, Ixy_r, 0],
                             [Ixy_r, Iyy_r, 0],
                             [0,     0,     Izz_r]])
        tensor_lr = tensor_l + tensor_r

        tensor_back = tensor_transl(tensor_lr, area_lr, centroid_lr, T2)
        tensor_back = R2.T @ tensor_back @ R2
        tensor_back = R1.T @ tensor_back @ R1

        inertia += tensor_back

    # print(inertia)
    return inertia


if __name__ == '__main__':
    my_shape = 'triangle'
    total_procedure(my_shape)
